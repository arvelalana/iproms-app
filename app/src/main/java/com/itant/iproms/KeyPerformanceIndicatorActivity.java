package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.adapter.AdapterTask;
import com.itant.iproms.lib.adapter.AdapterTaskKpi;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.itant.iproms.mapp.mObject.UserPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class KeyPerformanceIndicatorActivity extends MAppCompat {
    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private RecyclerView taskRecyclerView;
    private ConstraintLayout layoutMain;
    private GridLayoutManager mLayoutManager;
    private Boolean isOpen = false;
    private Spinner spinnerEmployee;
    private Button showTask;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_performance_indicator);
        activity = this;

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("DAFTAR TUGAS SAYA");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinnerEmployee = findViewById(R.id.spinner_kpi_task);
        showTask = findViewById(R.id.btn_show_kpi_task);
        taskRecyclerView = findViewById(R.id.kpi_recycler_view);
        mLayoutManager = new GridLayoutManager(activity, 1);
        taskRecyclerView.setLayoutManager(mLayoutManager);
        taskRecyclerView.setItemAnimator(new DefaultItemAnimator());
        doApiSubordinate();

        showTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserList userList = UserList.getInstance(activity);
                if (spinnerEmployee.getSelectedItemPosition() >= 0) {
                    userId = userList.getPosition(spinnerEmployee.getSelectedItemPosition()).getUserId();
                }
                Filter filter = Filter.getInstance(activity);
                filter.clear();
                doApiTaskFilter("0");
            }
        });
    }

    private void doApiSubordinate() {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "Subordinate");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);

                        UserList userArrayList = UserList.getInstance(activity);
                        userArrayList.clear();
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                user = new User(activity);
                                user.setUserId(userList.getString("user_id"));
                                user.setUsername(userList.getString("username"));
                                user.setFirstName(userList.getString("first_name"));
                                user.setLastName(userList.getString("last_name"));
                                user.setEmail(userList.getString("email"));
                                userArrayList.add(user);
                            }

                            ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, userArrayList.getAllUsername());
                            userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerEmployee.setAdapter(userArrayAdapter);
                            closeDialog(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
            closeDialog(activity);
        }
    }

    private void doApiTaskFilter(final String page) {
        openDialog(activity);
        Filter filter = Filter.getInstance(activity);
        filter.setTaskStatus("DONE");
        try {
            apiIProms = ApiIProms.factory(this, "TaskFilter");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("user_id", userId);
            postData.put("sort_by", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Task task = Task.getInstance(activity);
                        TaskList taskListArr = TaskList.getInstance(activity);
                        ArrayList<Task> tempArraylist = new ArrayList<Task>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject taskList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = taskList.getJSONArray("users");
                                task = new Task();

                                if (taskList.getString("to_do_list_id").length() > 0) {
                                    pageCount += 1;
                                }

                                task.setId(taskList.getString("to_do_list_id"));
                                task.setCreated(taskList.getString("creator_name"));
                                task.setCreatedDate(taskList.getString("created"));
                                task.setDescription(taskList.getString("description"));
                                task.setTaskCode(taskList.getString("to_do_list_code"));
                                task.setTaskName(taskList.getString("to_do_list_name"));
                                task.setTaskStatus(taskList.getString("to_do_list_status"));
                                task.setProjectName(taskList.getString("project_name"));
                                task.setProjectDate(taskList.getString("project_created_date"));
                                task.setProjectCreator(taskList.getString("project_created_by"));
                                task.setStartDate(taskList.getString("plan_start_date"));
                                task.setEndDate(taskList.getString("plan_end_date"));
                                task.setActualStartDate(taskList.getString("actual_start_date"));
                                task.setActualEndDate(taskList.getString("actual_end_date"));
                                task.setUserOrder(taskList.getString("user_order_name"));
                                task.setUserApprove(taskList.getString("user_approve"));
                                task.setTaskReason(taskList.getString("reason"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    userList.add(user);
                                }

                                task.setUserList(userList);
                                tempArraylist.add(task);
                            }

                            task = Task.getInstance(activity);
                            taskListArr.setTaskArrayList(tempArraylist);
                            AdapterTaskKpi adapterTaskKpi = new AdapterTaskKpi(activity, taskListArr.getTaskArrayList());
                            taskRecyclerView.setAdapter(adapterTaskKpi);
                            closeDialog(activity);

                            if (taskListArr.getTaskArrayList().size() < 1) {

                            }
                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        closeDialog(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            closeDialog(activity);
            e1.printStackTrace();
        }
    }


}
