package com.itant.iproms;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.TaskStatusDialog;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.MShareDataString;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Random;

public class DetailTaskActivity extends MAppCompat {

    private ApiIProms apiIProms;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private TaskList taskList;
    private TextView tvStatus, tvTaskStart, tvTaskEnd, tvTaskCreated, tvTaskCreatedBy, tvTaskDesc, tvProjectCreated, tvProjectName, tvProjectDate, tvTaskName, tvTaskUserStart, tvTaskReason, tvTaskFor;
    private Task task;
    private Button btnPending, btnInProgress, btnDone, btnAccept, btnReject, btnCancel;
    private String taskId, statusTask, reason, pendingDate;
    private DrawerLayout layoutMain;
    private ImageView imageRibbon;
    private MSharedData sharedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_task);
        activity = this;

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.my_task).toUpperCase());
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        layoutMain = findViewById(R.id.drawer_main);
        tvTaskName = findViewById(R.id.tv_task_name);
        tvTaskCreated = findViewById(R.id.tv_task_created_date);
        tvTaskCreatedBy = findViewById(R.id.tv_task_created_by);
        tvStatus = findViewById(R.id.tv_task_status);
        tvTaskStart = findViewById(R.id.tv_task_start_date);
        tvTaskUserStart = findViewById(R.id.tv_task_user_start_date);
        tvTaskEnd = findViewById(R.id.tv_task_end_date);
        tvTaskDesc = findViewById(R.id.tv_task_description);
        tvProjectName = findViewById(R.id.tv_task_project_name);
        tvProjectCreated = findViewById(R.id.tv_task_project_created_by);
        tvProjectDate = findViewById(R.id.tv_task_project_created_date);
        tvTaskReason = findViewById(R.id.tv_task_reason);
        tvTaskFor = findViewById(R.id.tv_task_for);
        btnPending = findViewById(R.id.btn_task_pending);
        btnInProgress = findViewById(R.id.btn_task_progress);
        btnDone = findViewById(R.id.btn_task_done);
        btnAccept = findViewById(R.id.btn_task_accept);
        btnReject = findViewById(R.id.btn_task_reject);
        btnCancel = findViewById(R.id.btn_task_cancel);
        imageRibbon = findViewById(R.id.image_status);

        sharedData = MSharedData.getInstance(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            taskId = bundle.getString("taskId");
            taskList = TaskList.getInstance(activity);
            Task task = taskList.get(taskId);
            tvTaskName.setText(task.getTaskName());
            tvTaskName.setTextColor(color);
            tvTaskCreated.setText(task.getCreatedDate());
            tvTaskCreatedBy.setText(task.getCreated());
            tvTaskStart.setText(task.getStartDate());
            tvTaskEnd.setText(task.getEndDate());
            tvStatus.setText(task.getTaskStatusText().toUpperCase());
            if (!task.getUserOrder().equalsIgnoreCase("null")) {
                tvTaskCreatedBy.setText(task.getUserOrder());
            }
            if (!task.getActualStartDate().equalsIgnoreCase("01-01-1970")) {
                tvTaskUserStart.setText(task.getActualStartDate());
            }
            if (!task.getTaskReason().equalsIgnoreCase("null")) {
                tvTaskReason.setText(task.getTaskReason());
            }
            tvTaskDesc.setText(task.getDescription());
            tvProjectName.setText(task.getProjectName().toUpperCase());
            tvProjectName.setTextColor(color);
            tvProjectCreated.setText("Proyek dari " + task.getProjectCreator());
            tvProjectDate.setText(task.getProjectDate());
            if (task.getTaskStatus().equalsIgnoreCase("request")) {
//                tvStatus.setTextColor(getResources().getColor(R.color.md_purple_500));
                btnPending.setVisibility(View.GONE);
                btnInProgress.setVisibility(View.GONE);
                btnDone.setVisibility(View.GONE);
                if (task.getUserApprove().equalsIgnoreCase(sharedData.get(SharedData.USER_ID))) {
                    btnAccept.setVisibility(View.VISIBLE);
                    btnReject.setVisibility(View.VISIBLE);
                }
            }

            if (task.getTaskStatus().equalsIgnoreCase("reject")) {
                btnPending.setVisibility(View.GONE);
                btnInProgress.setVisibility(View.GONE);
                btnDone.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
            }

            if (task.getTaskStatus().equalsIgnoreCase("in_progress")) {
                imageRibbon.setImageResource(R.drawable.ribbon_blue);
                tvStatus.setText(task.getTaskStatusText().toUpperCase());
                btnInProgress.setVisibility(View.GONE);
            }

            if (task.getTaskStatus().equalsIgnoreCase("pending")) {
                imageRibbon.setImageResource(R.drawable.ribbon_orange);
                tvStatus.setText(task.getTaskStatusText().toUpperCase());
                btnPending.setVisibility(View.GONE);
            }

            if (task.getTaskStatus().equalsIgnoreCase("active")) {
                imageRibbon.setImageResource(R.drawable.ribbon_green);
                tvStatus.setText(task.getTaskStatusText().toUpperCase());
            }

            if (task.getTaskStatus().equalsIgnoreCase("done")) {
                btnPending.setVisibility(View.GONE);
                btnInProgress.setVisibility(View.GONE);
                btnDone.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
            }
//            if (task.getCreated().equalsIgnoreCase(sharedData.get(SharedData.USERNAME))) {
//                btnPending.setVisibility(View.GONE);
//                btnInProgress.setVisibility(View.GONE);
//                btnDone.setVisibility(View.GONE);
//                btnAccept.setVisibility(View.GONE);
//                btnReject.setVisibility(View.GONE);
//            }

            UserList userList = task.getUserList();
            String userListString = "";
            for (int i = 0; i <= userList.getUserArrayList().size() - 1; i++) {
                if (i == 0) {
                    userListString = userListString + userList.getUserArrayList().get(i).getFullname();
                } else {
                    userListString = userListString + ", " + userList.getUserArrayList().get(i).getFullname();
                }
            }
            tvTaskFor.setText(userListString);

        }

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_reject_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_reject_yes));
                messageDialog.setHeader("Apakah anda yakin ingin menolak tugas ini?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "REJECT";
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusTask();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_accept_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_accept_yes));
                messageDialog.setHeader("Apakah anda yakin ingin menyetujui tugas ini?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "ACCEPT";
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusTask();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_cancel_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_cancel_yes));
                messageDialog.setHeader("Batalkan Tugas Ini?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "CANCEL";
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusTask();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });

        btnPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_pending_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_pending_yes));
                messageDialog.setHeader("Apakah anda yakin ingin pending tugas ini?");
                messageDialog.setShowDate(true, getFragmentManager());
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "PENDING";
                        openDialog(activity);
                        pendingDate = messageDialog.getPendingDate();
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusTask();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();

            }
        });

        btnInProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_progress_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_progress_yes));
                messageDialog.setHeader("Apakah tugas ini dalam pengerjaan?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "IN_PROGRESS";
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusTask();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();

            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_done_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_done_yes));
                messageDialog.setHint("Catatan");
                messageDialog.setHeader("Apakah tugas ini sudah selesai?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "DONE";
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusTask();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });
    }

    private void doApiUpdateStatusTask() {
        try {
            apiIProms = ApiIProms.factory(this, "UpdateStatusTask");
            HashMap postData = new HashMap();
            postData.put("reason", reason);
            postData.put("task_id", taskId);
            postData.put("status_task", statusTask);
            if (statusTask.equalsIgnoreCase("PENDING")) {
                postData.put("pending_date", pendingDate);
            }
            System.out.println(postData);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            closeDialog(activity);
                            finish();
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }
}
