package com.itant.iproms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextInputEditText;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class NewMyTaskActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener {

    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private DrawerLayout layoutMain;
    private ApiIProms apiIProms;
    private Spinner spinnerUserApprove, spinnerProject, spinnerUserOrder;
    private String dateMode;
    private Boolean isOpen = false;
    private MTextInputEditText editTextTaskName, editTextShortDescription, editTextStartDate, editTextEndDate;
    private String projectId, taskName, description, startDate, endDate, userOrder, userApprove;
    private Button buttonSave;
    private Project project;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_my_task);

        activity = this;

        layoutMain = findViewById(R.id.drawer_main);
        spinnerUserOrder = findViewById(R.id.spinner_user_order_my_task);
        spinnerUserApprove = findViewById(R.id.spinner_user_approve_my_task);
        spinnerProject = findViewById(R.id.spinner_project);
        editTextTaskName = findViewById(R.id.edit_text_my_task_name);
        editTextShortDescription = findViewById(R.id.edit_text_my_task_short_description);
        editTextStartDate = findViewById(R.id.edit_text_my_task_start_date);
        editTextEndDate = findViewById(R.id.edit_text_my_task_end_date);
        buttonSave = findViewById(R.id.button_save_my_task);

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.create_my_task));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;

                ProjectList projectList = ProjectList.getInstance(activity);
                projectId = projectList.getPosition(spinnerProject.getSelectedItemPosition()).getId();
                UserList userList = UserList.getInstance(activity);
                userOrder = userList.getPosition(spinnerUserOrder.getSelectedItemPosition()).getUserId();
                userApprove = userList.getPosition(spinnerUserApprove.getSelectedItemPosition()).getUserId();
                taskName = editTextTaskName.getText().toString();
                description = editTextShortDescription.getText().toString();
                startDate = editTextStartDate.getText().toString();
                endDate = editTextEndDate.getText().toString();

                if (taskName.length() < 1) {
                    err++;
                    editTextTaskName.setError(getResources().getString(R.string.task_name_blank));
                }

                if (description.length() < 1) {
                    err++;
                    editTextShortDescription.setError(getResources().getString(R.string.short_description_blank));
                }

                if (startDate.length() < 1) {
                    err++;
                    editTextStartDate.setError(getResources().getString(R.string.start_date_blank));
                }

                if (endDate.length() < 1) {
                    err++;
                    editTextEndDate.setError(getResources().getString(R.string.end_date_blank));
                }

                if (err == 0) {
                    openDialog(activity);
                    doApiNewMyTask();
                }
            }
        });

        openDialog(activity);
        doApiProject();
    }

    private void doApiProject() {
        try {
            apiIProms = ApiIProms.factory(this, "ProjectList");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        ProjectList projectArrayList = ProjectList.getInstance(activity);
                        projectArrayList.clear();
                        project = new Project(activity);
                        project.setId("0");
                        project.setProjectName("None");
                        projectArrayList.add(project);
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject projectList = (JSONObject) resultsArr.get(i);
                                project = new Project(activity);
                                project.setId(projectList.getString("project_id"));
                                project.setCreated(projectList.getString("created"));
                                project.setDescription(projectList.getString("description"));
                                project.setProjectCode(projectList.getString("project_code"));
                                project.setProjectName(projectList.getString("project_name"));
                                project.setProjectStatus(projectList.getString("status_project"));
                                projectArrayList.add(project);
                            }

                            ArrayAdapter<String> projectArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, projectArrayList.getAllProjectName());
                            projectArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerProject.setAdapter(projectArrayAdapter);

                            doApiUserList();

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }


    private void doApiUserList() {
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);

                        UserList userArrayList = UserList.getInstance(activity);
                        userArrayList.clear();
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                user = new User(activity);
                                user.setUserId(userList.getString("user_id"));
                                user.setUsername(userList.getString("username"));
                                user.setFirstName(userList.getString("first_name"));
                                user.setLastName(userList.getString("last_name"));
                                user.setEmail(userList.getString("email"));
                                userArrayList.add(user);
                            }

                            ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, userArrayList.getAllUsername());
                            userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerUserOrder.setAdapter(userArrayAdapter);
                            spinnerUserApprove.setAdapter(userArrayAdapter);
                            closeDialog(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    private void doApiNewMyTask() {
        try {
            apiIProms = ApiIProms.factory(this, "NewMyTask");
            HashMap postData = new HashMap();
            postData.put("project_id", projectId);
            postData.put("start_date", startDate);
            postData.put("end_date", endDate);
            postData.put("task_name", taskName);
            postData.put("description", description);
            postData.put("user_order", userOrder);
            postData.put("user_approve", userApprove);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            closeDialog(activity);
                            Intent intent = new Intent(activity, MyTaskActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
        } else {
            editTextEndDate.setText(fullDate);
        }
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;
        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Mulai";
            dateValue = editTextStartDate.getText().toString();
        } else {
            title = "Tanggal Berakhir";
            dateValue = editTextEndDate.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                NewMyTaskActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    NewMyTaskActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }

}
