package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DetailProjectActivity extends MAppCompat {

    public static final int NEW_PROJECT_ADDED = 11;
    public static final int NEW_PROJECT_REQUEST_USER = 1;
    private ApiIProms apiIProms;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ProjectList projectList;
    private DrawerLayout layoutMain;
    private FlexboxLayout layoutDetailProyekUserList;
    private TextView tvProjectName, tvProjectCreated, tvProjectCreatedBy, tvProjectDesc;
    private Button btnAddUser, btnShowTask, btnAddNewTask;
    private String projectId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_project);
        activity = this;

        //Set Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.my_project));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        layoutMain = findViewById(R.id.drawer_main);
        layoutDetailProyekUserList = findViewById(R.id.layout_user_list_detail_project);
        tvProjectName = findViewById(R.id.tv_project_name);
        tvProjectCreated = findViewById(R.id.tv_project_created);
        tvProjectCreatedBy = findViewById(R.id.tv_project_created_by);
        tvProjectDesc = findViewById(R.id.tv_project_desc);
        btnAddUser = findViewById(R.id.btn_add_user_detail_project);
        btnAddUser.setVisibility(View.GONE);
        btnShowTask = findViewById(R.id.btn_show_task);
        btnAddNewTask = findViewById(R.id.btn_add_task);

        //Var
        final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
        userChipActivity.getSelectedChipList().clear();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String projectId = bundle.getString("projectId");

            projectList = ProjectList.getInstance(activity);
            Project project = projectList.get(projectId);
            this.projectId = projectId;
            toolbarTitle.setText(project.getProjectName());

            tvProjectName.setText(project.getProjectName());
            tvProjectCreated.setText(project.getCreated());
            tvProjectCreatedBy.setText(project.getCreatedBy());
            tvProjectDesc.setText(project.getDescription());

            UserList userList = project.getUserList();

            System.out.println(userList.getUserArrayList());
            ChipView chipView;
            UserChip userChip = null;
            userChipActivity.clearSelectedChipList();
            for (int i = 0; i < userList.getUserArrayList().size(); i++) {
                String username = userList.getUserArrayList().get(i).getUsername();
                String userId = userList.getUserArrayList().get(i).getUserId();
                userChip = new UserChip(userId, username);
                chipView = new ChipView(activity);
                chipView.setPadding(15, 10, 15, 10);
                chipView.setLabel(userChip.getLabel());
                chipView.setHasAvatarIcon(true);
                chipView.setDeletable(false);

                final ChipView tempChipView = chipView;
                final int j = i;
                chipView.setOnDeleteClicked(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userChipActivity.getSelectedChipList().remove(j);
                        layoutDetailProyekUserList.removeView(tempChipView);
                    }
                });
                layoutDetailProyekUserList.addView(chipView);
                userChipActivity.addSelectedChipList(userId, userChip);
            }
//            userChipActivity.setSelectedChipList(chipList);
        }


        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(activity);
                if (UserChipActivity.getInstance(activity).getTotalUserChip() > 0) {
                    Intent intent = new Intent(activity, AddUserActivity.class);
                    startActivityForResult(intent, NEW_PROJECT_REQUEST_USER);
                } else {
                    doApiUserList();
                }
            }
        });

        btnShowTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MyTaskActivity.class);
                intent.putExtra("projectId", projectId);
                startActivity(intent);
            }
        });

        btnAddNewTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, NewTaskActivity.class);
                intent.putExtra("projectId", projectId);
                startActivity(intent);

            }
        });


    }

    private void doApiUserList() {
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                String username = userList.getString("username");
                                userChipActivity.add(new UserChip(userList.getString("user_id"), username));
                            }


                            openDialog(activity);
                            Intent intent = new Intent(activity, AddUserActivity.class);
                            startActivityForResult(intent, NEW_PROJECT_REQUEST_USER);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }
}
