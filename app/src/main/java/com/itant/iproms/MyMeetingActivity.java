package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterMeeting;
import com.itant.iproms.lib.adapter.AdapterProject;
import com.itant.iproms.lib.adapter.AdapterTask;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.FilterMeetingDialog;
import com.itant.iproms.lib.object.Meeting;
import com.itant.iproms.lib.object.MeetingList;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyMeetingActivity extends MAppCompat {

    private Activity activity;
    private Toolbar toolbar;
    private DrawerLayout layoutMain;
    private MTextView toolbarTitle;
    private FloatingActionButton fabAddMeeting;
    private ApiIProms apiIProms;
    private AdapterMeeting adapterMeeting;
    private RecyclerView meetingRecyclerView;
    private GridLayoutManager mLayoutManager;
    private ImageView imageMoreMeeting, imageNoMeeting;
    private String show;
    private SwipeRefreshLayout swipeMeeting;
    private Button btnFilter;
    private FilterMeetingDialog filterMeetingDialog;
    private String filterStartDate, filterEndDate;
    public static final int NEW_MEETING_REQUEST = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_meeting);
        activity = this;

        //Layout
        layoutMain = findViewById(R.id.drawer_main);
        fabAddMeeting = findViewById(R.id.fab_add_meeting);
        swipeMeeting = findViewById(R.id.swipe_meeting);
        btnFilter = findViewById(R.id.btn_filter_meeting);

        //Action Bar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getString(R.string.meeting_list));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        meetingRecyclerView = findViewById(R.id.meeting_recycler_view);
        mLayoutManager = new GridLayoutManager(activity, 1);
        meetingRecyclerView.setLayoutManager(mLayoutManager);
        meetingRecyclerView.setItemAnimator(new DefaultItemAnimator());

        filterStartDate = "";
        filterEndDate = "";

        fabAddMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, NewMeetingActivity.class);
                startActivityForResult(intent, NEW_MEETING_REQUEST);
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterMeetingDialog = new FilterMeetingDialog(activity, getFragmentManager());
                filterMeetingDialog.getBtnOk().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        isSent = true;
//                        status = "REJECT";
//                        rejectReason = rejectDialog.getEditTextReason().getText().toString();
                        filterStartDate = filterMeetingDialog.getEditTextDate1().getText().toString();
                        filterEndDate = filterMeetingDialog.getEditTextDate2().getText().toString();
//                        rejectDate3 = rejectDialog.getEditTextDate3().getText().toString();
                        doApiMeetingList("0");
                        filterMeetingDialog.dismiss();
                    }
                });
                filterMeetingDialog.show();
            }
        });

        swipeMeeting.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doApiMeetingList("0");
            }
        });

        doApiMeetingList("0");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        doApiMeetingList("0");
    }

    private void doApiMeetingList(String page) {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "MeetingList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("start_date", filterStartDate);
            postData.put("end_date", filterEndDate);
            postData.put("sort_by", "");
            postData.put("status", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        Meeting meeting = Meeting.getInstance(activity);
                        MeetingList meetingListArr = MeetingList.getInstance(activity);
                        ArrayList<Meeting> tempArraylist = new ArrayList<Meeting>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;
                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject meetingList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = meetingList.getJSONArray("users");

                                meeting = new Meeting();
                                meeting.setId(meetingList.getString("meeting_id"));
                                meeting.setMeetingName(meetingList.getString("meeting_name"));
                                meeting.setMeetingCode(meetingList.getString("meeting_code"));
                                meeting.setDescription(meetingList.getString("description"));
                                meeting.setMeetingStatus(meetingList.getString("status"));
                                meeting.setStartDate(meetingList.getString("start_date"));
                                meeting.setStartHour(meetingList.getString("start_hour"));
                                meeting.setEndHour(meetingList.getString("end_hour"));
                                meeting.setLocation(meetingList.getString("location"));
                                meeting.setHaveNotes(meetingList.getString("have_notes"));
                                meeting.setUpdated(meetingList.getString("updated"));
                                meeting.setUpdatedBy(meetingList.getString("updatedby"));
                                meeting.setCreated(meetingList.getString("created"));
                                meeting.setCreatedBy(meetingList.getString("createdby"));
                                meeting.setUserCreator(meetingList.getString("user_creator"));
                                meeting.setUserCreatorName(meetingList.getString("user_creator_name"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id"));
                                    user.setUsername(userArrList.getString("username"));
                                    user.setFirstName(userArrList.getString("first_name"));
                                    user.setLastName(userArrList.getString("last_name"));
                                    user.setEmail(userArrList.getString("email"));
                                    user.setFullname(userArrList.getString("full_name"));
                                    user.setMeetingEmployeeId(userArrList.getString("meeting_employee_id"));
                                    user.setMeetingEmployeeStatus(userArrList.getString("status"));
                                    userList.add(user);
                                }
                                meeting.setUserList(userList);
                                tempArraylist.add(meeting);
                            }
                            if (swipeMeeting.isRefreshing()) {
                                swipeMeeting.setRefreshing(false);
                            }
                            meetingListArr.setMeetingArrayList(tempArraylist);
                            adapterMeeting = new AdapterMeeting(activity, meetingListArr.getMeetingArrayList());
                            meetingRecyclerView.setAdapter(adapterMeeting);
                            closeDialog(activity);
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
            closeDialog(activity);
        }

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        if (requestCode == NEW_MEETING_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Snackbar.make(layoutMain, "Meeting Berhasil Ditambahkan", Snackbar.LENGTH_SHORT).show();
            }
        }

    }
}
