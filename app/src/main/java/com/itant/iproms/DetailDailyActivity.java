package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.TaskStatusDialog;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.DailyActivityList;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static com.itant.iproms.MyDailyActivity.NEW_DAILY_REQUEST;
import static com.itant.iproms.MyDailyActivity.RESULT_EDIT_SUCCESS;

public class DetailDailyActivity extends MAppCompat {

    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private DrawerLayout layoutMain;
    private DailyActivity dailyActivity;
    private DailyActivityList dailyActivityList;
    private TextView tvDailyName, tvDailyDescription, tvDailyStartDate, tvDailyStartHour, tvDailyEndHour, tvDailyApprove, tvDailyDeadline, tvDailyStatus, tvUserCreator;
    private MSharedData sharedData;
    private String dailyId, statusTask, reason;
    private ImageView imageRibbon;
    private ArrayList<String> dailyIdList;
    private Button btnAccept, btnReject, btnCreateNew, btnCreateCopy;

    public static final int EDIT_DAILY_ACTIVITY = 78;
    public static final int EDIT_DAILY_ACTIVITY_SUCCES = 79;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_daily);
        activity = this;

        layoutMain = findViewById(R.id.drawer_main);
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("DETAIL KINERJA HARIAN");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dailyIdList = new ArrayList<String>();

        layoutMain = findViewById(R.id.drawer_main);
        tvDailyName = findViewById(R.id.tv_daily_name);
        tvDailyDescription = findViewById(R.id.tv_daily_description);
        tvDailyStartDate = findViewById(R.id.tv_daily_start_date);
        tvDailyStartHour = findViewById(R.id.tv_daily_start_hour);
        tvDailyEndHour = findViewById(R.id.tv_daily_end_hour);
        tvDailyApprove = findViewById(R.id.tv_daily_user_approve);
        tvDailyDeadline = findViewById(R.id.tv_daily_deadline_date);
        tvDailyStatus = findViewById(R.id.tv_daily_status);
        tvUserCreator = findViewById(R.id.tv_daily_user_creator);
        imageRibbon = findViewById(R.id.image_status);
        btnAccept = findViewById(R.id.btn_daily_accept);
        btnReject = findViewById(R.id.btn_daily_reject);
        btnCreateNew = findViewById(R.id.btn_daily_create_new);
        btnCreateCopy = findViewById(R.id.btn_daily_create_copy);

        sharedData = MSharedData.getInstance(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

            dailyId = bundle.getString("dailyId");
            dailyIdList.add(dailyId);
            dailyActivityList = DailyActivityList.getInstance(activity);
            System.out.println("HUE" + dailyId);
            System.out.println("HUE" + dailyActivityList.getAllDailyActivityName());
            DailyActivity dailyActivity = dailyActivityList.get(dailyId);
            System.out.println("HUE" + dailyActivity);
            tvDailyName.setText(dailyActivity.getDailyActivityName());
            tvDailyName.setTextColor(color);
            tvDailyDescription.setText(dailyActivity.getDescription());
            tvDailyStartDate.setText(dailyActivity.getStartDateOnly());
            tvDailyStartHour.setText(dailyActivity.getStartHour());
            tvDailyEndHour.setText(dailyActivity.getEndHour());
            tvDailyApprove.setText(dailyActivity.getUserApproveName());
            tvDailyDeadline.setText(dailyActivity.getDeadlineDateOnly());
            tvDailyStatus.setText(dailyActivity.getDailyActivityStatusText().toUpperCase());
            tvUserCreator.setText(dailyActivity.getUsersCreator());
            if (dailyActivity.getApprovable().equalsIgnoreCase("1")) {
                btnAccept.setVisibility(View.VISIBLE);
                btnReject.setVisibility(View.VISIBLE);
                btnCreateNew.setVisibility(View.GONE);
                btnCreateCopy.setVisibility(View.GONE);
            } else {
                btnAccept.setVisibility(View.GONE);
                btnReject.setVisibility(View.GONE);
                btnCreateNew.setVisibility(View.VISIBLE);
                btnCreateCopy.setVisibility(View.VISIBLE);
            }

            if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("in_progress")) {
                imageRibbon.setImageResource(R.drawable.ribbon_blue);
                tvDailyStatus.setText(dailyActivity.getDailyActivityStatusText().toUpperCase());
            }

            if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("pending")) {
                imageRibbon.setImageResource(R.drawable.ribbon_orange);
                tvDailyStatus.setText(dailyActivity.getDailyActivityStatusText().toUpperCase());
            }

            if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("active")) {
                imageRibbon.setImageResource(R.drawable.ribbon_blue);
                tvDailyStatus.setText(dailyActivity.getDailyActivityStatusText().toUpperCase());
            }

            if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("accept")) {
                imageRibbon.setImageResource(R.drawable.ribbon_green);
                tvDailyStatus.setText(dailyActivity.getDailyActivityStatusText().toUpperCase());
                btnAccept.setVisibility(View.GONE);
                btnReject.setVisibility(View.GONE);
                btnCreateNew.setVisibility(View.GONE);
                btnCreateCopy.setVisibility(View.VISIBLE);
            }
            if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("reject")) {
                imageRibbon.setImageResource(R.drawable.ribbon_red);
                tvDailyStatus.setText(dailyActivity.getDailyActivityStatusText().toUpperCase());
                btnAccept.setVisibility(View.GONE);
                btnReject.setVisibility(View.GONE);
                btnCreateNew.setVisibility(View.GONE);
                btnCreateCopy.setVisibility(View.VISIBLE);
            }
        }

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_accept_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_accept_yes));
                messageDialog.setHeader("Apakah anda yakin ingin menyetujui tugas ini?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "ACCEPT";
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusDaily();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_reject_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_reject_yes));
                messageDialog.setHeader("Apakah anda yakin ingin menolak tugas ini?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "REJECT";
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusDaily();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });

        btnCreateCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(activity, NewDailyActivity.class);
                intent.putExtra("dailyId", dailyId);
                startActivityForResult(intent, NEW_DAILY_REQUEST);
            }
        });

        btnCreateNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(activity, NewDailyActivity.class);
                startActivityForResult(intent, NEW_DAILY_REQUEST);
            }
        });
    }

    private void doApiUpdateStatusDaily() {
        try {
            apiIProms = ApiIProms.factory(this, "UpdateStatusDaily");
            HashMap postData = new HashMap();
            postData.put("reason", reason);
            postData.put("daily_id", dailyIdList);
            postData.put("daily_status", statusTask);
            System.out.println(postData);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            closeDialog(activity);
                            finish();
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }

    }

}
