package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextInputEditText;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.itant.iproms.mapp.mObject.UserPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends MAppCompat {
    private Button btnLogin;
    private ConstraintLayout layoutMain;
    private ApiIProms apiIProms;
    private MSharedData sharedData;
    private Activity activity;
    private String loginUsername, loginPassword;
    private EditText edtUsername, edtPassword;
    private TextInputLayout inputUsername, inputPassword;
    private MTextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        activity = this;
        sharedData = MSharedData.getInstance(this);
        layoutMain = findViewById(R.id.layout_login);
        edtUsername = findViewById(R.id.edit_text_username);
        edtPassword = findViewById(R.id.edit_text_password);
        inputUsername = findViewById(R.id.text_input_username);
        inputPassword = findViewById(R.id.text_input_password);
        tvVersion = findViewById(R.id.tv_login_version);

        //get ANDROID VERSION
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionText = "";
            String versionCode = String.valueOf(pInfo.versionCode);
            for (int i = 0; i < versionCode.length(); i++) {
                versionCode.substring(i, i + 1);
                if (i + 1 == versionCode.length()) {
                    versionText = versionText + versionCode.substring(i, i + 1);
                } else {
                    versionText = versionText + versionCode.substring(i, i + 1) + ".";

                }
            }
            tvVersion.setText("App Version " + versionText);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        btnLogin = findViewById(R.id.button_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;
                loginUsername = edtUsername.getText().toString();
                loginPassword = edtPassword.getText().toString();

                if (loginUsername.length() <= 0) {
                    err++;
                    inputPassword.setError("Username harus diisi");
                }

                if (loginPassword.length() <= 0) {
                    err++;
                    inputPassword.setError("Password harus diisi");
                }

                if (err == 0) {
                    doApiLogin();
                }
            }
        });

    }

    private void doApiLogin() {
        openDialogWhite(activity);
        try {
            apiIProms = ApiIProms.factory(this, "Login");
            HashMap postData = new HashMap();
            postData.put("username", loginUsername);
            postData.put("password", loginPassword);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            String userCompleteName = data.getString("first_name") + " " + data.getString("last_name");
                            String username = data.getString("username");
                            String userId = data.getString("user_id");
                            String userToken = data.getString("user_token");
                            String userEmail = data.getString("email");
                            String permissions = data.getString("permissions");
                            String userParentId = data.getString("users_parent_id");

                            User user = User.getInstance(activity);
                            user.setUserId(userId);
                            user.setUsername(username);
                            user.setEmail(userEmail);
                            user.setUserParentId(userParentId);
                            user.setFirstName(data.getString("first_name"));
                            user.setLastName(data.getString("last_name"));
                            user.setMyProject(data.getString("my_project"));
                            user.setMyTask(data.getString("my_task_pending"));
                            user.setWeekTask(data.getString("deadline_this_week"));
                            sharedData.set(SharedData.USERNAME, username);
                            sharedData.set(SharedData.USER_ID, userId);
                            sharedData.set(SharedData.USER_TOKEN, userToken);
                            sharedData.set(SharedData.USER_COMPLETE_NAME, userCompleteName);
                            sharedData.set(SharedData.USER_PARENT_ID, userParentId);

                            UserPermission userPermission = UserPermission.getInstance();
                            userPermission.setRawData(permissions);
                            userPermission.process();

                            Intent mainActivity = new Intent(activity, MainActivity.class);
                            closeDialogWhite(activity);
                            startActivity(mainActivity);
                            finish();
                        } else {
                            closeDialogWhite(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        closeDialogWhite(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialogWhite(activity);
                }
            });
        } catch (MApiException e1) {
            closeDialogWhite(activity);
            e1.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();
    }
}
