package com.itant.iproms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterPageDailyActivity;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.TaskStatusDialog;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.DailyActivityList;
import com.itant.iproms.lib.object.MultiSelect;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyDailyActivity extends MAppCompat {
    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private RelativeLayout layoutMain;
    private FloatingActionButton fabNewDailyActivity;
    private Button btnAddNewDailyActivity, btnAccept, btnReject;
    public static final int NEW_DAILY_REQUEST = 76;
    public static final int RESULT_EDIT_SUCCESS = 77;
    private TabLayout.Tab tab1, tab2;
    private TabLayout tabLayout;
    public static Activity myActivity;
    private String statusTask, reason;
    private ArrayList<String> dailyId;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_daily);
        activity = this;
        myActivity = this;
        layoutMain = findViewById(R.id.layout_my_daily_activity);
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("KINERJA HARIAN");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        dailyRecyclerView = findViewById(R.id.daily_activity_recycler_view);
//        mLayoutManager = new GridLayoutManager(activity, 1);
//        dailyRecyclerView.setLayoutManager(mLayoutManager);
//        dailyRecyclerView.setItemAnimator(new DefaultItemAnimator());

        final MultiSelect multiSelect = MultiSelect.getInstance(activity);
        multiSelect.setToggleOn(false);
        multiSelect.clearSelectedDaily();

        // Set up the ViewPager with the sections adapter.=
        fabNewDailyActivity = findViewById(R.id.fab_add_my_daily_activity);
        fabNewDailyActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, NewDailyActivity.class);
                startActivityForResult(intent, NEW_DAILY_REQUEST);
            }
        });
        btnAddNewDailyActivity = findViewById(R.id.btn_new_daily_activity);
        btnAddNewDailyActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, NewDailyActivity.class);
                startActivityForResult(intent, NEW_DAILY_REQUEST);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            }
        });

        btnAccept = myActivity.findViewById(R.id.btn_accept_daily_activity);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_accept_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_accept_yes));
                messageDialog.setHeader("Apakah anda yakin ingin menyetujui tugas ini?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "ACCEPT";
                        dailyId = multiSelect.getSelectedDaily();
                        multiSelect.setToggleOn(false);
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusDaily();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });
        btnReject = myActivity.findViewById(R.id.btn_reject_daily_activity);
        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                messageDialog.setMessageNo(getResources().getString(R.string.task_reject_no));
                messageDialog.setMessageYes(getResources().getString(R.string.task_reject_yes));
                messageDialog.setHeader("Apakah anda yakin ingin menolak tugas ini?");
                Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statusTask = "REJECT";
                        dailyId = multiSelect.getSelectedDaily();
                        multiSelect.setToggleOn(false);
                        openDialog(activity);
                        reason = messageDialog.getMessage();
                        doApiUpdateStatusDaily();
                        messageDialog.dismiss();
                    }
                });
                Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        });
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tab1 = tabLayout.newTab().setText("Meminta Persetujuan");
        tab2 = tabLayout.newTab().setText("Pekerjaan Saya");
        tabLayout.addTab(tab2);
        tabLayout.addTab(tab1);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        viewPager = (ViewPager) findViewById(R.id.view_pager_daily);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
//        // Set Tab Listeners
//        tab1.setTabListener(new TabListener(fragmentTab1));
//        tab2.setTabListener(new TabListener(fragmentTab2));

//
//        swipeDaily = findViewById(R.id.swipe_task);
//        swipeDaily.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                doApiDailyACtivity("0");
//            }
//        });

//        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager_daily);
//        final AdapterPageDailyActivity adapter = new AdapterPageDailyActivity
//                (getSupportFragmentManager(), tabLayout.getTabCount());
//        viewPager.setAdapter(adapter);
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
        doApiDailyACtivity("0", false);
    }


    public void doApiDailyACtivity(final String page, final Boolean fromRefresh) {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "DailyList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("status", "");
            postData.put("start_date", "");
            postData.put("end_date", "");
            postData.put("user", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        DailyActivity dailyActivity = DailyActivity.getInstance(activity);
                        DailyActivityList dailyActivityList = DailyActivityList.getInstance(activity);
                        ArrayList<DailyActivity> tempArraylist = new ArrayList<DailyActivity>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            System.out.println(resultsArr);
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject dailyActivityObj = (JSONObject) resultsArr.get(i);
                                dailyActivity = new DailyActivity();

                                if (dailyActivityObj.getString("daily_activity_id").length() > 0) {
                                    pageCount += 1;
                                }

                                dailyActivity.setId(dailyActivityObj.getString("daily_activity_id"));
                                dailyActivity.setUsersId(dailyActivityObj.getString("users_id"));
                                dailyActivity.setCreated(dailyActivityObj.getString("createdby"));
                                dailyActivity.setCreatedDate(dailyActivityObj.getString("created"));
                                dailyActivity.setDescription(dailyActivityObj.getString("description"));
                                dailyActivity.setDailyActivityCode(dailyActivityObj.getString("daily_activity_code"));
                                dailyActivity.setDailyActivityName(dailyActivityObj.getString("daily_activity_name"));
                                dailyActivity.setDailyActivityStatus(dailyActivityObj.getString("daily_activity_status"));
                                dailyActivity.setStartDate(dailyActivityObj.getString("start_date"));
                                dailyActivity.setEndDate(dailyActivityObj.getString("end_date"));
                                dailyActivity.setDeadlineDate(dailyActivityObj.getString("deadline_date"));
                                dailyActivity.setUserApprove(dailyActivityObj.getString("users_id_approve"));
                                dailyActivity.setUserApproveName(dailyActivityObj.getString("user_approve_name"));
                                dailyActivity.setUsersCreator(dailyActivityObj.getString("user_creator"));
                                dailyActivity.setApprovable(dailyActivityObj.getString("approvable"));
                                dailyActivity.setDailyActivityReason(dailyActivityObj.getString("reason"));
                                dailyActivity.setUpdateDate(dailyActivityObj.getString("updated"));
                                dailyActivity.setUpdateBy(dailyActivityObj.getString("updatedby"));
                                tempArraylist.add(dailyActivity);

                            }

                            dailyActivity = DailyActivity.getInstance(activity);
                            dailyActivityList.setDailyActivityArrayList(tempArraylist);

                            final AdapterPageDailyActivity adapter = new AdapterPageDailyActivity
                                    (getSupportFragmentManager(), tabLayout.getTabCount());
                            viewPager.setAdapter(adapter);
                            if (fromRefresh) {
                                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                    @Override
                                    public void onTabSelected(TabLayout.Tab tab) {
                                        viewPager.setCurrentItem(tab.getPosition());
                                    }

                                    @Override
                                    public void onTabUnselected(TabLayout.Tab tab) {

                                    }

                                    @Override
                                    public void onTabReselected(TabLayout.Tab tab) {
                                        viewPager.setCurrentItem(tab.getPosition());

                                    }
                                });
                            }
                            closeDialog(activity);
                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        closeDialog(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            closeDialog(activity);
            e1.printStackTrace();
        }
    }

    private void doApiUpdateStatusDaily() {
        try {
            apiIProms = ApiIProms.factory(this, "UpdateStatusDaily");
            HashMap postData = new HashMap();
            postData.put("reason", reason);
            postData.put("daily_id", dailyId);
            postData.put("daily_status", statusTask);
            System.out.println(postData);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            closeDialog(activity);
                            doApiDailyACtivity("0", false);
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        MultiSelect multiSelect = MultiSelect.getInstance(activity);
        if (multiSelect.getToggleOn()) {
            selectionOff(activity);
            multiSelect.clearSelectedDaily();
            multiSelect.setToggleOn(false);
            doApiDailyACtivity("0", false);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        if (requestCode == NEW_DAILY_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                doApiDailyACtivity("0", false);
                Snackbar.make(layoutMain, "Kinerja Harian Berhasil Ditambahkan", Snackbar.LENGTH_SHORT).show();
            }
            if (resultCode == RESULT_EDIT_SUCCESS) {
                Snackbar.make(layoutMain, "Kinerja Harian Diubah", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    public static void selectionOn() {
        Button btnNew = myActivity.findViewById(R.id.btn_new_daily_activity);
        Button btnAccept = myActivity.findViewById(R.id.btn_accept_daily_activity);
        Button btnReject = myActivity.findViewById(R.id.btn_reject_daily_activity);
        btnNew.setVisibility(View.GONE);
        btnAccept.setVisibility(View.VISIBLE);
        btnReject.setVisibility(View.VISIBLE);
    }

    public static void selectionOff(Context context) {
        Button btnNew = myActivity.findViewById(R.id.btn_new_daily_activity);
        Button btnAccept = myActivity.findViewById(R.id.btn_accept_daily_activity);
        Button btnReject = myActivity.findViewById(R.id.btn_reject_daily_activity);
        MultiSelect multiSelect = MultiSelect.getInstance(context);
        multiSelect.clearSelectedDaily();
        btnNew.setVisibility(View.VISIBLE);
        btnAccept.setVisibility(View.GONE);
        btnReject.setVisibility(View.GONE);
    }
}
