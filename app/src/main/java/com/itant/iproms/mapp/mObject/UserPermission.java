package com.itant.iproms.mapp.mObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Raymond on 2/28/2017.
 */

public class UserPermission {

    public static UserPermission instance = null;
    protected HashMap<String, String> permissionMap;
    protected String rawData;

    public UserPermission(){
        permissionMap = new HashMap<>();
    }

    public static UserPermission getInstance(){
        if (UserPermission.instance == null) {
            UserPermission.instance = new UserPermission();
        }
        return UserPermission.instance;
    }

    public void add(String nav, String label) {
        permissionMap.put(nav, label);
    }

    public String get(String nav){
        return permissionMap.get(nav);
    }

    public Boolean isExists(String nav){
        if (permissionMap.get(nav) == null) {
            return false;
        }
        return true;
    }

    public void clearMap(){
        permissionMap = new HashMap<>();
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public void process() {
        this.clearMap();
        JSONObject permissions = null;
        try {
            permissions = new JSONObject(this.rawData);

            Iterator iterator = permissions.keys();
            while(iterator.hasNext()){
                String key = (String)iterator.next();
                JSONObject permission = permissions.getJSONObject(key);
                String name = permission.getString("name");
                this.add(key, name);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // do nothing
        }

    }

    @Override
    public String toString() {
        return "UserPermission{" +
                "permissionMap=" + permissionMap +
                ", rawData='" + rawData + '\'' +
                '}';
    }
}
