package com.itant.iproms.mapp.helper;

/**
 * Created by Raymond on 11/16/2016.
 */

public class MNumber {

    public static String thousandSeparator(String number) {
        String value = number.toString().replace(",", "");
        String reverseValue = new StringBuilder(value).reverse()
                .toString();
        StringBuilder finalValue = new StringBuilder();
        for (int i = 1; i <= reverseValue.length(); i++) {
            char val = reverseValue.charAt(i - 1);
            finalValue.append(val);
            if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                finalValue.append(",");
            }
        }
        return finalValue.reverse().toString();
    }

    public static String thousandSeparatorPlusCurrency(String number) {
        String currency = "Rp. ";
        String value = number.toString().replace(",", "");
        String reverseValue = new StringBuilder(value).reverse()
                .toString();
        StringBuilder finalValue = new StringBuilder();
        for (int i = 1; i <= reverseValue.length(); i++) {
            char val = reverseValue.charAt(i - 1);
            finalValue.append(val);
            if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                finalValue.append(",");
            }
        }
        return currency + finalValue.reverse().toString();
    }
}
