package com.itant.iproms.mapp.mApi;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Raymond on 12/31/2016.
 */

public class MApi {

    private RequestQueue requestQueue;
    private OkHttpClient okHttpClient;
    private MApiService apiService;
    private OkHttpService okHttpService;
    private Context context;
    public static final String VOLLEY = "VOLLEY";
    public static final String OKHTTP = "OKHTTP";
    public static String apiLib;

    protected String serviceName;
    protected String packageName; // com.puasmakan.cia.api.service.MApi
    protected HashMap postData;
    protected String domain;
    protected int method;

    public MApi(Context context, String serviceName) throws MApiException {
        this.context = context;
        this.serviceName = serviceName;
        this.domain = "http://www.puasmakan.com/api/";
        this.postData = new HashMap();
        if (this.apiLib == VOLLEY) {
            requestQueue = Volley.newRequestQueue(context);
            method = Request.Method.POST;
        } else if (this.apiLib == OKHTTP) {
            okHttpClient = new OkHttpClient();
        }
    }

    protected void initiate() throws MApiException {
        try {
//            if (this.apiLib == VOLLEY) {
            Constructor constructor = Class.forName(this.packageName + this.serviceName).getConstructor(MApi.class);
            apiService = (MApiService) constructor.newInstance(this);
//            } else {
//                Constructor constructor = Class.forName(this.packageName + this.serviceName).getConstructor(MApi.class);
//                okHttpService = (OkHttpService) constructor.newInstance(this);
//            }
        } catch (InstantiationException e) {
            throw new MApiException(e);
        } catch (IllegalAccessException e) {
            throw new MApiException(e);
        } catch (ClassNotFoundException e) {
            throw new MApiException(e);
        } catch (NoSuchMethodException e) {
            throw new MApiException(e);
        } catch (InvocationTargetException e) {
            throw new MApiException(e);
        }
    }

    public void execute(final ApiListener apiListener) {
        ArrayList stringRequestArrayList = apiService.execute(apiListener);
        if (this.apiLib == OKHTTP) {
            if (stringRequestArrayList != null) {
                for (int i = 0; i < stringRequestArrayList.size(); i++) {
                    okHttpClient.newCall((okhttp3.Request) stringRequestArrayList.get(i)).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            apiListener.onErrorResponse(e);
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (!response.isSuccessful()) {
                                throw new IOException("Unexpected code " + response);
                            } else {
                                apiListener.onResponse(response.body().string());
                            }
                        }
                    });
                }
            }

        } else {
            if (stringRequestArrayList != null) {
                for (int i = 0; i < stringRequestArrayList.size(); i++) {
                    requestQueue.add((StringRequest) stringRequestArrayList.get(i));
                }
            }
        }
    }

//    public void perform(OkHttpListener okHttpListener) throws IOException {
//        okHttpService.perform(okHttpListener);
//    }

    public MApiService getService() {
        return apiService;
    }

    public String getDomain() {
        return domain;
    }

    public HashMap getPostData() {
        return postData;
    }

    public void setPostData(HashMap postData) {
        this.postData = postData;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public int getMethod() {
        return method;
    }

    public interface ApiListener {
        void onResponse(String response);

//        void onErrorResponse(VolleyError error);

        void onErrorResponse(Exception error);
    }

    public Context getContext() {
        return context;
    }

    public String getApiLib() {
        return apiLib;
    }

    public void setApiLib(String apiLib) {
        this.apiLib = apiLib;
    }
}
