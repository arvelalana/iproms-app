package com.itant.iproms.mapp;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Raymond on 1/3/2017.
 */

public class MSharedData extends MShareDataString {

    public static MSharedData instance = null;
    public SharedPreferences cookies;
    public SharedPreferences.Editor cookies_editor;

    private Context context;

    public void set(String key, String value) {
        cookies_editor = cookies.edit();
        cookies_editor.putString(key, value);
        cookies_editor.commit();
    }

    public void setBoolean(String key, Boolean value) {
        cookies_editor = cookies.edit();
        cookies_editor.putBoolean(key, value);
        cookies_editor.commit();
    }


    public String get(String key) {
        return cookies.getString(key, null);
    }

    public Boolean getBoolean(String key) {
        return cookies.getBoolean(key, false);
    }

    public String get(String key, String _default) {
        return cookies.getString(key, _default);
    }

    public Boolean getBoolean(String key, Boolean _default) {
        return cookies.getBoolean(key, _default);
    }

    public MSharedData(Context context) {
        this.context = context;
        cookies = context.getSharedPreferences(MSharedData.SHARED_PREFERENCES, 0);
    }

    public static MSharedData getInstance(Context context) {
        if (instance == null) {
            instance = new MSharedData(context);
        }
        return instance;
    }
}

