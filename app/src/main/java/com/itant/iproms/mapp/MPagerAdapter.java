package com.itant.iproms.mapp;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;


import com.itant.iproms.R;
import com.itant.iproms.mapp.mComponent.MTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raymond on 2/24/2017.
 */

public class MPagerAdapter extends FragmentPagerAdapter {

    protected Context context;
    protected FragmentManager fragmentManager;
    protected final List<Fragment> mFragmentList = new ArrayList<>();
    protected final List<String> mFragmentTitleList = new ArrayList<>();
    protected final List<Integer> mFragmentTitleIcon = new ArrayList<>();

    public MPagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        this.fragmentManager = fm;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public void addFragment(MHostFragment fragment, String title, int imageIcon) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        mFragmentTitleIcon.add(imageIcon);


    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(context).inflate(R.layout.tab_title_custom, null);
        MTextView tv = (MTextView) v.findViewById(R.id.txt_title);
        tv.setText(mFragmentTitleList.get(position));

        ImageView img = (ImageView) v.findViewById(R.id.img_icon_tab);
        img.setImageResource(mFragmentTitleIcon.get(position));
        return v;
    }
}
