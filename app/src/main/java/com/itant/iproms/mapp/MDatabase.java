package com.itant.iproms.mapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.itant.iproms.mapp.helper.MDbHelper;
import com.itant.iproms.mapp.mObject.MTableInterface;

import java.util.HashMap;

/**
 * Created by Raymond on 3/22/2017.
 */

public class MDatabase {

    private static final String TAG = MDatabase.class.getSimpleName();
    public static MDatabase instance = null;
    public static final String DATABASE_NAME = "mdatabase";
    public static final int DATABASE_VERSION = 1;

    protected SQLiteDatabase database;
    protected MDbHelper dbHelper;
    protected Context context;

    public MDatabase(Context context, HashMap tables) {
        this.context = context;
        init(tables);
    }

    public static MDatabase getInstance(Context context, HashMap tables){
        if (instance == null) {
            instance = new MDatabase(context, tables);
        }
        return instance;
    }

    public static MDatabase getInstance(){
        return instance;
    }

    public void init(HashMap tables){
        dbHelper = new MDbHelper(context, DATABASE_NAME, DATABASE_VERSION, tables);
    }

    public Cursor query(String tableName, String[] columns, String where, String[] args) {
        Cursor cursor = database.query(tableName, columns, where, args, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor query(String sql, String[] args){
        Cursor cursor = database.rawQuery(sql, args);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor query(String sql){
        Cursor cursor = database.rawQuery(sql, null);
        cursor.moveToFirst();
        return cursor;
    }

    public int update(String tableName, ContentValues values, String whereClause){
        return database.update(tableName, values, whereClause, null);
    }

    public int update(String tableName, ContentValues values, String whereClause, String[] whereArgs){
        return database.update(tableName, values, whereClause, whereArgs);
    }

    public long insert(String tableName, ContentValues values) {
        long insertId = database.insert(tableName, null, values);
        return insertId;
    }


    public SQLiteDatabase open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        return database;
    }

    public void close() {
        dbHelper.close();
    }

    public void close(Cursor cursor) {
        cursor.close();
    }

    public void execSQL(String sql) {
        database.execSQL(sql);
    }

    public SQLiteStatement compileStatement(String sql) {
        return database.compileStatement(sql);
    }

    public void beginTransaction(){
        database.beginTransaction();
    }

    public void setTransactionSuccessfull(){
        database.setTransactionSuccessful();
    }

    public void endTransaction(){
        database.endTransaction();
    }



}
