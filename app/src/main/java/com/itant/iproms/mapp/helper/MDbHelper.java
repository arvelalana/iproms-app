package com.itant.iproms.mapp.helper;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.itant.iproms.mapp.mObject.MTableInterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Raymond on 3/21/2017.
 */

public class MDbHelper extends SQLiteOpenHelper {

    private static final String TAG = MDbHelper.class.getSimpleName();
    protected HashMap<String, MTableInterface> tables;

    public MDbHelper(Context context, String name, int version, HashMap tables) {
        super(context, name, null, version);
        this.tables = tables;
    }

    public MDbHelper(Context context, String name, int version, DatabaseErrorHandler errorHandler, HashMap tables) {
        super(context, name, null, version, errorHandler);
        this.tables = tables;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "OnCreate");
        Iterator iterator = tables.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry) iterator.next();

            MTableInterface mTableInterface = (MTableInterface) pair.getValue();
            mTableInterface.OnCreate(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Iterator iterator = tables.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry) iterator.next();

            MTableInterface mTableInterface = (MTableInterface) pair.getValue();
            mTableInterface.OnUpgrade(db, oldVersion, newVersion);
        }
    }
}
