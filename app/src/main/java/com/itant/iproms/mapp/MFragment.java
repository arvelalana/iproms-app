package com.itant.iproms.mapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by Raymond on 2/24/2017.
 */

public abstract class MFragment extends Fragment {

    public static final String FRAGMENT_NAME = "";

    public static boolean handleBackPressed(FragmentManager fm) {
        if (fm.getFragments() != null) {
            for (Fragment frag : fm.getFragments()) {
                if (frag != null && frag.isVisible() && frag instanceof MFragment) {
                    if (((MFragment) frag).onBackPressed()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    protected boolean onBackPressed() {
        FragmentManager fm = getChildFragmentManager();
        if (handleBackPressed(fm)) {
            return true;
        } else if (getUserVisibleHint() && fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            return true;
        }
        return false;
//        return true;
    }
}
