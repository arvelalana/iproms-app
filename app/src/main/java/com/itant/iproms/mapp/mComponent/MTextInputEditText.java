package com.itant.iproms.mapp.mComponent;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

import com.itant.iproms.R;
import com.itant.iproms.mapp.MFont;


/**
 * Created by Raymond on 11/18/2016.
 */

public class MTextInputEditText extends TextInputEditText {

    private String fontName = null;

    public MTextInputEditText(Context context) {
        super(context);
        applyDefaultFont(context);
    }

    public MTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttrs(context, attrs);
    }

    public MTextInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttrs(context, attrs);
    }

    private void readAttrs(Context context, AttributeSet attributeSet){
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.MTextView);

        String attrFontName = typedArray.getString(R.styleable.MTextView_font_name);
        if (fontName != null) {
            attrFontName = fontName;
        }

        if (attrFontName == null) {
            // set default font
            applyDefaultFont(context);
        }
        else {
            applyFont(context, attrFontName);
        }
    }

    private void applyFont(Context context, String attrFontName) {
        if (attrFontName.equalsIgnoreCase(context.getResources().getString(R.string.default_font))) {
            applyDefaultFont(context);
        }
    }

    private void applyDefaultFont(Context context) {
        Typeface customFont = MFont.getInstance(context).getFont("default");
        setTypeface(customFont);
    }



    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }
}
