package com.itant.iproms.mapp.helper;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by Raymond on 11/24/2016.
 */

public class Metric {

    public static int GetPixelFromDips(Context context, float pixels) {
        // Get the screen's density scale
        final float scale = context.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    /**
     * Converting dp to pixel
     */
    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
