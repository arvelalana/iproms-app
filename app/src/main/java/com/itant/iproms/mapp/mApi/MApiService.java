package com.itant.iproms.mapp.mApi;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by Raymond on 12/31/2016.
 */

public class MApiService {

    private static final String TAG = MApiService.class.getSimpleName();

    protected MApi api;
    protected String postData = null;
    protected String urlServer;
    protected String request;

    protected MApi.ApiListener apiListener;

    public MApiService(MApi api) {
        this.api = api;
    }

    public ArrayList execute(MApi.ApiListener apiListener) {
        this.apiListener = apiListener;
        // overwrite this method
        return null;
    }

    protected Object doRequest() {
        Object stringRequest = null;
        if (this.api.apiLib == MApi.OKHTTP) {
            stringRequest = new Request.Builder()
                    .url(urlServer)
                    .build();
        } else {
            stringRequest = new StringRequest(api.getMethod(), urlServer, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    apiListener.onResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    apiListener.onErrorResponse(error);
                }
            }) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return request == null ? null : request.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", request, "utf-8");
                        return null;
                    }
                }
            };
            ((StringRequest) stringRequest).setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        }
        return stringRequest;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }


}
