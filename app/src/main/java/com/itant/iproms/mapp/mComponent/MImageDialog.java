package com.itant.iproms.mapp.mComponent;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.itant.iproms.R;

/**
 * Created by user on 2/17/2017.
 */

public class MImageDialog extends Dialog {


    protected ImageView imageView, closeImage;
    protected com.itant.iproms.mapp.mComponent.MTextView tvHeader;
    protected Context context;

    public MImageDialog(Context _context, int themeResId) {
        super(_context, themeResId);
        this.context = _context;
        init();
    }

    public void init() {
        setContentView(R.layout.dialog_pop_up_image);
        imageView = (ImageView) findViewById(R.id.image_main_pop_up);
        tvHeader=(MTextView) findViewById(R.id.tv_image_pop_up_header);
        closeImage = (ImageView) findViewById(R.id.image_close_pop_up);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setImageView(String url) {
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(imageView);
    }

    public void setTvHeader(MTextView tvHeader) {
        this.tvHeader = tvHeader;
    }

    public MTextView getTvHeader() {
        return tvHeader;
    }

    public ImageView getCloseImage() {
        return closeImage;
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }
}
