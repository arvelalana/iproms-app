package com.itant.iproms.mapp.mObject;

import java.util.HashMap;

/**
 * Created by Raymond on 3/23/2017.
 */

public class MDatabaseTables {

    protected HashMap<String, MTableInterface> tables;

    public MDatabaseTables(){
        tables = new HashMap<>();
    }

    public void addTable(String name, MTableInterface mTableInterface){
        tables.put(name, mTableInterface);
    }

    public HashMap<String, MTableInterface> getTables() {
        return tables;
    }
}
