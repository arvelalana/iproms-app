package com.itant.iproms.mapp.mComponent;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.itant.iproms.R;


/**
 * Created by Raymond on 1/10/2017.
 */

public class MDialog extends Dialog {

    public static String BTN_YESNO = "yesno";
    public static String BTN_OK = "ok";
    public static String BTN_NONE = "none";
    public static String POPUP_PASSWORD = "password";


    protected String message;
    protected MTextView tvMessageDialog;
    protected LinearLayout llBtnOk, llPopupPassword, llBtnYesNo;
    protected FrameLayout frameLayoutCustomView;
    protected Button btnOk, btnYes, btnNo, btnConfirm, btnCancel;
    protected String buttonType;
    protected View viewContent;
    protected EditText ediTextPopupPassword;

    public MDialog(Context context) {
        super(context);
        init();
    }

    public MDialog(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    protected void init() {
        setContentView(R.layout.dialog_message);

        frameLayoutCustomView = (FrameLayout) findViewById(R.id.frame_layout_custom_view);
        System.out.println(frameLayoutCustomView);
        llBtnOk = (LinearLayout) findViewById(R.id.ll_btn_ok);
        llBtnYesNo = (LinearLayout) findViewById(R.id.ll_btn_yes_no);
        llPopupPassword = (LinearLayout) findViewById(R.id.ll_popup_password);
        tvMessageDialog = (MTextView) findViewById(R.id.tv_message_dialog);
        tvMessageDialog.setTextSize(16);
        tvMessageDialog.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
        btnOk = (Button) findViewById(R.id.btn_ok);
        btnYes = (Button) findViewById(R.id.btn_yes);
        btnNo = (Button) findViewById(R.id.btn_no);
        llBtnOk.setVisibility(View.GONE);
        llBtnYesNo.setVisibility(View.GONE);
        llPopupPassword.setVisibility(View.GONE);


        btnConfirm = (Button) findViewById(R.id.btn_password_popup_confirm);
        btnCancel = (Button) findViewById(R.id.btn_password_popup_cancel);
        ediTextPopupPassword = (EditText) findViewById(R.id.edit_text_popup_password);
    }

    public void setContent(int viewId) {
        viewContent = LayoutInflater.from(getContext()).inflate(viewId, frameLayoutCustomView, false);
        frameLayoutCustomView.addView(viewContent);
    }

    public View getViewContent() {
        return viewContent;
    }

    @Override
    public void show() {
        tvMessageDialog.setText(message);
        if (buttonType == null) {
            llBtnOk.setVisibility(View.VISIBLE);
        } else {
            if (buttonType.equalsIgnoreCase(this.BTN_OK)) {
                llBtnOk.setVisibility(View.VISIBLE);
            } else if (buttonType.equalsIgnoreCase(this.BTN_YESNO)) {
                llBtnYesNo.setVisibility(View.VISIBLE);
            } else if (buttonType.equalsIgnoreCase(this.BTN_NONE)) {
                // do nothing
            } else if (buttonType.equalsIgnoreCase(this.POPUP_PASSWORD)) {
                llPopupPassword.setVisibility(View.VISIBLE);
            }
        }
        super.show();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Button getBtnOk() {
        return btnOk;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public Button getBtnCancel() {
        return btnCancel;
    }

    public Button getBtnConfirm() {
        return btnConfirm;
    }

    public Button getBtnYes() {
        return btnYes;
    }

    public Button getBtnNo() {
        return btnNo;
    }

    public EditText getEdiTextPopupPassword() {
        return ediTextPopupPassword;
    }
}

