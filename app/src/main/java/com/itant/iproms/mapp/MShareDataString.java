package com.itant.iproms.mapp;

/**
 * Created by Raymond on 1/5/2017.
 */

abstract public class MShareDataString {

    public static final String SHARED_PREFERENCES = "com.itant.iproms.appdata";
    public static final String SESSION_ID = "session_id";
    public static final String DATA_WRITTEN = "data_written";

    public static final String USER_LOGIN = "user_login";
    public static final String USER_ID = "user_id";
    public static final String ROLE_ID = "role_id";
    public static final String RESTAURANT_ID = "restaurant_id";
    public static final String IS_ROOT = "is_root";

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String LAST_REQUEST = "last_request";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";


    public static final String __SETTLEMENT = "settlement";
    public static final String __SETTLEMENT_TYPE = "default";
    public static final String __SETTLEMENT_DEFAULT = "default";
    public static final String __SETTLEMENT_DATE = "date";
    public static final String __SETTLEMENT_ITEM = "item";
    public static final String __SETTLEMENT_FOOD_CATEGORY = "food_category_per_item";


    public static final String MENU_COUNT = "menu_count";
    public static final String MAIN_MENU_1 = "main_menu_1";
    public static final String MAIN_MENU_2 = "main_menu_2";
    public static final String MAIN_MENU_3 = "main_menu_3";
    public static final String MAIN_MENU_4 = "main_menu_4";
    public static final String MAIN_MENU_5 = "main_menu_5";
    public static final String MAIN_MENU_6 = "main_menu_6";
}
