package com.itant.iproms.mapp;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * Created by Raymond on 11/2/2016.
 */

public class MFont {

    public static MFont instance = null;

    protected Context context;
    protected HashMap<String, String> fontList;
    protected HashMap<String, Typeface> fontCache = new HashMap<>();

    public MFont(Context context){
        fontList = new HashMap();
        this.context = context;
    }

    public static MFont getInstance(Context context){
        if (instance == null) {
            instance = new MFont(context);
        }
        return instance;
    }

    public void addFont(String name, String location){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), location);
        fontCache.put(name, typeface);
        fontList.put(name, location);
    }

    public Typeface getFont(String fontname) {
        Typeface typeface = fontCache.get(fontname);
        return typeface;
    }


//    public static Typeface opensansSemibold(Context context){
//        Typeface customFont = MFontCache.getTypeface("fonts/open-sans.semibold.ttf", context);
//        return customFont;
//    }
//
//    public static Typeface opensans(Context context) {
//        Typeface customFont = MFontCache.getTypeface("fonts/open-sans.regular.ttf", context);
//        return customFont;
//    }

}
