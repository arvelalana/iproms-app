package com.itant.iproms.mapp.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Raymond on 11/24/2016.
 */

public class MDate {

    public static String formatDateFromString(String inputFormat, String outputFormat, String inputDate) throws ParseException {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            throw e;
        }

        return outputDate;

    }

    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public static HashMap getDifference(Date startDate, Date endDate){

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("different", String.valueOf(different));

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long totalElapsedSeconds = different / secondsInMilli;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;


        hashMap.put("elapsedDays", String.valueOf(elapsedDays));
        hashMap.put("elapsedHours", String.valueOf(elapsedHours));
        hashMap.put("elapsedMinutes", String.valueOf(elapsedMinutes));
        hashMap.put("elapsedSeconds", String.valueOf(elapsedSeconds));
        hashMap.put("totalElapsedSeconds", String.valueOf(totalElapsedSeconds));
        return hashMap;
    }

}
