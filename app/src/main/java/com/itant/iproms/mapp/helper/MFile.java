package com.itant.iproms.mapp.helper;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Raymond on 3/15/2017.
 */

public class MFile {

    private static final String TAG = MFile.class.getSimpleName();
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /**
     * Creating file uri to store image/video
     */
    public static Uri getOutputMediaFileUri(int type, String imageDirectoryName) {
        return Uri.fromFile(getOutputMediaFile(type, imageDirectoryName));
    }
//    Config.IMAGE_DIRECTORY_NAME
    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type, String imageDirectoryName) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),imageDirectoryName);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create " + imageDirectoryName + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
}
