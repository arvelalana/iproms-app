package com.itant.iproms.mapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.itant.iproms.R;

/**
 * Created by Raymond on 2/24/2017.
 */

public class MHostFragment extends MFragment {

    private Fragment fragment;

    public void replaceFragment(Fragment fragment, boolean addToBackstack) {
        if (addToBackstack) {
            getChildFragmentManager().beginTransaction().replace(R.id.hosted_fragment, fragment).addToBackStack(null).commit();
        } else {
            getChildFragmentManager().beginTransaction().replace(R.id.hosted_fragment, fragment).commit();
        }
    }

    public void  replaceFragment(int containerViewId, Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(containerViewId, fragment);
        transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }

    public void replaceFragment(int containerViewId, Fragment someFragment, String tag) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(containerViewId, someFragment, tag);
        transaction.addToBackStack(null);
        transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }

    public void replaceFragment(int containerViewId, Fragment someFragment, String tag, Boolean addToBackStack) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(containerViewId, someFragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }

    public boolean popFragment() {
        boolean isPop = false;
        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            isPop = true;
            getChildFragmentManager().popBackStack();
        }
        return isPop;
    }

}
