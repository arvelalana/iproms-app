package com.itant.iproms.mapp.mComponent;

import android.app.ProgressDialog;
import android.content.Context;


/**
 * Created by Raymond on 2/28/2017.
 */

public class MProgressDialog extends ProgressDialog {

    public MProgressDialog(Context context) {
        super(context);
    }

    public MProgressDialog(Context context, int theme) {
        super(context, theme);
    }


    public void show(){
        super.setMessage("Loading");
        super.show();
    }
}
