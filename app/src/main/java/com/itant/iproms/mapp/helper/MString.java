package com.itant.iproms.mapp.helper;

import android.widget.AutoCompleteTextView;

import com.itant.iproms.mapp.mComponent.MTextInputEditText;

/**
 * Created by Raymond on 2/22/2017.
 */

public class MString {

    public static String getText(MTextInputEditText editText){
        return editText.getText().toString().trim();
    }

    public static String getText(AutoCompleteTextView editText){
        return editText.getText().toString().trim();
    }
}
