package com.itant.iproms.mapp.mObject;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Raymond on 3/23/2017.
 */

public interface MTableInterface {

    public void OnCreate(SQLiteDatabase db);
    public void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);
}
