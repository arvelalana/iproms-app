package com.itant.iproms.mapp.mApi;

/**
 * Created by Raymond on 10/28/2016.
 */

public class MApiException extends Exception {

    public MApiException() {
        super();
    }

    public MApiException(String detailMessage) {
        super(detailMessage);
    }

    public MApiException(String detailMessage, Throwable throwable) {
        super(detailMessage,throwable);
    }

    public MApiException(Throwable throwable) {
        super(throwable);
    }

}
