package com.itant.iproms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterTask;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.FilterDialog;
import com.itant.iproms.lib.dialog.SortDialog;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.itant.iproms.NewTaskActivity.NEW_TASK_ADDED;

public class MyTaskActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener {

    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle, resetFilter;
    private FilterDialog filterDialog;
    private SortDialog sortDialog;
    private Button buttonFilter, buttonSort, buttonSeePerProject, buttonSortOk, buttonFilterOk;
    private ApiIProms apiIProms;
    private AdapterTask adapterTask;
    private RecyclerView taskRecyclerView;
    private ConstraintLayout layoutMain;
    private GridLayoutManager mLayoutManager;
    private String projectId;
    private Boolean isOpen = false;
    private String dateMode;
    private EditText editTextStartDate, editTextEndDate, editTextStartDeadline, editTextEndDeadline;
    private String filterStartDate, filterEndDate, filterStartDeadline, filterEndDeadline;
    private FlexboxLayout flexboxLayout;
    private int SEE_PER_PROJECT = 110;
    private RadioGroup radioGroupSort, radioGroupFilter;
    private String sortMode, deadlineDateMode;
    private SearchView searchViewTask;
    private int totalCheckBox, totalChecked;
    private Filter filter;
    private Boolean isThisDate;
    private RadioButton radioToday, radioThisWeek, radioThisMonth, radioThisDate;
    private ImageView imageViewError, imageViewNoTask;
    private SwipeRefreshLayout swipeTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_task);
        activity = this;
        projectId = "";
        sortMode = "";
        deadlineDateMode = "";
        filterStartDate = "";
        filterEndDate = "";
        filterStartDeadline = "";
        filterEndDeadline = "";
        isThisDate = false;
        totalCheckBox = 0;
        layoutMain = findViewById(R.id.layout_my_task);

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("DAFTAR TUGAS SAYA");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonFilter = findViewById(R.id.button_filter);
        buttonSort = findViewById(R.id.button_sort);
        buttonSeePerProject = findViewById(R.id.button_see_per_project);
        imageViewError = findViewById(R.id.image_view_error);
        imageViewNoTask = findViewById(R.id.image_view_no_task);
        swipeTask = findViewById(R.id.swipe_task);

        swipeTask.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doApiTaskFilter("0");
            }
        });

        //FILTER DIALOG
        filter = Filter.getInstance(activity);
        filter.clear();
        filterDialog = new FilterDialog(activity);

        resetFilter = filterDialog.findViewById(R.id.tv_reset_filter);
        buttonFilterOk = filterDialog.findViewById(R.id.btn_filter_ok);
        editTextStartDate = filterDialog.findViewById(R.id.edit_text_filter_task_start_date);
        editTextEndDate = filterDialog.findViewById(R.id.edit_text_filter_task_end_date);
        editTextStartDeadline = filterDialog.findViewById(R.id.edit_text_filter_task_start_deadline);
        editTextEndDeadline = filterDialog.findViewById(R.id.edit_text_filter_task_end_deadline);

        radioToday = filterDialog.findViewById(R.id.radio_today);
        radioThisWeek = filterDialog.findViewById(R.id.radio_this_week);
        radioThisMonth = filterDialog.findViewById(R.id.radio_this_month);
        radioThisDate = filterDialog.findViewById(R.id.radio_this_date);
        radioGroupFilter = filterDialog.findViewById(R.id.radio_group_filter);
        if (filter.getDeadlineDateMode() != null) {
            if (filter.getDeadlineDateMode().equalsIgnoreCase("today")) {
                radioToday.setChecked(true);
            }

            if (filter.getDeadlineDateMode().equalsIgnoreCase("this_week")) {
                radioThisWeek.setChecked(true);
            }

            if (filter.getDeadlineDateMode().equalsIgnoreCase("this_month")) {
                radioThisMonth.setChecked(true);
            }

            if (filter.getDeadlineDateMode().equalsIgnoreCase("this_date")) {
                radioThisDate.setChecked(true);
                editTextStartDeadline.setText(filter.getDeadlineStartDate());
                editTextEndDeadline.setText(filter.getDeadlineEndDate());
            }
        }
        editTextStartDate.setText(filter.getCreatedStartDate());
        editTextEndDate.setText(filter.getCreatedEndDate());


        resetFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFilter();
            }
        });

        final DateFormat df = new DateFormat();
        final Calendar c = Calendar.getInstance();
        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextStartDeadline.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start_deadline";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDeadline.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end_deadline";
                    datePickerOnClick();
                }
                return false;
            }
        });
        radioGroupFilter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int id) {
                switch (id) {
                    case R.id.radio_today:
                        if (radioToday.isChecked()) {
                            isThisDate = false;
                            editTextStartDeadline.setEnabled(false);
                            editTextEndDeadline.setEnabled(false);
                            radioThisDate.setChecked(false);
                            radioToday.setChecked(true);
                            filterStartDeadline = df.format("dd-MM-yyyy", c.getInstance().getTime()).toString();
                            c.set(Calendar.DATE, Calendar.DATE);
                            filterEndDeadline = df.format("dd-MM-yyyy", c.getInstance().getTime()).toString();
                            deadlineDateMode = "today";
                        }
                        break;
                    case R.id.radio_this_week:
                        if (radioThisWeek.isChecked()) {
                            isThisDate = false;
                            editTextStartDeadline.setEnabled(false);
                            editTextEndDeadline.setEnabled(false);
                            radioThisDate.setChecked(false);
                            radioThisWeek.setChecked(true);
                            c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                            filterStartDeadline = df.format("dd-MM-yyyy", c.getTime()).toString();
                            c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                            filterEndDeadline = df.format("dd-MM-yyyy", c.getTime()).toString();
                            deadlineDateMode = "this_week";
                        }
                        break;
                    case R.id.radio_this_month:
                        if (radioThisMonth.isChecked()) {
                            isThisDate = false;
                            editTextStartDeadline.setEnabled(false);
                            editTextEndDeadline.setEnabled(false);
                            radioThisDate.setChecked(false);
                            radioThisMonth.setChecked(true);
                            c.set(Calendar.DATE, c.getActualMinimum(Calendar.DAY_OF_MONTH));
                            filterStartDeadline = df.format("dd-MM-yyyy", c.getTime()).toString();
                            c.set(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                            filterEndDeadline = df.format("dd-MM-yyyy", c.getTime()).toString();
                            deadlineDateMode = "this_month";
                        }
                        break;

                }
            }
        });

        radioThisDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    radioGroupFilter.clearCheck();
                    radioToday.setChecked(false);
                    radioThisWeek.setChecked(false);
                    radioThisMonth.setChecked(false);
                    radioThisDate.setChecked(true);
                    isThisDate = true;
                    editTextStartDeadline.setEnabled(true);
                    editTextEndDeadline.setEnabled(true);
                    deadlineDateMode = "this_date";
                }

            }
        });


        flexboxLayout = filterDialog.findViewById(R.id.ll_checkbox_project_list);
        buttonFilterOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //GET CHECKED PROJECT
                totalChecked = 0;
                ProjectList projectList = ProjectList.getInstance(activity);
                filter.clearCheckedProjectList();
                for (int i = 0; i < flexboxLayout.getChildCount(); i++) {
                    View nextChild = flexboxLayout.getChildAt(i);
                    CheckBox check = (CheckBox) nextChild;
                    if (check.isChecked()) {
                        totalChecked++;
                        System.out.println(i);
                        filter.addCheckedProjectList(projectList.getPosition(i));
                    }
                }
                System.out.println(filter.getAllCheckedId());

                //GET TASK CREATED DATE
                filterStartDate = editTextStartDate.getText().toString();
                filterEndDate = editTextEndDate.getText().toString();
                if (filterStartDate.length() > 0 && filterEndDate.length() > 0) {
                    filter.setCreatedStartDate(filterStartDate);
                    filter.setCreatedEndDate(filterEndDate);
                }

                //GET TASK DEADLINE DATE
                if (isThisDate) {
                    filterStartDeadline = editTextStartDeadline.getText().toString();
                    filterEndDeadline = editTextEndDeadline.getText().toString();
                }
                filter.setDeadlineStartDate(filterStartDeadline);
                filter.setDeadlineEndDate(filterEndDeadline);
                filter.setDeadlineDateMode(deadlineDateMode);

                doApiTaskFilter("0");
                System.out.println(filterStartDeadline + '+' + filterEndDeadline);
            }
        });


        //SORT DIALOG
        sortDialog = new SortDialog(activity);
        buttonSortOk = sortDialog.findViewById(R.id.button_sort_ok);
        radioGroupSort = sortDialog.findViewById(R.id.radio_group_sort);
        radioGroupSort.check(R.id.radio_start_sort_asc);
        radioGroupSort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int id) {
                switch (id) {
                    case R.id.radio_start_sort_asc:
                        sortMode = "start_asc";
                        break;
                    case R.id.radio_start_sort_desc:
                        sortMode = "start_desc";
                        break;
                    case R.id.radio_deadline_sort_asc:
                        sortMode = "deadline_asc";
                        break;
                    case R.id.radio_deadline_sort_desc:
                        sortMode = "deadline_desc";
                        break;
                }
            }
        });
        buttonSortOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doApiTaskFilter("0");
                sortDialog.dismiss();
            }
        });


        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(activity);

                editTextStartDate.clearFocus();
                editTextEndDate.clearFocus();
                editTextStartDeadline.clearFocus();
                editTextEndDeadline.clearFocus();
                doApiProject("0");

            }
        });
        buttonSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sortDialog.show();
            }
        });
        buttonSeePerProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MyProjectActivity.class);
                intent.putExtra("mode", "see_per_project");
                activity.startActivityForResult(intent, SEE_PER_PROJECT);
            }
        });

        taskRecyclerView = findViewById(R.id.task_recycler_view);
        mLayoutManager = new GridLayoutManager(activity, 1);
        taskRecyclerView.setLayoutManager(mLayoutManager);
        taskRecyclerView.setItemAnimator(new DefaultItemAnimator());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            projectId = bundle.getString("projectId");
            Filter filter = Filter.getInstance(activity);
            ProjectList projectList = ProjectList.getInstance(activity);
            filter.addCheckedProjectList(projectList.get(projectId));
            doApiTaskFilter("0");
        } else {
            doApiTaskFilter("0");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ON RESUME TRIGERRED");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        doApiTaskFilter("0");
    }

    private void doApiTask(final String page) {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "TaskList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("project_id", projectId);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Task task = Task.getInstance(activity);
                        TaskList taskListArr = TaskList.getInstance(activity);
                        ArrayList<Task> tempArraylist = new ArrayList<Task>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject taskList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = taskList.getJSONArray("users");
                                task = new Task();

                                if (taskList.getString("to_do_list_id").length() > 0) {
                                    pageCount += 1;
                                }

                                task.setId(taskList.getString("to_do_list_id"));
                                task.setCreated(taskList.getString("createdby"));
                                task.setCreatedDate(taskList.getString("created"));
                                task.setDescription(taskList.getString("description"));
                                task.setTaskCode(taskList.getString("to_do_list_code"));
                                task.setTaskName(taskList.getString("to_do_list_name"));
                                task.setTaskStatus(taskList.getString("to_do_list_status"));
                                task.setProjectName(taskList.getString("project_name"));
                                task.setProjectDate(taskList.getString("project_created_date"));
                                task.setProjectCreator(taskList.getString("project_created_by"));
                                task.setStartDate(taskList.getString("plan_start_date"));
                                task.setEndDate(taskList.getString("plan_end_date"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    userList.add(user);
                                }

                                task.setUserList(userList);
                                tempArraylist.add(task);
                            }

                            task = Task.getInstance(activity);

                            taskListArr.setTaskArrayList(tempArraylist);
                            adapterTask = new AdapterTask(activity, taskListArr.getTaskArrayList());
                            taskRecyclerView.setAdapter(adapterTask);
                            closeDialog(activity);
                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        closeDialog(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            closeDialog(activity);
            e1.printStackTrace();
        }
    }


    private void doApiProject(final String page) {
        flexboxLayout.removeAllViews();
        try {
            apiIProms = ApiIProms.factory(this, "ProjectList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("show", "all");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Project project = Project.getInstance(activity);
                        ProjectList projectListArr = ProjectList.getInstance(activity);
                        ArrayList<Project> tempArraylist = new ArrayList<Project>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;
                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject projectList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = projectList.getJSONArray("users");

                                project = new Project();
                                if (projectList.getString("project_id").length() > 0) {
                                    pageCount += 1;
                                }
                                project.setId(projectList.getString("project_id"));
                                project.setCreated(projectList.getString("created"));
                                project.setCreatedBy(projectList.getString("createdby"));
                                project.setDescription(projectList.getString("description"));
                                project.setProjectCode(projectList.getString("project_code"));
                                project.setProjectName(projectList.getString("project_name"));
                                project.setProjectStatus(projectList.getString("status_project"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    user.setFirstName(userArrList.getString("first_name"));
                                    user.setLastName(userArrList.getString("last_name"));
                                    user.setEmail(userArrList.getString("email"));
                                    userList.add(user);
                                }

                                project.setUserList(userList);
                                tempArraylist.add(project);
                                CheckBox checkBox = new CheckBox(activity);
                                checkBox.setId(Integer.parseInt(project.getId()));
                                checkBox.setText(project.getProjectName());

                                filter = Filter.getInstance(activity);
                                if (filter.getAllCheckedId().contains(Integer.parseInt(project.getId()))) {
                                    checkBox.setChecked(true);
                                }
                                checkBox.setTextSize(11);
                                flexboxLayout.addView(checkBox);
                                totalCheckBox += 1;
                                closeDialog(activity);
                                filterDialog.show();
                            }

                            project = Project.getInstance(activity);


                            projectListArr.setProjectArrayList(tempArraylist);

                            closeDialog(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    private void doApiTaskFilter(final String page) {
        imageViewError.setVisibility(View.GONE);
        imageViewNoTask.setVisibility(View.GONE);
        try {
            if (!swipeTask.isRefreshing()) {
                openDialog(activity);
            }
            apiIProms = ApiIProms.factory(this, "TaskFilter");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("user_id", "");
            postData.put("sort_by", sortMode);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Task task = Task.getInstance(activity);
                        TaskList taskListArr = TaskList.getInstance(activity);
                        ArrayList<Task> tempArraylist = new ArrayList<Task>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject taskList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = taskList.getJSONArray("users");
                                task = new Task();

                                if (taskList.getString("to_do_list_id").length() > 0) {
                                    pageCount += 1;
                                }

                                task.setId(taskList.getString("to_do_list_id"));
                                task.setCreated(taskList.getString("creator_name"));
                                task.setCreatedDate(taskList.getString("created"));
                                task.setDescription(taskList.getString("description"));
                                task.setTaskCode(taskList.getString("to_do_list_code"));
                                task.setTaskName(taskList.getString("to_do_list_name"));
                                task.setTaskStatus(taskList.getString("to_do_list_status"));
                                task.setProjectName(taskList.getString("project_name"));
                                task.setProjectDate(taskList.getString("project_created_date"));
                                task.setProjectCreator(taskList.getString("project_created_by"));
                                task.setStartDate(taskList.getString("plan_start_date"));
                                task.setEndDate(taskList.getString("plan_end_date"));
                                task.setActualStartDate(taskList.getString("actual_start_date"));
                                task.setActualEndDate(taskList.getString("actual_end_date"));
                                task.setUserOrder(taskList.getString("user_order_name"));
                                task.setUserApprove(taskList.getString("user_approve"));
                                task.setTaskReason(taskList.getString("reason"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    user.setFullname(userArrList.getString("user_employee_name"));
                                    userList.add(user);
                                }

                                task.setUserList(userList);
                                tempArraylist.add(task);
                            }

                            task = Task.getInstance(activity);

                            taskListArr.setTaskArrayList(tempArraylist);

                            adapterTask = new AdapterTask(activity, taskListArr.getTaskArrayList());
                            taskRecyclerView.setAdapter(adapterTask);
                            closeDialog(activity);
                            filterDialog.dismiss();

                            if (swipeTask.isRefreshing()) {
                                swipeTask.setRefreshing(false);
                            }
                            if (taskListArr.getTaskArrayList().size() < 1) {
                                imageViewNoTask.setVisibility(View.VISIBLE);
                            }
                        } else {
                            closeDialog(activity);
                            if (swipeTask.isRefreshing()) {
                                swipeTask.setRefreshing(false);
                            }
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            imageViewError.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        closeDialog(activity);
                        if (swipeTask.isRefreshing()) {
                            swipeTask.setRefreshing(false);
                        }
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        imageViewError.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialog(activity);
                    imageViewError.setVisibility(View.VISIBLE);
                }
            });
        } catch (MApiException e1) {
            closeDialog(activity);
            imageViewError.setVisibility(View.VISIBLE);
            e1.printStackTrace();
        }
    }


    protected void clearFilter() {

        editTextStartDate.clearFocus();
        editTextEndDate.clearFocus();
        editTextStartDeadline.clearFocus();
        editTextEndDeadline.clearFocus();

        editTextStartDate.setText("");
        editTextEndDate.setText("");
        editTextStartDeadline.setText("");
        editTextEndDeadline.setText("");
        radioGroupFilter.clearCheck();
        radioThisDate.setChecked(false);
        filterDialog.dismiss();
        filter.clear();
        doApiProject("0");
        doApiTask("0");
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (resultCode == NEW_TASK_ADDED) {
            Snackbar.make(layoutMain, "Tugas Berhasil Ditambahkan", Snackbar.LENGTH_SHORT).show();
        }

    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;

        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Mulai";
            dateValue = editTextStartDate.getText().toString();
        } else if (dateMode.equalsIgnoreCase("end")) {
            title = "Tanggal Berakhir";
            dateValue = editTextEndDate.getText().toString();
        } else if (dateMode.equalsIgnoreCase("start_deadline")) {
            title = "Tanggal Awal Deadline";
            dateValue = editTextStartDeadline.getText().toString();
        } else if (dateMode.equalsIgnoreCase("end_deadline")) {
            title = "Tanggal Akhir Deadline";
            dateValue = editTextEndDeadline.getText().toString();
        } else {
            title = "";
            dateValue = "";
        }
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                MyTaskActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );
        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    MyTaskActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
        } else {
        }
        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
        } else if (dateMode.equalsIgnoreCase("end")) {
            editTextEndDate.setText(fullDate);
        } else if (dateMode.equalsIgnoreCase("start_deadline")) {
            editTextStartDeadline.setText(fullDate);
        } else if (dateMode.equalsIgnoreCase("end_deadline")) {
            editTextEndDeadline.setText(fullDate);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
        Filter filter = Filter.getInstance(activity);
        filter.clear();
    }
}
