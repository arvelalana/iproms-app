package com.itant.iproms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.DailyActivityList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.MShareDataString;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MDialog;
import com.itant.iproms.mapp.mComponent.MTextInputEditText;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import static com.itant.iproms.DetailDailyActivity.EDIT_DAILY_ACTIVITY;
import static com.itant.iproms.MyDailyActivity.NEW_DAILY_REQUEST;

public class NewDailyActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private Spinner spinnerUserApprove;
    private RelativeLayout layoutMain;
    private Boolean isOpen = false;
    private Boolean isSent = false;
    private Boolean isCopy = false;
    private String dateMode, hourMode, taskName, description, startDate, startHour, endHour, userApprove, deadlineDate;
    private MTextInputEditText editTextTaskName, editTextDescription, editTextStartDate, editTextStartHour, editTextEndHour, editTextDeadlineDate;
    private Button btnSave;
    private String mode, dailyId;
    private DailyActivityList dailyActivityList;
    private Date fullStartDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_daily);
        activity = this;
        layoutMain = findViewById(R.id.layout_new_daily_activity);
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("BUAT KINERJA HARIAN");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mode = "add";
        editTextTaskName = findViewById(R.id.edit_text_daily_name);
        editTextDescription = findViewById(R.id.edit_text_daily_short_description);
        editTextStartDate = findViewById(R.id.edit_text_daily_date_start);
        editTextStartHour = findViewById(R.id.edit_text_daily_hour_start);
        editTextEndHour = findViewById(R.id.edit_text_daily_hour_end);
        spinnerUserApprove = findViewById(R.id.spinner_user_approve_daily);
        editTextDeadlineDate = findViewById(R.id.edit_text_daily_deadline);
        btnSave = findViewById(R.id.button_save_daily);
        doApiUserListSpinner();


        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = cal.getTime();
        editTextStartDate.setText(simpleDateFormat.format(date));
        editTextDeadlineDate.setText(simpleDateFormat.format(date));
        editTextStartHour.setText("08:30");
        editTextEndHour.setText("17:30");
        editTextTaskName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                editTextDescription.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isCopy = true;
            dailyId = bundle.getString("dailyId");
            dailyActivityList = DailyActivityList.getInstance(activity);
            toolbarTitle.setText("BUAT KINERJA HARIAN");
            DailyActivity dailyActivity = dailyActivityList.get(dailyId);
            editTextTaskName.setText(dailyActivity.getDailyActivityName());
            editTextDescription.setText(dailyActivity.getDescription());
            editTextStartDate.setText(dailyActivity.getStartDateOnly());
            editTextStartHour.setText(dailyActivity.getStartHour());
            editTextEndHour.setText(dailyActivity.getEndHour());
            editTextDeadlineDate.setText(dailyActivity.getDeadlineDateOnly());
        }
        editTextStartHour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    hourMode = "start";
                    timePickerOnClick();
                }
                return false;
            }
        });

        editTextEndHour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    hourMode = "end";
                    timePickerOnClick();
                }
                return false;
            }
        });

        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextDeadlineDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {
                    isSent = true;
                    int err = 0;
                    UserList userList = UserList.getInstance(activity);
                    taskName = editTextTaskName.getText().toString();
                    description = editTextDescription.getText().toString();
                    startDate = editTextStartDate.getText().toString();
                    startHour = editTextStartHour.getText().toString();
                    endHour = editTextEndHour.getText().toString();
                    deadlineDate = editTextDeadlineDate.getText().toString();
                    userApprove = userList.getPosition(spinnerUserApprove.getSelectedItemPosition()).getUserId();
                    if (taskName.length() < 1) {
                        err++;
                        editTextTaskName.setError(getResources().getString(R.string.task_name_blank));
                    }

                    if (description.length() < 1) {
                        err++;
                        editTextDescription.setError(getResources().getString(R.string.short_description_blank));
                    }


                    if (startDate.length() < 1) {
                        err++;
                        editTextStartDate.setError(getResources().getString(R.string.start_date_blank));
                    }

                    if (startHour.length() < 1) {
                        err++;
                        editTextStartHour.setError(getResources().getString(R.string.start_hour_blank));
                    }

                    if (startHour.length() < 1) {
                        err++;
                        editTextStartHour.setError(getResources().getString(R.string.start_hour_blank));
                    }

                    if (endHour.length() < 1) {
                        err++;
                        editTextStartHour.setError(getResources().getString(R.string.end_hour_blank));
                    }

                    if (deadlineDate.length() < 1) {
                        err++;
                        editTextDeadlineDate.setError(getResources().getString(R.string.end_date_blank));
                    }

                    if (err == 0) {
                        if (mode.equalsIgnoreCase("edit")) {
                            doApiEditDailyActivity();
                        } else {
                            doApiNewDailyActivity();
                        }
                    } else {
                        isSent = false;
                    }
                }
            }
        });
    }

    private void doApiUserListSpinner() {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            postData.put("project_id", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);

                        UserList userArrayList = UserList.getInstance(activity);
                        userArrayList.clear();
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                String userId = userList.getString("user_id");
                                user = new User(activity);
                                user.setUserId(userList.getString("user_id"));
                                user.setUsername(userList.getString("username"));
                                user.setFirstName(userList.getString("first_name"));
                                user.setLastName(userList.getString("last_name"));
                                user.setEmail(userList.getString("email"));
                                MSharedData sharedData = MSharedData.getInstance(activity);
                                if (!sharedData.get(MShareDataString.USER_ID).equalsIgnoreCase(userId) && sharedData.get(SharedData.USER_PARENT_ID).equalsIgnoreCase(userId)) {
                                    userArrayList.add(user);
                                }
                            }

                            ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, userArrayList.getAllUsername());
                            userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerUserApprove.setAdapter(userArrayAdapter);
                            if (isCopy) {
                                DailyActivity dailyActivity = dailyActivityList.get(dailyId);
                                System.out.println(userArrayList.getIndex(dailyActivity.getUserApprove()));
                                spinnerUserApprove.setSelection(Integer.valueOf(userArrayList.getIndex(dailyActivity.getUserApprove())));
                            }
                            closeDialog(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
            closeDialog(activity);
        }
    }


    private void doApiNewDailyActivity() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "NewDailyActivity");
            HashMap postData = new HashMap();
            postData.put("task_name", taskName);
            postData.put("start_date", startDate);
            postData.put("start_hour", startHour);
            postData.put("end_hour", endHour);
            postData.put("deadline_date", deadlineDate);
            postData.put("user_approve", userApprove);
            postData.put("description", description);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(Activity.RESULT_OK);
                            Intent intent = new Intent(activity, NewDailyActivity.class);
                            startActivityForResult(intent, NEW_DAILY_REQUEST);
                            overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Menambahkan Kinerja Harian", Snackbar.LENGTH_LONG)
                                    .show();

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }

    private void doApiEditDailyActivity() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "EditDailyActivity");
            HashMap postData = new HashMap();
            postData.put("daily_id", dailyId);
            postData.put("task_name", taskName);
            postData.put("start_date", startDate);
            postData.put("start_hour", startHour);
            postData.put("end_hour", endHour);
            postData.put("deadline_date", deadlineDate);
            postData.put("user_approve", userApprove);
            postData.put("description", description);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(EDIT_DAILY_ACTIVITY);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Mengubah Kinerja Harian", Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }


    public void timePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        String title = "Jam Mulai";
        if (hourMode.equalsIgnoreCase("start")) {
            title = "jam Mulai";
        } else {
            title = "jam Berakhir";
        }

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                NewDailyActivity.this,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                true
        );

        tpd.setTitle(title);
        tpd.show(getFragmentManager(), "Timepickerdialog");
        tpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;
        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Mulai";
            dateValue = editTextStartDate.getText().toString();
        } else {
            title = "Tanggal Berakhir";
            dateValue = editTextDeadlineDate.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                NewDailyActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    NewDailyActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }

        if (dateMode.equalsIgnoreCase("start")) {
            Calendar calendarNow = Calendar.getInstance();
            calendarNow.add(Calendar.DATE, -3);
//            dpd.setMinDate(calendarNow);
        } else {
            Calendar calendarNow = Calendar.getInstance();
            if (dateValue.length() > 0) {
                String dateValueStart = editTextStartDate.getText().toString();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                Date date = calendarNow.getTime();
                try {
                    date = (Date) df.parse(dateValueStart);
                    calendarNow.setTime(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            dpd.setMinDate(calendarNow);
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = String.valueOf(monthOfYear);
        String day = String.valueOf(dayOfMonth);
        if (monthOfYear < 10) {
            month = "0" + String.valueOf(monthOfYear);
        }
        if (dayOfMonth < 10) {
            day = "0" + String.valueOf(dayOfMonth);
        }
        fullDate = day + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
            editTextDeadlineDate.setText(fullDate);
        } else {
            editTextDeadlineDate.setText(fullDate);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            final MDialog messageDialog = new MDialog(this, R.style.dialogNoTitle);
            messageDialog.setButtonType("yesno");
            String strLblPopup = activity.getResources().getString(R.string.quit_task_ask);
            messageDialog.setMessage(strLblPopup);
            Button yesbutton = (Button) messageDialog.findViewById(R.id.btn_yes);
            yesbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    overridePendingTransition(R.anim.going_out, R.anim.going_in);
                }
            });
            Button nobutton = (Button) messageDialog.findViewById(R.id.btn_no);
            nobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messageDialog.dismiss();

                }
            });
            messageDialog.show();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        final MDialog messageDialog = new MDialog(this, R.style.dialogNoTitle);
        messageDialog.setButtonType("yesno");
        String strLblPopup = activity.getResources().getString(R.string.quit_task_ask);
        messageDialog.setMessage(strLblPopup);
        Button yesbutton = (Button) messageDialog.findViewById(R.id.btn_yes);
        yesbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.going_out, R.anim.going_in);
            }
        });
        Button nobutton = (Button) messageDialog.findViewById(R.id.btn_no);
        nobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();

            }
        });
        messageDialog.show();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String fullTime;
        String hourString = String.valueOf(hourOfDay);
        String minuteString = String.valueOf(minute);
        if (hourString.length() == 1) {
            hourString = "0" + hourString;
        }
        if (minuteString.length() == 1) {
            minuteString = "0" + minuteString;
        }
        fullTime = hourString + ":" + minuteString;
        if (hourMode.equalsIgnoreCase("start")) {
            editTextStartHour.setText(fullTime);
        } else {
            editTextEndHour.setText(fullTime);
        }
    }
}
