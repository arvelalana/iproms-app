package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;
import com.pchmn.materialchips.ChipsInput;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by Arvel on 07/09/2017.
 */

public class AddUserActivity extends MAppCompat {

    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ChipsInput chipsInput;
    private ImageView imageDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        activity = this;
//        closeDialog(activity);
        //Set Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.title_add_user_activity).toUpperCase());
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        chipsInput = findViewById(R.id.chips_input);
        imageDone = findViewById(R.id.image_done);

        final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
        chipsInput.setFilterableList(userChipActivity.getUserChipArrayList());

        Iterator it = userChipActivity.getSelectedChipList().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            final String userId = pair.getKey().toString();
            final UserChip userChip = (UserChip) pair.getValue();
            chipsInput.addChip(userChip);
        }

        imageDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < chipsInput.getSelectedChipList().size(); i++) {
                    System.out.println("AUW" + chipsInput.getSelectedChipList().get(i).getId());
                    UserChip userChip1 = (UserChip) chipsInput.getSelectedChipList().get(i);
                    userChipActivity.addSelectedChipList(userChip1.getId().toString(), userChip1);
                }

//                userChipActivity.setSelectedChipList(chipsInput.getSelectedChipList());
                setResult(Activity.RESULT_OK);
                closeKeyboard(getCurrentFocus());
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        closeKeyboard(getCurrentFocus());
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);

    }
}
