package com.itant.iproms;

import android.app.Activity;
import android.app.MediaRouteActionProvider;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterDaily;
import com.itant.iproms.lib.adapter.AdapterUserStatus;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.PieChartDialog;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.DailyActivityList;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.lib.object.UserStatus;
import com.itant.iproms.lib.object.UserStatusList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ReportUserCountActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener {

    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private RecyclerView userStatusRecyclerView;
    private DrawerLayout layoutMain;
    private GridLayoutManager mLayoutManager;
    private Spinner spinnerEmployee;
    private Button showTask, downloadPdf;
    private Boolean isOpen = false;
    private String dateMode;
    private String userId;
    private EditText editTextStartDate, editTextEndDate;
    private String filterStartDate, filterEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_user_count);

        activity = this;
        layoutMain = findViewById(R.id.drawer_main);

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.user_count_report).toUpperCase());
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editTextStartDate = findViewById(R.id.edit_text_report_count_start_date);
        editTextEndDate = findViewById(R.id.edit_text_report_count_end_date);
        showTask = findViewById(R.id.btn_show_user_count_report);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String stringDate = simpleDateFormat.format(new Date());
        editTextStartDate.setText(stringDate);
        editTextEndDate.setText(stringDate);

        userStatusRecyclerView = findViewById(R.id.report_user_status_recycler_view);
        mLayoutManager = new GridLayoutManager(activity, 1);
        userStatusRecyclerView.setLayoutManager(mLayoutManager);
        userStatusRecyclerView.setItemAnimator(new DefaultItemAnimator());

        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });

        showTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;
                editTextStartDate.setError(null);
                editTextEndDate.setError(null);
                filterStartDate = editTextStartDate.getText().toString();
                filterEndDate = editTextEndDate.getText().toString();

                if (filterStartDate.length() <= 0) {
                    err++;
                    editTextStartDate.setError("Isi tanggal awal kinerja harian");
                }

                if (filterEndDate.length() <= 0) {
                    err++;
                    editTextEndDate.setError("Isi tanggal akhir kinerja harian");
                }

                if (err == 0) {
                    doApiReportUserActivity("0");
                }
            }
        });
    }

    public void doApiReportUserActivity(final String page) {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "ReportUserActivity");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("status", "ACCEPT");
            postData.put("start_date", filterStartDate);
            postData.put("end_date", filterEndDate);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        UserStatus userStatus = UserStatus.getInstance(activity);
                        UserStatusList userStatusList = UserStatusList.getInstance(activity);
                        ArrayList<UserStatus> tempArraylist = new ArrayList<UserStatus>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userActivityObj = (JSONObject) resultsArr.get(i);
                                userStatus = new UserStatus();
                                userStatus.setUserId(userActivityObj.getString("user_id"));
                                userStatus.setUsername(userActivityObj.getString("user_name"));
                                userStatus.setTaskCount(userActivityObj.getString("task_count"));
                                userStatus.setProjectCount(userActivityObj.getString("project_count"));
                                userStatus.setDailyCount(userActivityObj.getString("daily_activity_count"));
                                userStatus.setStartDate(filterStartDate);
                                userStatus.setEndDate(filterEndDate);
                                tempArraylist.add(userStatus);
                            }
                            userStatus = UserStatus.getInstance(activity);
                            userStatusList.setUserStatusArrayList(tempArraylist);

                            AdapterUserStatus adapterUserStatus = new AdapterUserStatus(activity, tempArraylist);
                            userStatusRecyclerView.setAdapter(adapterUserStatus);

                            closeDialog(activity);
                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        closeDialog(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            closeDialog(activity);
            e1.printStackTrace();
        }
    }


    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;
        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Mulai";
            dateValue = editTextStartDate.getText().toString();
        } else {
            title = "Tanggal Berakhir";
            dateValue = editTextEndDate.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                ReportUserCountActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    ReportUserCountActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
        } else {
            editTextEndDate.setText(fullDate);
        }
    }

}
