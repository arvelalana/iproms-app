package com.itant.iproms.lib.object;

import android.content.Context;

/**
 * Created by asus on 5/9/2017.
 */

public class UserStatus {

    public static UserStatus instance = null;
    protected String userId;
    protected String username;
    protected String taskCount;
    protected String projectCount;
    protected String dailyCount;
    protected Context context;
    protected String startDate, endDate;

    public UserStatus() {

    }

    public UserStatus(Context context) {
        this.context = context;
    }

    public static UserStatus getInstance(Context context) {
        if (instance == null) {
            instance = new UserStatus(context);
        }
        return instance;
    }

    public String getUsername() {
        if (username.equalsIgnoreCase("null")) {
            return "";
        }
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(String taskCount) {
        this.taskCount = taskCount;
    }

    public String getProjectCount() {
        return projectCount;
    }

    public void setProjectCount(String projectCount) {
        this.projectCount = projectCount;
    }

    public String getDailyCount() {
        return dailyCount;
    }

    public void setDailyCount(String dailyCount) {
        this.dailyCount = dailyCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
