package com.itant.iproms.lib.object;

import android.content.Context;

/**
 * Created by asus on 5/9/2017.
 */

public class User {

    public static User instance = null;
    protected String username;
    protected String firstName;
    protected String lastName;
    protected String fullname;
    protected String email;
    protected String userId;
    protected String userCode;
    protected String birthday;
    protected String weekTask;
    protected String myTask;
    protected String myProject;
    protected String roleId;
    protected String meetingEmployeeId;
    protected String meetingEmployeeStatus;
    protected String userParentId;
    protected Context context;

    public User() {

    }

    public User(Context context) {
        this.context = context;
    }

    public static User getInstance(Context context) {
        if (instance == null) {
            instance = new User(context);
        }
        return instance;
    }

    public String getUsername() {
        if (username.equalsIgnoreCase("null")) {
            return "";
        }
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getFirstName() {
        if (firstName.equalsIgnoreCase("null")) {
            return "";
        }
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        if (lastName.equalsIgnoreCase("null")) {
            return "";
        }
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        if (email.equalsIgnoreCase("null")) {
            return "";
        }
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        if (userId.equalsIgnoreCase("null")) {
            return "";
        }
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBirthday() {
        if (birthday.equalsIgnoreCase("null")) {
            return "";
        }
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getUserCode() {
        if (userCode.equalsIgnoreCase("null")) {
            return "";
        }
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getFullname() {
        return fullname;
    }

    public String getWeekTask() {
        if (weekTask == null) {
            return "0";
        }
        return weekTask;
    }

    public void setWeekTask(String weekTask) {
        this.weekTask = weekTask;
    }

    public String getMyTask() {
        if (myTask == null) {
            return "0";
        }
        return myTask;
    }

    public void setMyTask(String myTask) {
        this.myTask = myTask;
    }

    public String getMyProject() {
        if (myProject == null) {
            return "0";
        }
        return myProject;
    }

    public void setMyProject(String myProject) {
        this.myProject = myProject;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMeetingEmployeeId() {
        return meetingEmployeeId;
    }

    public void setMeetingEmployeeId(String meetingEmployeeId) {
        this.meetingEmployeeId = meetingEmployeeId;
    }

    public String getMeetingEmployeeStatus() {
        return meetingEmployeeStatus;
    }

    public String getMeetingEmployeeStatusString() {
        String meetingStatusString = "";
        if (meetingEmployeeStatus.equalsIgnoreCase("ACTIVE")) {
            meetingStatusString = "Menunggu";
        } else if (meetingEmployeeStatus.equalsIgnoreCase("ACCEPT")) {
            meetingStatusString = "Hadir";
        } else if (meetingEmployeeStatus.equalsIgnoreCase("REJECT")) {
            meetingStatusString = "Tidak Hadir";
        }
        return meetingStatusString;
    }

    public void setMeetingEmployeeStatus(String meetingEmployeeStatus) {
        this.meetingEmployeeStatus = meetingEmployeeStatus;
    }

    public String getUserParentId() {
        return userParentId;
    }

    public void setUserParentId(String userParentId) {
        this.userParentId = userParentId;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fullname='" + fullname + '\'' +
                ", email='" + email + '\'' +
                ", userId='" + userId + '\'' +
                ", userCode='" + userCode + '\'' +
                ", birthday='" + birthday + '\'' +
                ", weekTask='" + weekTask + '\'' +
                ", myTask='" + myTask + '\'' +
                ", myProject='" + myProject + '\'' +
                ", roleId='" + roleId + '\'' +
                ", meetingEmployeeId='" + meetingEmployeeId + '\'' +
                ", meetingEmployeeStatus='" + meetingEmployeeStatus + '\'' +
                ", userParentId='" + userParentId + '\'' +
                ", context=" + context +
                '}';
    }
}
