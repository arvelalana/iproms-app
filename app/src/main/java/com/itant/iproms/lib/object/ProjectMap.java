package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.HashMap;

/**
 * Created by x441s on 06/09/2017.
 */

public class ProjectMap {

    public static ProjectMap instance = null;
    protected Context context;

    protected HashMap<String, Project> projectMap;

    public ProjectMap(Context context) {
        projectMap = new HashMap<String, Project>();
        this.context = context;
    }


    public static ProjectMap getInstance(Context context) {
        if (instance == null) {
            instance = new ProjectMap(context);
        }
        return instance;
    }

    public void add(Project project) {
        this.projectMap.put(project.getEndDate(), project);
    }

    public void clear() {
        this.projectMap.clear();
    }

    public HashMap<String, Project> getProjectMap() {
        return projectMap;
    }

    public void setProjectMap(HashMap<String, Project> projectMap) {
        this.projectMap = projectMap;
    }
}
