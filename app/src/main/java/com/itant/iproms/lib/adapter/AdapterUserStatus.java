package com.itant.iproms.lib.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.itant.iproms.R;
import com.itant.iproms.lib.dialog.PieChartDialog;
import com.itant.iproms.lib.object.UserStatus;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Raymond on 2/28/2017.
 */

public class AdapterUserStatus extends RecyclerView.Adapter<AdapterUserStatus.UserStatusViewHolder> {

    private static final String TAG = AdapterUserStatus.class.getSimpleName();

    private Context mContext;
    private ArrayList<UserStatus> userStatusArrayList;

    public AdapterUserStatus(Context context, ArrayList userStatusArrayList) {
        this.mContext = context;
        this.userStatusArrayList = userStatusArrayList;
    }

    public void clearAll() {
//        int size = this.restaurantList.size();
//        if (size > 0) {
//            for (int i = 0; i < size; i++) {
//                restaurantList.remove(0);
//            }
//            this.notifyItemRangeRemoved(0, size);
//        }
    }

    @Override
    public UserStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_user_status, parent, false);

        return new UserStatusViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserStatusViewHolder holder, int position) {
        Random rnd = new Random();

        final UserStatus userStatus = userStatusArrayList.get(position);
        holder.tvAdapterUserStatusName.setText(userStatus.getUsername());
        holder.tvAdapterUserTaskCount.setText(userStatus.getTaskCount());
        holder.tvAdapterUserTaskCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PieChartDialog pieChartDialog = new PieChartDialog(mContext, userStatus, "task");
                pieChartDialog.show();
            }
        });
        holder.tvAdapterUserProjectCount.setText(userStatus.getProjectCount());
        holder.tvAdapterUserProjectCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PieChartDialog pieChartDialog = new PieChartDialog(mContext, userStatus, "project");
                pieChartDialog.show();
            }
        });
        holder.tvAdapterDailyTaskCount.setText(userStatus.getDailyCount());
        holder.tvAdapterDailyTaskCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PieChartDialog pieChartDialog = new PieChartDialog(mContext, userStatus, "daily");
                pieChartDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return userStatusArrayList.size();
    }

    public class UserStatusViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layoutAdapterUserStatus;
        public TextView tvAdapterUserStatusName, tvAdapterUserTaskCount, tvAdapterUserProjectCount, tvAdapterDailyTaskCount;

        public UserStatusViewHolder(View view) {
            super(view);
            layoutAdapterUserStatus = view.findViewById(R.id.layout_adapter_user_status);
            tvAdapterUserStatusName = view.findViewById(R.id.tv_adapter_user_status_name);
            tvAdapterUserTaskCount = view.findViewById(R.id.tv_adapter_task_count);
            tvAdapterUserProjectCount = view.findViewById(R.id.tv_adapter_project_count);
            tvAdapterDailyTaskCount = view.findViewById(R.id.tv_adapter_daily_count);
        }
    }

}
