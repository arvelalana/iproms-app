package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class ProjectList {

    public static ProjectList instance = null;
    protected Context context;

    protected ArrayList<Project> projectArrayList;

    public ProjectList(Context context) {
        projectArrayList = new ArrayList<Project>();
        this.context = context;
    }


    public static ProjectList getInstance(Context context) {
        if (instance == null) {
            instance = new ProjectList(context);
        }
        return instance;
    }

    public void add(Project project) {
        this.projectArrayList.add(project);
    }

    public void clear() {
        this.projectArrayList.clear();
    }

    public Project get(String id) {
        Project project = null;
        for (int i = 0; i < this.projectArrayList.size(); i++) {
            if (projectArrayList.get(i).getId().equalsIgnoreCase(id)) {
                project = projectArrayList.get(i);
            }
        }
        return project;
    }

    public Project getPosition(int pos) {
        Project project = this.getProjectArrayList().get(pos);
        return project;
    }

    public ArrayList<String> getAllProjectName() {
        ArrayList<String> projectNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.projectArrayList.size(); i++) {
            projectNameArrayList.add(this.projectArrayList.get(i).getProjectName().toString());
        }
        return projectNameArrayList;
    }

    public ArrayList<Project> getProjectArrayList() {
        return projectArrayList;
    }

    public void setProjectArrayList(ArrayList<Project> projectArrayList) {
        this.projectArrayList = projectArrayList;
    }

}
