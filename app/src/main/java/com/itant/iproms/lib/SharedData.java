package com.itant.iproms.lib;

import android.content.Context;

import com.itant.iproms.lib.object.User;
import com.itant.iproms.mapp.MSharedData;


/**
 * Created by Raymond on 4/27/2017.
 */

public class SharedData extends MSharedData {

    public static final String IS_LOGIN = "is_login";
    public static final String USERNAME= "username";
    public static final String MEMBER_ID= "member_id";
    public static final String AUTH_ID = "auth_id";
    public static final String ORG_ID = "org_id";
    public static final String SESSION_ID = "session_id";
    public static final String USER_ID = "user_id";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_COMPLETE_NAME = "user_complete_name";
    public static final String USER_PARENT_ID = "user_parent_id";

    public SharedData(Context context) {
        super(context);

        cookies = context.getSharedPreferences(SharedData.SHARED_PREFERENCES, 0);
    }


    public static MSharedData getInstance(Context context) {
        if (instance == null) {
            instance = new SharedData(context);
        }
        return instance;
    }
}
