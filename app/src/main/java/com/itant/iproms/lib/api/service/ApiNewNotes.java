package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiNewNotes extends MApiService {

    public static String TAG = ApiNewNotes.class.getSimpleName();

    public ApiNewNotes(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/newNotes";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String sessionId = shareData.get(SharedData.SESSION_ID);
        String orgId = shareData.get(SharedData.ORG_ID);
        HashMap data = api.getPostData();

        request = "";
        request += "session_id=" + sessionId;
        request += "&org_id=" + orgId;
        request += "&meeting_id=" + data.get("meeting_id");
        request += "&notes_title=" + data.get("notes_title");
        request += "&user_creator=" + data.get("user_creator");
        request += "&meeting_leader=" + data.get("meeting_leader");
        request += "&person_in_charge=" + data.get("person_in_charge");
        request += "&topic=" + data.get("notes_topic");

        ArrayList<String> attendance_users = (ArrayList<String>) data.get("attendance_users");
        for (int i = 0; i < attendance_users.size(); i++) {
            request += "&attendance_users[" + i + "]=" + attendance_users.get(i);
        }

        ArrayList<String> not_attendance_users = (ArrayList<String>) data.get("non_attendance_users");
        for (int i = 0; i < not_attendance_users.size(); i++) {
            request += "&non_attendance_users[" + i + "]=" + not_attendance_users.get(i);
        }

        ArrayList<String> opinionCreators = (ArrayList<String>) data.get("opinion_creator");
        for (int i = 0; i < opinionCreators.size(); i++) {
            request += "&opinion_creator[" + i + "]=" + opinionCreators.get(i);
        }

        ArrayList<String> opinion = (ArrayList<String>) data.get("opinion");
        for (int i = 0; i < opinion.size(); i++) {
            request += "&opinion_list[" + i + "]=" + opinion.get(i);
        }
        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
