package com.itant.iproms.lib.object;

import android.content.Context;

import java.net.Inet4Address;
import java.util.ArrayList;

/**
 * Created by Arvel Alana on 15/09/2017.
 */

public class Filter {

    public static Filter instance = null;
    protected Context context;

    protected ArrayList<Project> checkedProjectList;
    protected String createdStartDate;
    protected String createdEndDate;
    protected String deadlineDateMode;
    protected String deadlineStartDate;
    protected String deadlineEndDate;
    protected String planStartDate;
    protected String planEndDate;
    protected String taskStatus;

    public Filter(Context context) {
        this.context = context;
        checkedProjectList = new ArrayList<Project>();
    }

    public static Filter getInstance(Context context) {
        if (instance == null) {
            instance = new Filter(context);
        }
        return instance;
    }

    public Project getPosition(int pos) {
        Project project = this.getCheckedProjectList().get(pos);
        return project;
    }

    public void addCheckedProjectList(Project project) {
        this.checkedProjectList.add(project);
    }

    public void clearCheckedProjectList() {
        this.checkedProjectList.clear();
    }
    public ArrayList<Project> getCheckedProjectList() {
        return checkedProjectList;
    }

    public void setCheckedProjectList(ArrayList<Project> checkedProjectList) {
        this.checkedProjectList = checkedProjectList;
    }

    public String getCreatedStartDate() {
        if (createdStartDate == null) {
            createdStartDate = "";
        }
        return createdStartDate;
    }

    public void setCreatedStartDate(String createdStartDate) {
        this.createdStartDate = createdStartDate;
    }

    public String getCreatedEndDate() {
        if (createdEndDate == null) {
            createdEndDate = "";
        }
        return createdEndDate;
    }

    public void clear() {
        instance = new Filter(context);
        createdStartDate = "";
        createdEndDate = "";
//        planStartDate = "";
//        planEndDate = "";
        deadlineStartDate = "";
        deadlineEndDate = "";
        this.checkedProjectList.clear();
    }

    public void setCreatedEndDate(String createdEndDate) {
        this.createdEndDate = createdEndDate;
    }

    public String getDeadlineStartDate() {
        if (deadlineStartDate == null) {
            deadlineStartDate = "";
        }
        return deadlineStartDate;
    }

    public void setDeadlineStartDate(String deadlineStartDate) {
        this.deadlineStartDate = deadlineStartDate;
    }

    public String getDeadlineEndDate() {
        if (deadlineEndDate == null) {
            deadlineEndDate = "";
        }
        return deadlineEndDate;
    }

    public void setDeadlineEndDate(String deadlineEndDate) {
        this.deadlineEndDate = deadlineEndDate;
    }

    public ArrayList<Integer> getAllCheckedId() {
        ArrayList<Integer> checkedIdList = new ArrayList<Integer>();
        for (int i = 0; i < this.checkedProjectList.size(); i++) {
            checkedIdList.add(Integer.parseInt(this.checkedProjectList.get(i).getId()));
        }
        return checkedIdList;
    }

    public String getDeadlineDateMode() {
        return deadlineDateMode;
    }

    public void setDeadlineDateMode(String deadlineDateMode) {
        this.deadlineDateMode = deadlineDateMode;
    }

    public String getPlanStartDate() {
        if (planStartDate == null) {
            planStartDate = "";
        }
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanEndDate() {
        if (planEndDate == null) {
            planEndDate = "";
        }
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getTaskStatus() {
        if (taskStatus == null) {
            taskStatus = "";
        }
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }
}
