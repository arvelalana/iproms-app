package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiReportTask extends MApiService {

    public static String TAG = ApiReportTask.class.getSimpleName();

    public ApiReportTask(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/reportTask";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String userId = shareData.get(SharedData.USER_ID);
        String sessionId = shareData.get(SharedData.SESSION_ID);
        HashMap data = api.getPostData();

        String userSendId = "";
        userSendId = data.get("user_id").toString();

        Filter filter = Filter.getInstance(api.getContext());
        request = "";
        request += "session_id=" + sessionId;
        if (userSendId.length() > 0) {
            request += "&user_id=" + userSendId;
        } else {
            request += "&user_id=" + userId;
        }
        request += "&created_start=" + filter.getCreatedStartDate();
        request += "&created_end=" + filter.getCreatedEndDate();
        request += "&deadline_start=" + filter.getDeadlineStartDate();
        request += "&deadline_end=" + filter.getDeadlineEndDate();
        request += "&plan_start=" + filter.getPlanStartDate();
        request += "&plan_end=" + filter.getPlanEndDate();
        request += "&sort_by=" + data.get("sort_by");
        request += "&task_status=" + filter.getTaskStatus();

        ArrayList<Integer> projects = filter.getAllCheckedId();
        for (int i = 0; i < projects.size(); i++) {
            request += "&projects[" + i + "]=" + String.valueOf(projects.get(i));
        }
        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
