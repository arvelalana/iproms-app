package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.itant.iproms.R;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.mapp.helper.MAndroidUtil;
import com.itant.iproms.mapp.mComponent.MTextView;

/**
 * Created by x441s on 05/09/2017.
 */

public class SortDialog extends Dialog {

    protected Context context;

    public SortDialog(Context _context) {
        super(_context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.context = _context;
        init();

    }

    public void init() {
        setContentView(R.layout.dialog_sort);
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }
}

