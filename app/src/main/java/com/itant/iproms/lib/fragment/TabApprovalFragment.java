package com.itant.iproms.lib.fragment;

/**
 * Created by PC on 23/10/2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.itant.iproms.MyDailyActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.adapter.AdapterDaily;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.LoadingSmallDialog;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.DailyActivityList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TabApprovalFragment extends Fragment {

    private Context mContext;
    private SwipeRefreshLayout swipeDaily;
    private RecyclerView dailyRecyclerView;
    private GridLayoutManager mLayoutManager;
    private ApiIProms apiIProms;
    private RelativeLayout layoutMain;
    private LoadingSmallDialog loadingSmallDialog;
    private ImageView imageNoDaily;
    private Fragment fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_daily_approval, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragment = this;
        layoutMain = view.findViewById(R.id.layout_main);
        imageNoDaily = view.findViewById(R.id.image_view_no_daily_approval);
        dailyRecyclerView = view.findViewById(R.id.daily_activity_recycler_view);
        mLayoutManager = new GridLayoutManager(mContext, 1);
        dailyRecyclerView.setLayoutManager(mLayoutManager);
        dailyRecyclerView.setItemAnimator(new DefaultItemAnimator());
        swipeDaily = view.findViewById(R.id.swipe_task);
        swipeDaily.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ((MyDailyActivity) mContext).doApiDailyACtivity("0", true);
//                doApiDailyACtivity("0");
            }
        });

        DailyActivityList dailyActivityList = DailyActivityList.getInstance(mContext);
        ArrayList<DailyActivity> tempArraylist = new ArrayList<DailyActivity>();
        for (int i = 0; i < dailyActivityList.getDailyActivityArrayList().size(); i++) {
            if (dailyActivityList.getPosition(i).getApprovable().equalsIgnoreCase("1")) {
                tempArraylist.add(dailyActivityList.getPosition(i));
            }
        }

        if (tempArraylist.size() < 1) {
            imageNoDaily.setVisibility(View.VISIBLE);
        } else {
            imageNoDaily.setVisibility(View.GONE);
        }

        AdapterDaily adapterDaily = new AdapterDaily(mContext, tempArraylist, false);
        dailyRecyclerView.setAdapter(adapterDaily);

//        doApiDailyACtivity("0");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    private void doApiDailyACtivity(final String page) {
//        openDialog(mContext);
        imageNoDaily.setVisibility(View.GONE);
        try {
            if (!swipeDaily.isRefreshing()) {

            }
            apiIProms = ApiIProms.factory(mContext, "DailyList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("status", "");
            postData.put("start_date", "");
            postData.put("end_date", "");
            postData.put("user", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        DailyActivity dailyActivity = DailyActivity.getInstance(mContext);
                        DailyActivityList dailyActivityList = DailyActivityList.getInstance(mContext);
                        ArrayList<DailyActivity> tempArraylist = new ArrayList<DailyActivity>();
                        ArrayList<DailyActivity> usedArrayList = new ArrayList<DailyActivity>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            System.out.println(resultsArr);
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject dailyActivityObj = (JSONObject) resultsArr.get(i);
                                dailyActivity = new DailyActivity();

                                if (dailyActivityObj.getString("daily_activity_id").length() > 0) {
                                    pageCount += 1;
                                }

                                dailyActivity.setId(dailyActivityObj.getString("daily_activity_id"));
                                dailyActivity.setUsersId(dailyActivityObj.getString("users_id"));
                                dailyActivity.setCreated(dailyActivityObj.getString("createdby"));
                                dailyActivity.setCreatedDate(dailyActivityObj.getString("created"));
                                dailyActivity.setDescription(dailyActivityObj.getString("description"));
                                dailyActivity.setDailyActivityCode(dailyActivityObj.getString("daily_activity_code"));
                                dailyActivity.setDailyActivityName(dailyActivityObj.getString("daily_activity_name"));
                                dailyActivity.setDailyActivityStatus(dailyActivityObj.getString("daily_activity_status"));
                                dailyActivity.setStartDate(dailyActivityObj.getString("start_date"));
                                dailyActivity.setEndDate(dailyActivityObj.getString("end_date"));
                                dailyActivity.setDeadlineDate(dailyActivityObj.getString("deadline_date"));
                                dailyActivity.setUserApprove(dailyActivityObj.getString("users_id_approve"));
                                dailyActivity.setUserApproveName(dailyActivityObj.getString("user_approve_name"));
                                dailyActivity.setApprovable(dailyActivityObj.getString("approvable"));
                                dailyActivity.setDailyActivityReason(dailyActivityObj.getString("reason"));
                                dailyActivity.setUsersCreator(dailyActivityObj.getString("user_creator"));
                                dailyActivity.setUpdateDate(dailyActivityObj.getString("updated"));
                                dailyActivity.setUpdateBy(dailyActivityObj.getString("updatedby"));
                                if (dailyActivityObj.getString("approvable").equalsIgnoreCase("1")) {
                                    usedArrayList.add(dailyActivity);
                                    tempArraylist.add(dailyActivity);
                                }
                            }

                            dailyActivity = DailyActivity.getInstance(mContext);
                            dailyActivityList.setDailyActivityArrayList(tempArraylist);
                            AdapterDaily adapterDaily = new AdapterDaily(mContext, usedArrayList, true);
                            dailyRecyclerView.setAdapter(adapterDaily);

                            if (swipeDaily.isRefreshing()) {
                                swipeDaily.setRefreshing(false);
                            }
                            System.out.println("MAYSIZE:" + tempArraylist.size());
                            if (tempArraylist.size() < 1) {
                                imageNoDaily.setVisibility(View.VISIBLE);
                            }
                            MyDailyActivity.selectionOff(mContext);
                        } else {
                            if (swipeDaily.isRefreshing()) {
                                swipeDaily.setRefreshing(false);
                            }
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        closeDialog(mContext);
                    } catch (JSONException e) {
                        if (swipeDaily.isRefreshing()) {
                            swipeDaily.setRefreshing(false);
                        }
                        closeDialog(mContext);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialog(mContext);
                }
            });
        } catch (MApiException e1) {
            closeDialog(mContext);
            e1.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        doApiDailyACtivity("0");
    }

    protected void openDialog(Context dContext) {
        loadingSmallDialog = new LoadingSmallDialog(dContext);
        loadingSmallDialog.show();
    }

    protected void closeDialog(Context dContext) {
        if (loadingSmallDialog != null && loadingSmallDialog.isShowing()) {
            loadingSmallDialog.dismiss();
        }
    }
}
