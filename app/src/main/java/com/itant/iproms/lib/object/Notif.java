package com.itant.iproms.lib.object;

import android.content.Context;

/**
 * Created by asus on 5/9/2017.
 */

public class Notif {

    public static Notif instance = null;
    protected Context context;
    protected String id;
    protected String module;
    protected String title;
    protected String body;
    protected String refId;
    protected String createdDate;
    protected Boolean isRead;

    public Notif() {

    }

    public Notif(Context context) {
        this.context = context;
    }

    public static Notif getInstance(Context context) {
        if (instance == null) {
            instance = new Notif(context);
        }
        return instance;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(String read) {
        if (read.equalsIgnoreCase("1")) {
            isRead = true;
        } else {
            isRead = false;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
