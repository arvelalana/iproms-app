package com.itant.iproms.lib.api;

import android.content.Context;

import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;


/**
 * Created by Raymond on 4/27/2017.
 */

public class ApiIProms extends MApi {

    public ApiIProms(Context context, String serviceName) throws MApiException {
        super(context, serviceName);

//        this.domain = "http://iproms.local/api/";
//        this.domain = "http://dev.iproms.kip-capital.com/api/";
//        this.domain = "http://192.168.1.10/codeigniter-wapp.git/api/";
//        this.domain = "http://192.168.1.10/api/";
        this.domain = "http://iproms.kip-capital.com/api/";
        this.packageName = "com.itant.iproms.lib.api.service.Api";
        initiate();
    }

    public static ApiIProms factory(Context context, String serviceName) throws MApiException {
        MApi.apiLib = MApi.VOLLEY;
        return new ApiIProms(context, serviceName);
    }
}