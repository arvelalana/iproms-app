package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiUpdateStatusDaily extends MApiService {

    public static String TAG = ApiUpdateStatusDaily.class.getSimpleName();

    public ApiUpdateStatusDaily(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/updateStatusDailyActivity";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String sessionId = shareData.get(SharedData.SESSION_ID);
        String orgId = shareData.get(SharedData.ORG_ID);
        HashMap data = api.getPostData();

        request = "";
        request += "session_id=" + sessionId;
        request += "&org_id=" + orgId;
//        request += "&daily_id=" + data.get("daily_id");
        ArrayList<String> dailyList = (ArrayList<String>) data.get("daily_id");
        for (int i = 0; i < dailyList.size(); i++) {
            request += "&daily_id[" + i + "]=" + dailyList.get(i);
        }
        request += "&daily_status=" + data.get("daily_status");
        request += "&reason=" + data.get("reason");
        request += "&token=" + FirebaseInstanceId.getInstance().getToken();


        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
