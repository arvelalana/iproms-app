package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.itant.iproms.R;

/**
 * Created by x441s on 05/09/2017.
 */

public class LoadingSmallReverseDialog extends Dialog {

    protected Context context;

    public LoadingSmallReverseDialog(Context _context) {
        super(_context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.context = _context;
        init();
    }

    public void init() {
        setContentView(R.layout.dialog_small_loading_reverse);
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }
}

