package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.itant.iproms.R;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterNotif;
import com.itant.iproms.lib.adapter.AdapterTaskDashboard;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Notif;
import com.itant.iproms.lib.object.NotifList;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by x441s on 05/09/2017.
 */

public class NotifDialog extends Dialog {

    protected Toolbar toolbar;
    protected MTextView toolbarTitle;
    protected Context context;

    protected AdapterNotif adapterNotif;
    protected RecyclerView notifRecycleView;
    protected GridLayoutManager mLayoutManager;
    protected ImageView imageViewBell;

    public NotifDialog(Context _context) {
        super(_context, R.style.AppTheme);
        this.context = _context;
        init();
    }

    public void init() {
        setContentView(R.layout.dialog_notif);
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("KALENDER");
        FontStyle.toolbarTitle(context, toolbarTitle);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

            }
        });
        imageViewBell = findViewById(R.id.image_notif_bell);
        imageViewBell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        //Recycleview bot
        notifRecycleView = findViewById(R.id.notif_recycle_view);
        mLayoutManager = new GridLayoutManager(context, 1);
        notifRecycleView.setLayoutManager(mLayoutManager);
        notifRecycleView.setItemAnimator(new DefaultItemAnimator());
        doApiNotifFilter("");
    }

    public void show() {
        doApiNotifFilter("");
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }

    public void doApiNotifFilter(final String page) {
        try {
            ApiIProms apiIProms = ApiIProms.factory(context, "NotificationList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Notif notif = Notif.getInstance(context);
                        NotifList notifListArr = NotifList.getInstance(context);
                        ArrayList<Notif> tempArraylist = new ArrayList<Notif>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONArray data = responseObj.getJSONArray("data");
                            int pageCount = 0;

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject notifObj = (JSONObject) data.get(i);
                                notif = new Notif();

                                if (notifObj.getString("notif_message_detail_id").length() > 0) {
                                    pageCount += 1;
                                }

                                notif.setId(notifObj.getString("notif_message_detail_id"));
                                notif.setTitle(notifObj.getString("title"));
                                notif.setBody(notifObj.getString("message"));
                                notif.setRead(notifObj.getString("read"));
                                notif.setModule(notifObj.getString("module"));
                                notif.setRefId(notifObj.getString("reference_id"));
                                notif.setCreatedDate(notifObj.getString("created"));

                                tempArraylist.add(notif);
                            }

                            notif = Notif.getInstance(context);

                            notifListArr.setNotifArrayList(tempArraylist);
                            adapterNotif = new AdapterNotif(context, notifListArr.getNotifArrayList());
                            notifRecycleView.setAdapter(adapterNotif);


                        } else {
                        }
                    } catch (JSONException e) {
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }
}

