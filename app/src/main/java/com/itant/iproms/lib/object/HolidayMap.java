package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.HashMap;

/**
 * Created by x441s on 06/09/2017.
 */

public class HolidayMap {

    public static HolidayMap instance = null;
    protected Context context;

    protected HashMap<String, Holiday> holidayMap;

    public HolidayMap(Context context) {
        holidayMap = new HashMap<String, Holiday>();
        this.context = context;
    }


    public static HolidayMap getInstance(Context context) {
        if (instance == null) {
            instance = new HolidayMap(context);
        }
        return instance;
    }

    public void add(Holiday holiday) {
        this.holidayMap.put(holiday.getDate(), holiday);
    }

    public void clear() {
        this.holidayMap.clear();
    }

    public HashMap<String, Holiday> getHolidayMap() {
        return holidayMap;
    }

    public void setHolidayMap(HashMap<String, Holiday> holidayMap) {
        this.holidayMap = holidayMap;
    }
}
