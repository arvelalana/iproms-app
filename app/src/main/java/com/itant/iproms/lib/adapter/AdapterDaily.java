package com.itant.iproms.lib.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itant.iproms.DetailDailyActivity;
import com.itant.iproms.MyDailyActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.MultiSelect;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Raymond on 2/28/2017.
 */

public class AdapterDaily extends RecyclerView.Adapter<AdapterDaily.DailyActivityViewHolder> {

    private static final String TAG = AdapterDaily.class.getSimpleName();

    private Context mContext;
    private ArrayList<DailyActivity> dailyActivityArrayList;
    private Boolean isSelectable;
    private Boolean isFirst;

    public AdapterDaily(Context context, ArrayList dailyActivityArrayList, Boolean isSelectable) {
        this.mContext = context;
        this.dailyActivityArrayList = dailyActivityArrayList;
        this.isSelectable = isSelectable;
    }

    public void clearAll() {
//        int size = this.restaurantList.size();
//        if (size > 0) {
//            for (int i = 0; i < size; i++) {
//                restaurantList.remove(0);
//            }
//            this.notifyItemRangeRemoved(0, size);
//        }
    }

    @Override
    public DailyActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_daily_activity, parent, false);

        return new DailyActivityViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DailyActivityViewHolder holder, int position) {
        final Boolean[] isSelected = {false};
        final Boolean[] isFirst = {false};
        final Boolean[] fromDownClick = {false};
        final MultiSelect multiSelect = MultiSelect.getInstance(mContext);
        Random rnd = new Random();
        int color = rnd.nextInt(7);
        final DailyActivity dailyActivity = dailyActivityArrayList.get(position);
        holder.tvAdapterDailyActivityName.setText(dailyActivity.getDailyActivityName());
        System.out.println(mContext.getApplicationContext());
        if (dailyActivity.getApprovable().equalsIgnoreCase("1")) {
            if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("ACCEPT") || dailyActivity.getDailyActivityStatus().equalsIgnoreCase("REJECT")) {
                this.isSelectable = false;
            }
            holder.tvAdapterDailyActivityDescription.setText(mContext.getString(R.string.daily_activity_approvable) + " " + dailyActivity.getUsersCreator());
        } else {
            if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("ACTIVE")) {
                holder.tvAdapterDailyActivityDescription.setText(mContext.getString(R.string.daily_activity_waiting) + " " + dailyActivity.getUserApproveName());

            } else if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("ACCEPT")) {
                holder.tvAdapterDailyActivityDescription.setText(mContext.getString(R.string.daily_activity_accepted) + " " + dailyActivity.getUserApproveName());

            } else if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("REJECT")) {
                holder.tvAdapterDailyActivityDescription.setText(mContext.getString(R.string.daily_activity_rejected) + " " + dailyActivity.getUserApproveName());
            }
        }
//        holder.layoutAdapterDailyActivity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                System.out.println("clicked");
//                if (multiSelect.getToggleOn()) {
////                    if (isSelected) {
////                        isSelected = false;
////                        holder.layoutAdapterDailyActivity.setBackground(mContext.getDrawable(R.drawable.black_border));
////
////                    } else {
////                        holder.layoutAdapterDailyActivity.setBackgroundColor(mContext.getColor(R.color.md_grey_300));
////                    }
//                } else {
//                    Intent intent = new Intent(mContext, DetailDailyActivity.class);
//                    intent.putExtra("dailyId", dailyActivity.getId());
//                    mContext.startActivity(intent);
//                    ((Activity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
//                }
//            }
//        });
        if (dailyActivity.getApprovable().equalsIgnoreCase("1") && dailyActivity.getDailyActivityStatus().equalsIgnoreCase("ACTIVE") ) {
            isSelectable = true;
        }


        if (isSelectable) {

            holder.layoutAdapterDailyActivity.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    System.out.println("onLongClick : active");
                    multiSelect.setToggleOn(true);
                    isFirst[0] = true;
                    return false;
                }
            });

            holder.layoutAdapterDailyActivity.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        System.out.println(dailyActivity.getDailyActivityName());
                    }
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                        fromDownClick[0] = false;

                        if (multiSelect.getSelectedDailySize() == 0 && !multiSelect.getToggleOn()) {
                            Intent intent = new Intent(mContext, DetailDailyActivity.class);
                            intent.putExtra("dailyId", dailyActivity.getId());
                            mContext.startActivity(intent);
                            ((Activity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
                        }

                        if (isSelected[0]) {
                            fromDownClick[0] = true;
                            isSelected[0] = false;
                            multiSelect.removeSelectedDaily(dailyActivity.getId());
                            holder.layoutAdapterDailyActivity.setBackground(mContext.getDrawable(R.drawable.black_border));
                            holder.imageViewDailyActivity.setImageDrawable(mContext.getDrawable(R.drawable.ic_daily_activity));
                            System.out.println(multiSelect.getSelectedDailySize());
                            if (multiSelect.getSelectedDailySize() == 0) {
                                System.out.println("NOTHING LEFT");
                                multiSelect.setToggleOn(false);
                                MyDailyActivity.selectionOff(mContext);
                            }
                        }
                        if (multiSelect.getToggleOn() && !fromDownClick[0]) {
                            isSelected[0] = true;
                            MyDailyActivity.selectionOn();
                            multiSelect.addSelectedDaily(dailyActivity.getId());
                            holder.layoutAdapterDailyActivity.setBackground(mContext.getDrawable(R.drawable.black_border_grey_bg));
                            holder.imageViewDailyActivity.setImageDrawable(mContext.getDrawable(R.drawable.ic_check_white));
                        }


                        System.out.println("isSelected" + isSelected[0]);
                    }
                    return false;
                }
            });

        } else {
            holder.layoutAdapterDailyActivity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!multiSelect.getToggleOn()) {
                        Intent intent = new Intent(mContext, DetailDailyActivity.class);
                        intent.putExtra("dailyId", dailyActivity.getId());
                        mContext.startActivity(intent);
                        ((Activity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
                    }
                }
            });
        }

        holder.circleImageView.setImageResource(R.color.md_amber_500);
        if (color == 0) {
            holder.circleImageView.setImageResource(R.color.md_red_500);
        }
        if (color == 1) {
            holder.circleImageView.setImageResource(R.color.md_blue_500);
        }
        if (color == 2) {
            holder.circleImageView.setImageResource(R.color.md_green_500);
        }
        if (color == 3) {
            holder.circleImageView.setImageResource(R.color.md_yellow_500);
        }
        if (color == 4) {
            holder.circleImageView.setImageResource(R.color.md_purple_500);
        }
        if (color == 5) {
            holder.circleImageView.setImageResource(R.color.md_deep_purple_500);
        }
        if (color == 6) {
            holder.circleImageView.setImageResource(R.color.md_light_green_500);
        }
        if (color == 7) {
            holder.circleImageView.setImageResource(R.color.md_light_blue_500);
        }
        holder.tvAdapterDailyActivityStatus.setText(dailyActivity.getDailyActivityStatusText());
        if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("ACTIVE")) {
            holder.tvAdapterDailyActivityStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_500));
        } else if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("ACCEPT")) {
            holder.tvAdapterDailyActivityUpdate.setVisibility(View.VISIBLE);
            holder.tvAdapterDailyActivityUpdate.setText(dailyActivity.getUpdateDate());
            holder.tvAdapterDailyActivityUpdate.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
            holder.tvAdapterDailyActivityStatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
        } else if (dailyActivity.getDailyActivityStatus().equalsIgnoreCase("REJECT")) {
            holder.tvAdapterDailyActivityUpdate.setVisibility(View.VISIBLE);
            holder.tvAdapterDailyActivityUpdate.setText(dailyActivity.getUpdateDate());
            holder.tvAdapterDailyActivityUpdate.setTextColor(mContext.getResources().getColor(R.color.md_red_500));
            holder.tvAdapterDailyActivityStatus.setTextColor(mContext.getResources().getColor(R.color.md_red_500));
        }
        holder.tvAdapterDailyActivityDate.setText(dailyActivity.getStartDateOnly());
        holder.tvAdapterDailyActivityEndDate.setText(dailyActivity.getDeadlineDateOnly());
        holder.tvAdapterDailyActivityStartTime.setText(dailyActivity.getStartHour());
        holder.tvAdapterDailyActivityEndTime.setText(dailyActivity.getEndHour());
    }

    @Override
    public int getItemCount() {
        return dailyActivityArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class DailyActivityViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout layoutAdapterDailyActivity;
        public TextView tvAdapterDailyActivityName, tvAdapterDailyActivityDescription, tvAdapterDailyActivityStatus,
                tvAdapterDailyActivityDate, tvAdapterDailyActivityEndDate, tvAdapterDailyActivityUpdate,tvAdapterDailyActivityStartTime, tvAdapterDailyActivityEndTime;
        public CircleImageView circleImageView;
        public ImageView imageViewDailyActivity;

        public DailyActivityViewHolder(View view) {
            super(view);
            layoutAdapterDailyActivity = view.findViewById(R.id.layout_adapter_daily_activity);
            tvAdapterDailyActivityName = view.findViewById(R.id.tv_adapter_daily_activity_name);
            tvAdapterDailyActivityDescription = view.findViewById(R.id.tv_adapter_daily_activity_description);
            tvAdapterDailyActivityStatus = view.findViewById(R.id.tv_adapter_daily_activity_status);
            tvAdapterDailyActivityDate = view.findViewById(R.id.tv_adapter_daily_activity_date);
            tvAdapterDailyActivityEndDate = view.findViewById(R.id.tv_adapter_daily_activity_end_date);
            tvAdapterDailyActivityStartTime = view.findViewById(R.id.tv_adapter_daily_activity_start_time);
            tvAdapterDailyActivityEndTime= view.findViewById(R.id.tv_adapter_daily_activity_end_time);
            tvAdapterDailyActivityUpdate = view.findViewById(R.id.tv_adapter_date_status);
            circleImageView = view.findViewById(R.id.circle_background_image);
            imageViewDailyActivity = view.findViewById(R.id.iv_adapter_daily_activity);
        }
    }


}
