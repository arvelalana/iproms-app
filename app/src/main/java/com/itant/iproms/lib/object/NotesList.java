package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class NotesList {

    public static NotesList instance = null;
    protected Context context;

    protected ArrayList<Notes> notesArrayList;

    public NotesList(Context context) {
        notesArrayList = new ArrayList<Notes>();
        this.context = context;
    }


    public static NotesList getInstance(Context context) {
        if (instance == null) {
            instance = new NotesList(context);
        }
        return instance;
    }

    public void add(Notes notes) {
        this.notesArrayList.add(notes);
    }

    public void clear() {
        this.notesArrayList.clear();
    }

    public Notes get(String id) {
        Notes notes = null;
        for (int i = 0; i < this.notesArrayList.size(); i++) {
            if (notesArrayList.get(i).getId().equalsIgnoreCase(id)) {
                notes = notesArrayList.get(i);
            }
        }
        return notes;
    }

    public Notes getPosition(int pos) {
        Notes notes = this.getNotesArrayList().get(pos);
        return notes;
    }

    public ArrayList<String> getAllNotesName() {
        ArrayList<String> notesNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.notesArrayList.size(); i++) {
            notesNameArrayList.add(this.notesArrayList.get(i).getNotesTitle().toString());
        }
        return notesNameArrayList;
    }

    public ArrayList<Notes> getNotesArrayList() {
        return notesArrayList;
    }

    public void setNotesArrayList(ArrayList<Notes> notesArrayList) {
        this.notesArrayList = notesArrayList;
    }

}
