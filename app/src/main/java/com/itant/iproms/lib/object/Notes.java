package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by ArvAL on 5/9/2017.
 */

public class Notes {

    public static Notes instance = null;
    protected Context context;
    protected String id;
    protected String meetingId;
    protected String meetingTitle;
    protected String notesTitle;
    protected String notesTopic;
    protected String userNotes;
    protected String userLeader;
    protected String userPic;
    protected String startDate;
    protected String startHour;
    protected String endHour;
    protected ArrayList<String> userAttendance;
    protected ArrayList<String> userNotAttendance;
    protected ArrayList<String> opinionCreator;
    protected ArrayList<String> opinion;
    protected String updated;
    protected String updatedBy;
    protected String created;
    protected String createdBy;

    public Notes() {

    }

    public Notes(Context context) {
        this.context = context;
    }

    public static Notes getInstance(Context context) {
        if (instance == null) {
            instance = new Notes(context);
        }
        return instance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }


    public String getNotesTopic() {
        return notesTopic;
    }

    public void setNotesTopic(String notesTopic) {
        this.notesTopic = notesTopic;
    }

    public String getUserNotes() {
        return userNotes;
    }

    public void setUserNotes(String userNotes) {
        this.userNotes = userNotes;
    }

    public String getUserLeader() {
        return userLeader;
    }

    public void setUserLeader(String userLeader) {
        this.userLeader = userLeader;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }


    public ArrayList<String> getOpinionCreator() {
        return opinionCreator;
    }

    public void setOpinionCreator(ArrayList<String> opinionCreator) {
        this.opinionCreator = opinionCreator;
    }

    public ArrayList<String> getOpinion() {
        return opinion;
    }

    public void setOpinion(ArrayList<String> opinion) {
        this.opinion = opinion;
    }

    public String getNotesTitle() {
        return notesTitle;
    }

    public void setNotesTitle(String notesTitle) {
        this.notesTitle = notesTitle;
    }

    public ArrayList<String> getUserAttendance() {
        return userAttendance;
    }

    public void setUserAttendance(ArrayList<String> userAttendance) {
        this.userAttendance = userAttendance;
    }

    public ArrayList<String> getUserNotAttendance() {
        return userNotAttendance;
    }

    public void setUserNotAttendance(ArrayList<String> userNotAttendance) {
        this.userNotAttendance = userNotAttendance;
    }

    public String getMeetingTitle() {
        return meetingTitle;
    }

    public void setMeetingTitle(String meetingTitle) {
        this.meetingTitle = meetingTitle;
    }


    public void addUserAttendance(String user) {
        this.userAttendance.add(user);
    }

    public void addUserNotAttendance(String user) {
        this.userNotAttendance.add(user);
    }

    public void addOpinionCreator(String opinionUser) {
        this.opinionCreator.add(opinionUser);
    }

    public void addOpinion(String opinionText) {
        this.opinion.add(opinionText);
    }

    public void clearAllArr(){
        this.userAttendance = new ArrayList<String>();
        this.userNotAttendance = new ArrayList<String>();
        this.opinionCreator = new ArrayList<String>();
        this.opinion = new ArrayList<String>();
        this.userAttendance.clear();
        this.userNotAttendance.clear();
        this.opinionCreator.clear();
        this.opinion.clear();
    }
    @Override
    public String toString() {
        return "Notes{" +
                "context=" + context +
                ", id='" + id + '\'' +
                ", meetingId='" + meetingId + '\'' +
                ", meetingTitle='" + meetingTitle + '\'' +
                ", notesTitle='" + notesTitle + '\'' +
                ", notesTopic='" + notesTopic + '\'' +
                ", userNotes='" + userNotes + '\'' +
                ", userLeader='" + userLeader + '\'' +
                ", userPic='" + userPic + '\'' +
                ", startDate='" + startDate + '\'' +
                ", startHour='" + startHour + '\'' +
                ", endHour='" + endHour + '\'' +
                ", userAttendance=" + userAttendance +
                ", userNotAttendance=" + userNotAttendance +
                ", opinionCreator=" + opinionCreator +
                ", opinion=" + opinion +
                ", updated='" + updated + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", created='" + created + '\'' +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
