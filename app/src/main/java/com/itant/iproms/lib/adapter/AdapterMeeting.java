package com.itant.iproms.lib.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.itant.iproms.DetailMeetingActivity;
import com.itant.iproms.MyMeetingActivity;
import com.itant.iproms.NewMeetingActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.object.Meeting;
import com.itant.iproms.mapp.MSharedData;

import java.util.ArrayList;
import java.util.Random;

import static com.itant.iproms.MyMeetingActivity.NEW_MEETING_REQUEST;

/**
 * Created by Raymond on 2/28/2017.
 */

public class AdapterMeeting extends RecyclerView.Adapter<AdapterMeeting.MeetingViewHolder> {

    private static final String TAG = AdapterMeeting.class.getSimpleName();

    private MSharedData sharedData;
    private Context mContext;
    private ArrayList<Meeting> meetingArrayList;

    public AdapterMeeting(Context context, ArrayList meetingArrayList) {
        this.mContext = context;
        this.meetingArrayList = meetingArrayList;
        sharedData = MSharedData.getInstance(mContext);
    }

    public void clearAll() {
//        int size = this.restaurantList.size();
//        if (size > 0) {
//            for (int i = 0; i < size; i++) {
//                restaurantList.remove(0);
//            }
//            this.notifyItemRangeRemoved(0, size);
//        }
    }

    @Override
    public MeetingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_meeting, parent, false);
        return new MeetingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MeetingViewHolder holder, int position) {
        Random rnd = new Random();
        final Meeting meeting = meetingArrayList.get(position);

        holder.tvAdapterMeetingName.setText(meeting.getMeetingName());
        if (meeting.getMeetingStatus().equalsIgnoreCase("ACCEPT")) {
            holder.tvAdapterMeetingStatus.setBackground(mContext.getResources().getDrawable(R.drawable.border_green));
            holder.tvAdapterMeetingStatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
            holder.tvAdapterMeetingStatus.setText("Disetujui");
        } else if (meeting.getMeetingStatus().equalsIgnoreCase("ACTIVE")) {

            holder.tvAdapterMeetingStatus.setBackground(mContext.getResources().getDrawable(R.drawable.border_orange));
            holder.tvAdapterMeetingStatus.setTextColor(mContext.getResources().getColor(R.color.md_orange_500));
            holder.tvAdapterMeetingStatus.setText(meeting.getMeetingStatus());
            holder.tvAdapterMeetingStatus.setText("Proses");
        } else if (meeting.getMeetingStatus().equalsIgnoreCase("CANCEL")) {
            holder.tvAdapterMeetingStatus.setBackground(mContext.getResources().getDrawable(R.drawable.border_red));
            holder.tvAdapterMeetingStatus.setTextColor(mContext.getResources().getColor(R.color.md_red_500));
            holder.tvAdapterMeetingStatus.setText(meeting.getMeetingStatus());
            holder.tvAdapterMeetingStatus.setText("Dibatalkan");
        }
        if (sharedData.get(SharedData.USER_ID).equalsIgnoreCase(meeting.getUserCreator())) {
            if (meeting.getMeetingStatus().equalsIgnoreCase("ACTIVE")) {
                holder.btnRescheduleMeeting.setVisibility(View.VISIBLE);
                holder.btnRescheduleMeeting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, NewMeetingActivity.class);
                        intent.putExtra("meetingId", meeting.getId());
                        mContext.startActivity(intent);
                    }
                });
            }
        }
        holder.tvAdapterMeetingCreator.setText(meeting.getUserCreatorName());
        holder.tvAdapterMeetingDate.setText(meeting.getStartDate());
        holder.tvAdapterMeetingHour.setText(meeting.getStartHour() + " " + meeting.getEndHour());
        holder.tvAdapterMeetingLocation.setText(meeting.getLocation());
        holder.layoutAdapterMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailMeetingActivity.class);
                intent.putExtra("meetingId", meeting.getId());
                mContext.startActivity(intent);
                ((Activity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            }
        });


    }

    @Override
    public int getItemCount() {
        return meetingArrayList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MeetingViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout layoutAdapterMeeting;
        public TextView tvAdapterMeetingName, tvAdapterMeetingDescription, tvAdapterMeetingStatus, tvAdapterMeetingDate, tvAdapterMeetingHour, tvAdapterMeetingLocation, tvAdapterMeetingCreator;
        public Button btnRescheduleMeeting;

        public MeetingViewHolder(View view) {
            super(view);
            layoutAdapterMeeting = view.findViewById(R.id.layout_adapter_meeting);
            tvAdapterMeetingName = view.findViewById(R.id.tv_adapter_meeting_name);
            tvAdapterMeetingStatus = view.findViewById(R.id.tv_adapter_meeting_status);
            tvAdapterMeetingDate = view.findViewById(R.id.tv_adapter_meeting_date);
            tvAdapterMeetingHour = view.findViewById(R.id.tv_adapter_meeting_hour);
            tvAdapterMeetingLocation = view.findViewById(R.id.tv_adapter_meeting_location);
            tvAdapterMeetingCreator = view.findViewById(R.id.tv_adapter_meeting_user_creator);
            btnRescheduleMeeting = view.findViewById(R.id.btn_reschedule_meeting);
        }
    }

}
