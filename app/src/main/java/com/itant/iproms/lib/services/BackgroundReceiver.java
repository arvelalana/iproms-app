package com.itant.iproms.lib.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;

public class BackgroundReceiver extends BroadcastReceiver {
    public static final int BACKGROUND_RECEIVER_CODE = 777;
    @Override
    public void onReceive(Context context, Intent intent) {

//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(FirebaseJobService.class) // the JobService that will be called
//                .setTag("iPromsService")
//                .build();
//        dispatcher.mustSchedule(myJob);


        Intent background = new Intent(context, BackgroundService.class);
        context.startService(background);
        System.out.println("Arvel Service : Alarm Manager Trigerred");
    }
}
