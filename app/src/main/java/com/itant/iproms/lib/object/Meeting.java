package com.itant.iproms.lib.object;

import android.content.Context;

/**
 * Created by ArvAL on 5/9/2017.
 */

public class Meeting {

    public static Meeting instance = null;
    protected Context context;
    protected String id;
    protected String meetingCode;
    protected String meetingName;
    protected String description;
    protected String startDate;
    protected String startHour;
    protected String endHour;
    protected String meetingStatus;
    protected String updated;
    protected String updatedBy;
    protected String created;
    protected String createdBy;
    protected String location;
    protected String status;
    protected String userCreator;
    protected String userCreatorName;
    protected String haveNotes;
    protected UserList userList;

    public Meeting() {

    }

    public Meeting(Context context) {
        this.context = context;
    }

    public static Meeting getInstance(Context context) {
        if (instance == null) {
            instance = new Meeting(context);
        }
        return instance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMeetingCode() {
        return meetingCode;
    }

    public void setMeetingCode(String meetingCode) {
        this.meetingCode = meetingCode;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getMeetingStatus() {
        return meetingStatus;
    }

    public void setMeetingStatus(String meetingStatus) {
        this.meetingStatus = meetingStatus;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserList getUserList() {
        return userList;
    }

    public void setUserList(UserList userList) {
        this.userList = userList;
    }

    public String getUserCreator() {
        return userCreator;
    }

    public void setUserCreator(String userCreator) {
        this.userCreator = userCreator;
    }

    public String getUserCreatorName() {
        return userCreatorName;
    }

    public void setUserCreatorName(String userCreatorName) {
        this.userCreatorName = userCreatorName;
    }

    public Boolean getHaveNotes() {
        Boolean hvNotes = false;
        if (haveNotes.equalsIgnoreCase("1")) {
            hvNotes = true;
        }
        return hvNotes;
    }

    public void setHaveNotes(String haveNotes) {
        this.haveNotes = haveNotes;
    }

    @Override
    public String toString() {
        return "Meeting{" +
                "context=" + context +
                ", id='" + id + '\'' +
                ", meetingCode='" + meetingCode + '\'' +
                ", meetingName='" + meetingName + '\'' +
                ", description='" + description + '\'' +
                ", startDate='" + startDate + '\'' +
                ", startHour='" + startHour + '\'' +
                ", endHour='" + endHour + '\'' +
                ", meetingStatus='" + meetingStatus + '\'' +
                ", updated='" + updated + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", created='" + created + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", location='" + location + '\'' +
                ", status='" + status + '\'' +
                ", userCreator='" + userCreator + '\'' +
                ", userList=" + userList +
                '}';
    }
}
