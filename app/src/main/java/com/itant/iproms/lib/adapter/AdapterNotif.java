package com.itant.iproms.lib.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itant.iproms.DetailProjectActivity;
import com.itant.iproms.DetailTaskActivity;
import com.itant.iproms.MyProjectActivity;
import com.itant.iproms.MyTaskActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.object.Notif;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Raymond on 2/28/2017.
 */

public class AdapterNotif extends RecyclerView.Adapter<AdapterNotif.NotifViewHolder> {

    private static final String TAG = AdapterNotif.class.getSimpleName();

    private Context mContext;
    private ArrayList<Notif> notifArrayList;

    public AdapterNotif(Context context, ArrayList notifArrayList) {
        this.mContext = context;
        this.notifArrayList = notifArrayList;
    }

    public void clearAll() {
//        int size = this.restaurantList.size();
//        if (size > 0) {
//            for (int i = 0; i < size; i++) {
//                restaurantList.remove(0);
//            }
//            this.notifyItemRangeRemoved(0, size);
//        }
    }

    @Override
    public NotifViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notif, parent, false);

        return new NotifViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotifViewHolder holder, int position) {
        final Notif notif = notifArrayList.get(position);

        String moduleFirstWord = notif.getModule().substring(0, 3);
        holder.tvAdapterNotifTitle.setText(notif.getTitle());
        holder.tvAdapterNotifBody.setText(notif.getBody());
        holder.tvAdapterNotifDate.setText(notif.getCreatedDate());
//        holder.layoutAdapterNotif.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String moduleFirstWord = notif.getModule().substring(0, 3);
//                if (moduleFirstWord.equalsIgnoreCase("PJT")) {
//                    Intent intent = new Intent(mContext, DetailProjectActivity.class);
//                    intent.putExtra("taskId", notif.getId());
//                    mContext.startActivity(intent);
//                } else if (moduleFirstWord.equalsIgnoreCase("TDL")) {
//                    Intent intent = new Intent(mContext, DetailTaskActivity.class);
//                    intent.putExtra("taskId", notif.getId());
//                    mContext.startActivity(intent);
//                } else {
//
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return notifArrayList.size();
    }

    public class NotifViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout layoutAdapterNotif;
        public TextView tvAdapterNotifTitle, tvAdapterNotifBody, tvAdapterNotifDate;

        public NotifViewHolder(View view) {
            super(view);
            layoutAdapterNotif = view.findViewById(R.id.layout_adapter_notif);
            tvAdapterNotifTitle = view.findViewById(R.id.tv_adapter_notif_name);
            tvAdapterNotifBody = view.findViewById(R.id.tv_adapter_notif_description);
            tvAdapterNotifDate = view.findViewById(R.id.tv_adapter_notif_date);

        }
    }

}
