package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiKeepMeLoggedIn extends MApiService {

    public static String TAG = ApiKeepMeLoggedIn.class.getSimpleName();

    public ApiKeepMeLoggedIn(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/keepMeLoggedIn";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String sessionId = shareData.get(SharedData.SESSION_ID);
        String orgId = shareData.get(SharedData.ORG_ID);
        String username = shareData.get(SharedData.USERNAME);
        String user_token = shareData.get(SharedData.USER_TOKEN);
        HashMap data = api.getPostData();

        request = "";
        request += "session_id=" + sessionId;
        request += "&org_id=" + orgId;
        request += "&username=" + username;
        request += "&user_token=" + user_token;
        request += "&token=" + FirebaseInstanceId.getInstance().getToken();
        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
