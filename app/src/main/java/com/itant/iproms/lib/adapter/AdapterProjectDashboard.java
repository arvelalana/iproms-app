package com.itant.iproms.lib.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itant.iproms.DetailProjectActivity;
import com.itant.iproms.MainActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.Project;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Arvel on 2/28/2017.
 */

public class AdapterProjectDashboard extends RecyclerView.Adapter<AdapterProjectDashboard.ProjectViewHolder> {

    private static final String TAG = AdapterProjectDashboard.class.getSimpleName();

    private Context mContext;
    private ArrayList<Project> projectArrayList;

    public AdapterProjectDashboard(Context context, ArrayList projectArrayList) {
        this.mContext = context;
        this.projectArrayList = projectArrayList;
    }

    public void clearAll() {
//        int size = this.restaurantList.size();
//        if (size > 0) {
//            for (int i = 0; i < size; i++) {
//                restaurantList.remove(0);
//            }
//            this.notifyItemRangeRemoved(0, size);
//        }
    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_project_dashboard, parent, false);

        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, int position) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        final Project project = projectArrayList.get(position);
        holder.tvAdapterProjectName.setText(project.getProjectName());
        holder.tvAdapterProjectDescription.setText(project.getDescription());
        holder.imageProjectColor.setBackgroundColor(color);
        holder.layoutAdapterProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailProjectActivity.class);
                intent.putExtra("projectId", project.getId());
                mContext.startActivity(intent);
                ((MainActivity)mContext).overridePendingTransition(R.anim.coming_in,R.anim.coming_out);

            }
        });
        holder.tvAdapterProjectStatus.setText(project.getProjectStatus());
}

    @Override
    public int getItemCount() {
        return projectArrayList.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layoutAdapterProject;
        public TextView tvAdapterProjectName, tvAdapterProjectDescription, tvAdapterProjectStatus,tvAdapterProjectDate;
        public ImageView imageProjectColor;

        public ProjectViewHolder(View view) {
            super(view);
            layoutAdapterProject = view.findViewById(R.id.layout_adapter_project);
            tvAdapterProjectName = view.findViewById(R.id.tv_adapter_project_name);
            tvAdapterProjectDescription = view.findViewById(R.id.tv_adapter_project_description);
            tvAdapterProjectStatus = view.findViewById(R.id.tv_adapter_project_status);
            tvAdapterProjectDate= view.findViewById(R.id.tv_adapter_project_date);
            imageProjectColor = view.findViewById(R.id.image_project_color);

        }
    }

}
