package com.itant.iproms.lib.Helper;

import android.content.Context;
import android.graphics.Typeface;

import com.itant.iproms.R;
import com.itant.iproms.mapp.MFont;
import com.itant.iproms.mapp.mComponent.MTextView;


/**
 * Created by Raymond on 2/28/2017.
 */

public class FontStyle {

    public static void toolbarTitle(Context context, MTextView tv) {

        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/NirmalaUIBold.ttf");
        tv.setTextColor(context.getResources().getColor(R.color.colorTextWhite));
        tv.setTypeface(customFont);
        tv.setTextSize(22);
    }

    public static void nirmalaUIBoldExtraLargeWhite(Context context, MTextView tv) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/NirmalaUIBold.ttf");
        tv.setTextColor(context.getResources().getColor(R.color.colorTextWhite));
        tv.setTypeface(customFont);
        tv.setTextSize(35);
    }

    public static void nirmalaUIBoldExtraLarge(Context context, MTextView tv) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/NirmalaUIBold.ttf");
        tv.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        tv.setTypeface(customFont);
        tv.setTextSize(35);
    }

    public static void toolbarTitleReverse(Context context, MTextView tv) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/NirmalaUIBold.ttf");
        tv.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        tv.setTypeface(customFont);
        tv.setTextSize(22);
    }

    public static void summary(Context context, MTextView tv) {
        Typeface customFont = MFont.getInstance(context).getFont("opensanssemibold");
        tv.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        tv.setTypeface(customFont);
        tv.setTextSize(20);
    }

    public static void summaryBottomNewTransaction(Context context, MTextView tv) {
        Typeface customFont = MFont.getInstance(context).getFont("opensanssemibold");
        tv.setTypeface(customFont);
        tv.setTextSize(20);
    }
}
