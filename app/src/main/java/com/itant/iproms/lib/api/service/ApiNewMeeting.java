package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiNewMeeting extends MApiService {

    public static String TAG = ApiNewMeeting.class.getSimpleName();

    public ApiNewMeeting(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/newMeeting";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String sessionId = shareData.get(SharedData.SESSION_ID);
        String orgId = shareData.get(SharedData.ORG_ID);
        HashMap data = api.getPostData();

        request = "";
        request += "session_id=" + sessionId;
        request += "&org_id=" + orgId;
        request += "&meeting_name=" + data.get("meeting_name");
        request += "&description=" + data.get("description");
        request += "&start_date=" + data.get("start_date");
        request += "&start_hour=" + data.get("start_hour");
        request += "&end_hour=" + data.get("end_hour");
        request += "&location=" + data.get("location");
        request += "&mode=" + data.get("mode");

        if(data.get("mode").toString().equalsIgnoreCase("edit")){
            request += "&meeting_id=" + data.get("meeting_id");
        }

        request += "&token=" + FirebaseInstanceId.getInstance().getToken();
        ArrayList<String> users = (ArrayList<String>) data.get("users");
        for (int i = 0; i < users.size(); i++) {
            request += "&users[" + i + "]=" + users.get(i);
        }
        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
