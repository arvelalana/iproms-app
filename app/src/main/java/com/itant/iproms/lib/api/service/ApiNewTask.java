package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiNewTask extends MApiService {

    public static String TAG = ApiNewTask.class.getSimpleName();

    public ApiNewTask(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/newTask";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String sessionId = shareData.get(SharedData.SESSION_ID);
        String orgId = shareData.get(SharedData.ORG_ID);
        HashMap data = api.getPostData();

        request = "";
        request += "session_id=" + sessionId;
        request += "&org_id=" + orgId;
        request += "&project_id=" + data.get("project_id");
        request += "&task_name=" + data.get("task_name");
        request += "&description=" + data.get("description");
        request += "&start_date=" + data.get("start_date");
        request += "&end_date=" + data.get("end_date");
        ArrayList<String> users = (ArrayList<String>) data.get("users");
        for (int i = 0; i < users.size(); i++) {
            request += "&users[" + i + "]=" + users.get(i);
        }
        request += "&token=" + FirebaseInstanceId.getInstance().getToken();
        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
