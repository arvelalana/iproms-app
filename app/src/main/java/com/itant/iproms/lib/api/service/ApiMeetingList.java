package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiMeetingList extends MApiService {

    public static String TAG = ApiMeetingList.class.getSimpleName();

    public ApiMeetingList(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/meetingList";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String userId = shareData.get(SharedData.USER_ID);
        String sessionId = shareData.get(SharedData.SESSION_ID);
        HashMap data = api.getPostData();


        Filter filter = Filter.getInstance(api.getContext());
        request = "";
        request += "session_id=" + sessionId;
        if(data.get("start_date").toString().length()>0){
            request += "&start_date=" + data.get("start_date");
        }
        if(data.get("end_date").toString().length()>0){
            request += "&end_date=" + data.get("end_date");
        }
        request += "&sort_by=" + data.get("sort_by");
        request += "&status=" + data.get("status");
        request += "&page=" + data.get("page");

        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
