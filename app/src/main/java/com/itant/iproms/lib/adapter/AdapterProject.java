package com.itant.iproms.lib.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itant.iproms.MAppCompat;
import com.itant.iproms.MainActivity;
import com.itant.iproms.MyProjectActivity;
import com.itant.iproms.MyTaskActivity;
import com.itant.iproms.NewProjectActivity;
import com.itant.iproms.R;
import com.itant.iproms.DetailProjectActivity;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static com.itant.iproms.MyProjectActivity.NEW_PROJECT_REQUEST;

/**
 * Created by Raymond on 2/28/2017.
 */

public class AdapterProject extends RecyclerView.Adapter<AdapterProject.ProjectViewHolder> {

    private static final String TAG = AdapterProject.class.getSimpleName();
    private ApiIProms apiIProms;
    private Context mContext;
    private ArrayList<Project> projectArrayList;

    public AdapterProject(Context context, ArrayList projectArrayList) {
        this.mContext = context;
        this.projectArrayList = projectArrayList;
    }

    public void clearAll() {
//        int size = this.restaurantList.size();
//        if (size > 0) {
//            for (int i = 0; i < size; i++) {
//                restaurantList.remove(0);
//            }
//            this.notifyItemRangeRemoved(0, size);
//        }
    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_project, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, int position) {
        Random r = new Random();
        int randomNumber = r.nextInt(5) + 1;
        final Project project = projectArrayList.get(position);
        if (randomNumber == 1) {
            holder.layoutBackgroundProject.setBackground(mContext.getResources().getDrawable(R.drawable.note_blue));
        }
        if (randomNumber == 2) {
            holder.layoutBackgroundProject.setBackground(mContext.getResources().getDrawable(R.drawable.note_green));
        }
        if (randomNumber == 3) {
            holder.layoutBackgroundProject.setBackground(mContext.getResources().getDrawable(R.drawable.note_grey));
        }
        if (randomNumber == 4) {
            holder.layoutBackgroundProject.setBackground(mContext.getResources().getDrawable(R.drawable.note_orange));
        }
        if (randomNumber == 5) {
            holder.layoutBackgroundProject.setBackground(mContext.getResources().getDrawable(R.drawable.note_purple));
        }


        holder.tvAdapterProjectName.setText(project.getProjectName());
        holder.tvAdapterProjectDate.setText(project.getEndDate());
        holder.layoutAdapterProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailProjectActivity.class);
                intent.putExtra("projectId", project.getId());
                mContext.startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.coming_in,R.anim.coming_out);
            }
        });
        holder.imageMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(mContext, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.project_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit_project:
                                Intent intent = new Intent(mContext, NewProjectActivity.class);
                                intent.putExtra("mode", "edit");
                                intent.putExtra("projectId", project.getId());
                                ((Activity) mContext).startActivityForResult(intent, NEW_PROJECT_REQUEST);
                                break;
                            case R.id.close_project:

                                final MDialog messageDialog = new MDialog(mContext, R.style.dialogNoTitle);
                                messageDialog.setButtonType("yesno");
                                String strLblPopup = mContext.getResources().getString(R.string.close_project_ask);
                                messageDialog.setMessage(strLblPopup);
                                Button yesbutton = (Button) messageDialog.findViewById(R.id.btn_yes);
                                yesbutton.setText(mContext.getResources().getString(R.string.btn_msg_yes_in));
                                yesbutton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        doApiCloseProject(project.getId());
                                        messageDialog.dismiss();
                                    }
                                });
                                Button nobutton = (Button) messageDialog.findViewById(R.id.btn_no);
                                nobutton.setText(mContext.getResources().getString(R.string.btn_msg_no_in));
                                nobutton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        messageDialog.dismiss();

                                    }
                                });
                                messageDialog.show();
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return projectArrayList.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout layoutAdapterProject, layoutBackgroundProject;
        public TextView tvAdapterProjectName, tvAdapterProjectDate;
        public ImageView imageMore;

        public ProjectViewHolder(View view) {
            super(view);
            imageMore = view.findViewById(R.id.img_more_project);
            layoutBackgroundProject = view.findViewById(R.id.layout_background_project);
            layoutAdapterProject = view.findViewById(R.id.layout_adapter_project);
            tvAdapterProjectName = view.findViewById(R.id.tv_adapter_project_name);
            tvAdapterProjectDate = view.findViewById(R.id.tv_adapter_project_date);

        }
    }

    private void doApiCloseProject(final String projectId) {
        try {
            apiIProms = ApiIProms.factory(mContext, "CloseProject");
            HashMap postData = new HashMap();
            postData.put("project_id", projectId);

            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        if (errCode.equalsIgnoreCase("0")) {
                            Intent mainActivity = new Intent(mContext, MyProjectActivity.class);
                            mContext.startActivity(mainActivity);
                            ((Activity) mContext).finish();
                        } else {

                        }
                    } catch (JSONException e) {

                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

}
