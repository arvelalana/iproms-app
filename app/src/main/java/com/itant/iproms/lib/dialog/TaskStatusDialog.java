package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.itant.iproms.NewMyTaskActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.mapp.mComponent.MTextInputEditText;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by user on 2/17/2017.
 */

public class TaskStatusDialog extends Dialog implements DatePickerDialog.OnDateSetListener {


    private MTextView textTaskDialog;
    protected Context context;
    protected Button buttonNo, buttonYes;
    protected MTextInputEditText editTextTaskDialog, editTextTaskDate;
    protected MTextView tvHeader;
    private Boolean isOpen = false;
    protected String header, message, hint, messageYes, messageNo;
    protected FragmentManager fragmentManager;

    public TaskStatusDialog(Context _context, int themeResId) {
        super(_context, themeResId);
        this.context = _context;
        init();
    }

    public void init() {
        message = "";
        hint = "Alasan";
        setContentView(R.layout.dialog_task_message);
        tvHeader = findViewById(R.id.tv_task_message_dialog);
        editTextTaskDialog = findViewById(R.id.edit_task_message_dialog);
        editTextTaskDate = findViewById(R.id.edit_task_date_dialog);
        editTextTaskDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    datePickerOnClick();
                }
                return false;
            }
        });
        buttonNo = findViewById(R.id.btn_task_no);
        buttonYes = findViewById(R.id.btn_task_yes);
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TaskStatusDialog.super.dismiss();
            }
        });
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void show() {
        editTextTaskDialog.setHint(hint);
        tvHeader.setText(header);
        buttonNo.setText(messageNo);
        buttonYes.setText(messageYes);
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getMessage() {
        String messageText = "";
        messageText = editTextTaskDialog.getText().toString();
        return messageText;
    }

    public void setShowDate(Boolean showDate, FragmentManager fm) {
        fragmentManager = fm;
        if (showDate) {
            editTextTaskDate.setVisibility(View.VISIBLE);
        } else {
            editTextTaskDate.setVisibility(View.GONE);
        }
    }

    public String getPendingDate() {
        String pendingDate = editTextTaskDate.getText().toString();
        return pendingDate;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageYes() {
        return messageYes;
    }

    public void setMessageYes(String messageYes) {
        this.messageYes = messageYes;
    }

    public String getMessageNo() {
        return messageNo;
    }

    public void setMessageNo(String messageNo) {
        this.messageNo = messageNo;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        editTextTaskDate.setText(fullDate);

    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;
        title = "Tanggal Mulai";
        dateValue = editTextTaskDate.getText().toString();

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(fragmentManager, "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }

    public MTextView getTvHeader() {
        return tvHeader;
    }

    public void setTvHeader(MTextView tvHeader) {
        this.tvHeader = tvHeader;
    }

    public void setHeaderDisable() {
        this.tvHeader.setVisibility(View.GONE);
    }

    public void setInputDisable() {
        this.editTextTaskDialog.setVisibility(View.GONE);
    }

    public MTextInputEditText getEditTextTaskDate() {
        return editTextTaskDate;
    }

    public void setEditTextTaskDate(MTextInputEditText editTextTaskDate) {
        this.editTextTaskDate = editTextTaskDate;
    }
}
