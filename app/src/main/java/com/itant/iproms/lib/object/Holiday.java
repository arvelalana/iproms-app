package com.itant.iproms.lib.object;

import android.content.Context;

/**
 * Created by asus on 5/9/2017.
 */

public class Holiday {

    public static Holiday instance = null;
    protected Context context;
    protected String id;
    protected String name;
    protected String date;

    public Holiday() {

    }

    public Holiday(Context context) {
        this.context = context;
    }

    public static Holiday getInstance(Context context) {
        if (instance == null) {
            instance = new Holiday(context);
        }
        return instance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
