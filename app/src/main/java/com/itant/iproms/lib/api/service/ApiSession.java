package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;
import com.itant.iproms.mapp.mApi.OkHttpService;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Raymond on 12/31/2016.
 */

public class ApiSession extends MApiService {

    public static String TAG = ApiSession.class.getSimpleName();

    public ApiSession(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);

        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String authId = shareData.get(SharedData.AUTH_ID);

        urlServer = api.getDomain() + "session";
        request = "";
        request += "auth_id=" + authId;

        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
