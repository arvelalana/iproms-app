package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiDailyList extends MApiService {

    public static String TAG = ApiDailyList.class.getSimpleName();

    public ApiDailyList(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/dailyActivityList";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String userId = shareData.get(SharedData.USER_ID);
        String sessionId = shareData.get(SharedData.SESSION_ID);
        HashMap data = api.getPostData();

        String status = "";
        String filterStartDate = "";
        String filterEndDate = "";
        String user = "";
        status = data.get("status").toString();
        filterStartDate = data.get("start_date").toString();
        filterEndDate = data.get("end_date").toString();
        user = data.get("user").toString();

        Filter filter = Filter.getInstance(api.getContext());
        request = "";
        request += "session_id=" + sessionId;

        if (status.length() > 0) {
            request += "&status=" + status;
        }

        if (filterStartDate.length() > 0) {
            request += "&start_date=" + filterStartDate;
        }

        if (filterEndDate.length() > 0) {
            request += "&end_date=" + filterEndDate;
        }

        if (user.length() > 0) {
            request += "&user=" + user;
        }

        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
