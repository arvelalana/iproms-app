package com.itant.iproms.lib.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.itant.iproms.ReportPerformanceActivity;
import com.itant.iproms.R;
import com.itant.iproms.ReportActivity;
import com.itant.iproms.ReportTaskActivity;
import com.itant.iproms.ReportUserCountActivity;

/**
 * Created ARVEL ALANA on 02/11/2017.
 */

public class AdapterReport extends BaseAdapter {

    private Context mContext;

    public AdapterReport(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return gridTitle.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            view = layoutInflater.inflate(R.layout.adapter_report, null);
        }

        final ImageView gridImageView = view.findViewById(R.id.image_adapter_report);
        final TextView gridTextView = view.findViewById(R.id.tv_adapter_report);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gridId[i].equalsIgnoreCase("Performance")) {
                    Intent intent = new Intent(mContext, ReportPerformanceActivity.class);
                    mContext.startActivity(intent);
                    ((ReportActivity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
                }

                if (gridId[i].equalsIgnoreCase("Task")) {
                    Intent intent = new Intent(mContext, ReportTaskActivity.class);
                    mContext.startActivity(intent);
                    ((ReportActivity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
                }
                if (gridId[i].equalsIgnoreCase("UserActivity")) {
                    Intent intent = new Intent(mContext, ReportUserCountActivity.class);
                    mContext.startActivity(intent);
                    ((ReportActivity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
                }
            }
        });
        // 4
        gridImageView.setImageResource(gridPicture[i]);
        gridTextView.setText(gridTitle[i]);
        return view;
    }

    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    private String[] gridId = {
            "Performance", "Task", "UserActivity"
    };
    private String[] gridTitle = {
            "Kinerja", "Tugas", "Aktivitas User"
    };

    private Integer[] gridPicture = {
            R.drawable.ic_report_project, R.drawable.ic_report_task, R.drawable.ic_report_daily
    };
}
