package com.itant.iproms.lib.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.itant.iproms.DetailProjectActivity;
import com.itant.iproms.DetailTaskActivity;
import com.itant.iproms.MyTaskActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.object.Task;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Raymond on 2/28/2017.
 */

public class AdapterTask extends RecyclerView.Adapter<AdapterTask.TaskViewHolder> {

    private static final String TAG = AdapterTask.class.getSimpleName();

    private Context mContext;
    private ArrayList<Task> taskArrayList;

    public AdapterTask(Context context, ArrayList taskArrayList) {
        this.mContext = context;
        this.taskArrayList = taskArrayList;
    }

    public void clearAll() {
//        int size = this.restaurantList.size();
//        if (size > 0) {
//            for (int i = 0; i < size; i++) {
//                restaurantList.remove(0);
//            }
//            this.notifyItemRangeRemoved(0, size);
//        }
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_task, parent, false);

        return new TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        final Task task = taskArrayList.get(position);
        holder.tvAdapterTaskName.setText(task.getTaskName());
        holder.tvAdapterTaskDescription.setText(task.getDescription());
        holder.imageLetter.setLetter(task.getTaskName().substring(0));
        holder.imageLetter.setShapeColor(color);
        holder.layoutAdapterTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailTaskActivity.class);
                intent.putExtra("taskId", task.getId());
                mContext.startActivity(intent);
                ((Activity) mContext).overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            }
        });
        holder.tvAdapterTaskStatus.setText(task.getTaskStatusText());
        holder.tvAdapterTaskDate.setText(task.getStartDate());
        holder.tvAdapterTaskEndDate.setText(task.getEndDate());
    }

    @Override
    public int getItemCount() {
        return taskArrayList.size();
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout layoutAdapterTask;
        public TextView tvAdapterTaskName, tvAdapterTaskDescription, tvAdapterTaskStatus, tvAdapterTaskDate, tvAdapterTaskEndDate;
        public MaterialLetterIcon imageLetter;

        public TaskViewHolder(View view) {
            super(view);
            layoutAdapterTask = view.findViewById(R.id.layout_adapter_task);
            tvAdapterTaskName = view.findViewById(R.id.tv_adapter_task_name);
            tvAdapterTaskDescription = view.findViewById(R.id.tv_adapter_task_description);
            tvAdapterTaskStatus = view.findViewById(R.id.tv_adapter_task_status);
            tvAdapterTaskDate = view.findViewById(R.id.tv_adapter_task_date);
            tvAdapterTaskEndDate = view.findViewById(R.id.tv_adapter_task_end_date);
            imageLetter = view.findViewById(R.id.image_letter);
        }
    }

}
