package com.itant.iproms.lib.object;

import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.pchmn.materialchips.model.ChipInterface;

/**
 * Created by x441s on 05/09/2017.
 */

public class UserChip implements ChipInterface {

    private String id;
    private Uri avatarUri;
    private String fullName;

    public UserChip(String id,String fullName) {
        this.id = id;
        this.fullName = fullName;
    }

    @Override
    public Object getId() {
        return id;
    }

    @Override
    public Uri getAvatarUri() {
        return avatarUri;
    }

    @Override
    public Drawable getAvatarDrawable() {
        return null;
    }

    @Override
    public String getLabel() {
        return fullName;
    }

    @Override
    public String getInfo() {
        return null;
    }
}
