package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by asus on 5/9/2017.
 */

public class MultiSelect {

    public static MultiSelect instance = null;
    protected Context context;
    protected Boolean toggleOn = false;
    protected ArrayList selectedDaily;

    public MultiSelect() {

    }

    public MultiSelect(Context context) {
        selectedDaily = new ArrayList<String>();
        this.context = context;
    }

    public static MultiSelect getInstance(Context context) {
        if (instance == null) {
            instance = new MultiSelect(context);
        }
        return instance;
    }

    public Boolean getToggleOn() {
        return toggleOn;
    }

    public void setToggleOn(Boolean toggleOn) {
        this.toggleOn = toggleOn;
    }


    public void addSelectedDaily(String id) {
        this.selectedDaily.add(id);
    }

    public void removeSelectedDaily(String id) {
        this.selectedDaily.remove(id);
    }

    public void clearSelectedDaily() {
        this.selectedDaily.clear();
    }
    public ArrayList getSelectedDaily() {
        return selectedDaily;
    }

    public int getSelectedDailySize() {
        return selectedDaily.size();
    }

    public void setSelectedDaily(ArrayList selectedDaily) {
        this.selectedDaily = selectedDaily;
    }

}
