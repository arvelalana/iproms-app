package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class UserStatusList {

    public static UserStatusList instance = null;
    protected Context context;

    protected ArrayList<UserStatus> userStatusArrayList;

    public UserStatusList(Context context) {
        userStatusArrayList = new ArrayList<UserStatus>();
        this.context = context;
    }


    public static UserStatusList getInstance(Context context) {
        if (instance == null) {
            instance = new UserStatusList(context);
        }
        return instance;
    }

    public void add(UserStatus userStatus) {
        this.userStatusArrayList.add(userStatus);
    }

    public void clear() {
        this.userStatusArrayList.clear();
    }

    public UserStatus get(String id) {
        UserStatus userStatus = null;
        for (int i = 0; i < this.userStatusArrayList.size(); i++) {
            if (userStatusArrayList.get(i).getUserId().equalsIgnoreCase(id)) {
                userStatus = userStatusArrayList.get(i);
            }
        }
        return userStatus;
    }

    public int getIndex(String id) {
        int index = 0;
        for (int i = 0; i < this.userStatusArrayList.size(); i++) {
            if (userStatusArrayList.get(i).getUserId().equalsIgnoreCase(id)) {
                index=i;
            }
        }
        return index;
    }

    public UserStatus getPosition(int pos) {
        UserStatus userStatus = this.getUserStatusArrayList().get(pos);
        return userStatus;
    }

    public ArrayList<String> getAllUserStatusName() {
        ArrayList<String> userStatusNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.userStatusArrayList.size(); i++) {
            userStatusNameArrayList.add(this.userStatusArrayList.get(i).getUsername().toString());
        }
        return userStatusNameArrayList;
    }

    public ArrayList<UserStatus> getUserStatusArrayList() {
        return userStatusArrayList;
    }

    public void setUserStatusArrayList(ArrayList<UserStatus> userStatusArrayList) {
        this.userStatusArrayList = userStatusArrayList;
    }

}
