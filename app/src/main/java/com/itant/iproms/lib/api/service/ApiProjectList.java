package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiProjectList extends MApiService {

    public static String TAG = ApiProjectList.class.getSimpleName();

    public ApiProjectList(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/projectList";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String userId = shareData.get(SharedData.USER_ID);
        String sessionId = shareData.get(SharedData.SESSION_ID);
        HashMap data = api.getPostData();

        request = "";
        request += "session_id=" + sessionId;
        request += "&user_id=" + userId;
        request += "&show=" + data.get("show");
        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
