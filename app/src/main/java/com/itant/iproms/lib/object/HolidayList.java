package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class HolidayList {

    public static HolidayList instance = null;
    protected Context context;

    protected ArrayList<Holiday> holidayArrayList;

    public HolidayList(Context context) {
        holidayArrayList = new ArrayList<Holiday>();
        this.context = context;
    }


    public static HolidayList getInstance(Context context) {
        if (instance == null) {
            instance = new HolidayList(context);
        }
        return instance;
    }

    public void add(Holiday holiday) {
        this.holidayArrayList.add(holiday);
    }

    public void clear() {
        this.holidayArrayList.clear();
    }

    public Holiday get(String id) {
        Holiday holiday = null;
        for (int i = 0; i < this.holidayArrayList.size(); i++) {
            if (holidayArrayList.get(i).getId().equalsIgnoreCase(id)) {
                holiday = holidayArrayList.get(i);
            }
        }
        return holiday;
    }

    public Holiday getPosition(int pos) {
        Holiday holiday = this.getHolidayArrayList().get(pos);
        return holiday;
    }

    public ArrayList<String> getAllHolidayName() {
        ArrayList<String> holidayNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.holidayArrayList.size(); i++) {
            holidayNameArrayList.add(this.holidayArrayList.get(i).getName().toString());
        }
        return holidayNameArrayList;
    }

    public ArrayList<Holiday> getHolidayArrayList() {
        return holidayArrayList;
    }

    public void setHolidayArrayList(ArrayList<Holiday> holidayArrayList) {
        this.holidayArrayList = holidayArrayList;
    }

}
