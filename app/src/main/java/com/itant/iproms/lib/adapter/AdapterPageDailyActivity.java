package com.itant.iproms.lib.adapter;

/**
 * Created by PC on 23/10/2017.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.itant.iproms.lib.fragment.TabApprovalFragment;
import com.itant.iproms.lib.fragment.TabMyDailyFragment;

public class AdapterPageDailyActivity extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public AdapterPageDailyActivity(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabMyDailyFragment tabMyDailyFragment = new TabMyDailyFragment();
                return tabMyDailyFragment;
            case 1:
                TabApprovalFragment tabApprovalFragment = new TabApprovalFragment();
                return tabApprovalFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
