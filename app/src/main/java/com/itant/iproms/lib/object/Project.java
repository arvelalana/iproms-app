package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by asus on 5/9/2017.
 */

public class Project {

    public static Project instance = null;
    protected Context context;
    protected String projectName;
    protected String projectCode;
    protected String description;
    protected String projectStatus;
    protected String created;
    protected String createdBy;
    protected String id;
    protected String startDate;
    protected String endDate;
    protected String actualStartDate;
    protected String actualEndDate;
    protected UserList userList;

    public Project() {

    }

    public Project(Context context) {
        this.context = context;
        userList = new UserList(context);
    }

    public static Project getInstance(Context context) {
        if (instance == null) {
            instance = new Project(context);
        }
        return instance;
    }

    public String getProjectName() {

        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserList getUserList() {
        return userList;
    }

    public void setUserList(UserList userList) {
        this.userList = userList;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(String actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public String getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(String actualEndDate) {
        this.actualEndDate = actualEndDate;
    }
}
