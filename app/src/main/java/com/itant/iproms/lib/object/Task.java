package com.itant.iproms.lib.object;

import android.content.Context;

/**
 * Created by asus on 5/9/2017.
 */

public class Task {

    public static Task instance = null;
    protected Context context;
    protected String taskCode;
    protected String taskName;
    protected String description;
    protected String startDate;
    protected String endDate;
    protected String actualStartDate;
    protected String actualEndDate;
    protected String taskStatus;
    protected String created;
    protected String createdDate;
    protected String id;
    protected UserList userList;
    protected String userOrder;
    protected String userApprove;
    protected String projectName;
    protected String projectDate;
    protected String projectCreator;
    protected String taskReason;

    public Task() {

    }

    public Task(Context context) {
        this.context = context;
    }

    public static Task getInstance(Context context) {
        if (instance == null) {
            instance = new Task(context);
        }
        return instance;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTaskStatus() {
        if (taskStatus == null) {
            return "";
        }
        return taskStatus;
    }

    public String getTaskStatusText() {
        String textStatus = "";
        if (taskStatus == null) {
            return "";
        } else {

            if (taskStatus.equalsIgnoreCase("PENDING")) {
                textStatus = "Ditunda";
            }

            if (taskStatus.equalsIgnoreCase("IN_PROGRESS")) {
                textStatus = "Dalam Proses";
            }

            if (taskStatus.equalsIgnoreCase("DONE")) {
                textStatus = "Selesai";
            }

            if (taskStatus.equalsIgnoreCase("ACTIVE")) {
                textStatus = "Aktif";
            }

            if (taskStatus.equalsIgnoreCase("REQUEST")) {
                textStatus = "Request";
            }
        }

        return textStatus;
    }


    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserList getUserList() {
        return userList;
    }

    public void setUserList(UserList userList) {
        this.userList = userList;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDate() {
        return projectDate;
    }

    public void setProjectDate(String projectDate) {
        this.projectDate = projectDate;
    }

    public String getUserOrder() {
        if (userOrder == null) {
            return "";
        }
        return userOrder;
    }

    public void setUserOrder(String userOrder) {
        this.userOrder = userOrder;
    }

    public String getUserApprove() {
        if (userApprove == null) {
            return "";
        }
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public String getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(String actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public String getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(String actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public String getTaskReason() {
        return taskReason;
    }

    public void setTaskReason(String taskReason) {
        this.taskReason = taskReason;
    }

    public String getProjectCreator() {
        return projectCreator;
    }

    public void setProjectCreator(String projectCreator) {
        this.projectCreator = projectCreator;
    }

    @Override
    public String toString() {
        return "Task{" +
                "context=" + context +
                ", taskCode='" + taskCode + '\'' +
                ", taskName='" + taskName + '\'' +
                ", description='" + description + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", actualStartDate='" + actualStartDate + '\'' +
                ", actualEndDate='" + actualEndDate + '\'' +
                ", taskStatus='" + taskStatus + '\'' +
                ", created='" + created + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", id='" + id + '\'' +
                ", userList=" + userList +
                ", userOrder='" + userOrder + '\'' +
                ", userApprove='" + userApprove + '\'' +
                ", projectName='" + projectName + '\'' +
                ", projectDate='" + projectDate + '\'' +
                ", projectCreator='" + projectCreator + '\'' +
                ", taskReason='" + taskReason + '\'' +
                '}';
    }
}
