package com.itant.iproms.lib.Helper;

import android.content.Context;

import com.itant.iproms.MAppCompat;
import com.itant.iproms.lib.dialog.LoadingSmallDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by PC on 07/11/2017.
 */

public class Downloader {

    private static LoadingSmallDialog loadingSmallDialog;

    public static void DownloadFile(Context context, String fileURL, File directory) {
        System.out.println(fileURL);

        loadingSmallDialog = new LoadingSmallDialog(context);
        loadingSmallDialog.show();
        try {
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            if (loadingSmallDialog != null && loadingSmallDialog.isShowing()) {
                loadingSmallDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (loadingSmallDialog != null && loadingSmallDialog.isShowing()) {
                loadingSmallDialog.dismiss();
            }
        }

    }
}