package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class MeetingList {

    public static MeetingList instance = null;
    protected Context context;

    protected ArrayList<Meeting> meetingArrayList;

    public MeetingList(Context context) {
        meetingArrayList = new ArrayList<Meeting>();
        this.context = context;
    }


    public static MeetingList getInstance(Context context) {
        if (instance == null) {
            instance = new MeetingList(context);
        }
        return instance;
    }

    public void add(Meeting meeting) {
        this.meetingArrayList.add(meeting);
    }

    public void clear() {
        this.meetingArrayList.clear();
    }

    public Meeting get(String id) {
        Meeting meeting = null;
        for (int i = 0; i < this.meetingArrayList.size(); i++) {
            if (meetingArrayList.get(i).getId().equalsIgnoreCase(id)) {
                meeting = meetingArrayList.get(i);
            }
        }
        return meeting;
    }

    public Meeting getPosition(int pos) {
        Meeting meeting = this.getMeetingArrayList().get(pos);
        return meeting;
    }

    public ArrayList<String> getAllMeetingName() {
        ArrayList<String> meetingNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.meetingArrayList.size(); i++) {
            meetingNameArrayList.add(this.meetingArrayList.get(i).getMeetingName().toString());
        }
        return meetingNameArrayList;
    }

    public ArrayList<Meeting> getMeetingArrayList() {
        return meetingArrayList;
    }

    public void setMeetingArrayList(ArrayList<Meeting> meetingArrayList) {
        this.meetingArrayList = meetingArrayList;
    }

}
