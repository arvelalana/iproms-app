package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class NotifList {

    public static NotifList instance = null;
    protected Context context;

    protected ArrayList<Notif> notifArrayList;

    public NotifList(Context context) {
        notifArrayList = new ArrayList<Notif>();
        this.context = context;
    }


    public static NotifList getInstance(Context context) {
        if (instance == null) {
            instance = new NotifList(context);
        }
        return instance;
    }

    public void add(Notif notif) {
        this.notifArrayList.add(notif);
    }

    public void clear() {
        this.notifArrayList.clear();
    }

    public Notif get(String id) {
        Notif notif = null;
        for (int i = 0; i < this.notifArrayList.size(); i++) {
            if (notifArrayList.get(i).getId().equalsIgnoreCase(id)) {
                notif = notifArrayList.get(i);
            }
        }
        return notif;
    }

    public Notif getPosition(int pos) {
        Notif notif = this.getNotifArrayList().get(pos);
        return notif;
    }

    public ArrayList<String> getAllNotifName() {
        ArrayList<String> notifNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.notifArrayList.size(); i++) {
            notifNameArrayList.add(this.notifArrayList.get(i).getTitle().toString());
        }
        return notifNameArrayList;
    }

    public ArrayList<Notif> getNotifArrayList() {
        return notifArrayList;
    }

    public void setNotifArrayList(ArrayList<Notif> notifArrayList) {
        this.notifArrayList = notifArrayList;
    }

}
