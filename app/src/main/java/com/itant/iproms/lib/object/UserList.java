package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 07/09/2017.
 */

public class UserList {

    public static UserList instance = null;
    protected Context context;

    protected ArrayList<User> userArrayList;

    public UserList(Context context) {
        userArrayList = new ArrayList<User>();
        this.context = context;
    }

    public static UserList getInstance(Context context) {
        if (instance == null) {
            instance = new UserList(context);
        }
        return instance;
    }

    public void add(User user) {
        this.userArrayList.add(user);
    }

    public User get(String id) {
        User user = null;
        for (int i = 0; i < this.userArrayList.size(); i++) {
            if (userArrayList.get(i).getUserId().equalsIgnoreCase(id)) {
                user = userArrayList.get(i);
            }
        }
        return user;
    }

    public int getIndex(String id) {
        int index = 0;
        for (int i = 0; i < this.userArrayList.size(); i++) {
            if (userArrayList.get(i).getUserId().equalsIgnoreCase(id)) {
                index = i;
            }
        }
        return index;
    }

    public User getPosition(int pos) {
        User user = this.getUserArrayList().get(pos);
        return user;
    }

    public void clear() {
        this.userArrayList.clear();
    }

    public ArrayList<User> getUserArrayList() {
        return userArrayList;
    }

    public void setUserArrayList(ArrayList<User> userArrayList) {
        this.userArrayList = userArrayList;
    }

    public ArrayList<String> getAllUsername() {
        ArrayList<String> usernameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.userArrayList.size(); i++) {
            usernameArrayList.add(this.userArrayList.get(i).getFirstName().toString() + " " + this.userArrayList.get(i).getLastName().toString());
        }
        return usernameArrayList;
    }
}
