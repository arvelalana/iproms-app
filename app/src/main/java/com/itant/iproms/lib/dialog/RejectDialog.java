package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.itant.iproms.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by user on 2/17/2017.
 */

public class RejectDialog extends Dialog implements DatePickerDialog.OnDateSetListener {


    protected Context context;
    protected RelativeLayout relativeLayout;
    protected EditText editTextReason, editTextDate1, editTextDate2, editTextDate3;
    protected Button btnReject;
    protected FragmentManager fragmentManager;
    private Boolean isOpen = false;
    private int dateMode;

    public RejectDialog(Context _context, FragmentManager fm) {
        super(_context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.context = _context;
        fragmentManager = fm;
        init();
    }

    public void init() {
        setContentView(R.layout.dialog_reject);
        relativeLayout = findViewById(R.id.relative_dialog);
        editTextReason = findViewById(R.id.edit_text_reject_meeting_reason);
        editTextDate1 = findViewById(R.id.edit_text_reject_meeting_date_1);
        editTextDate2 = findViewById(R.id.edit_text_reject_meeting_date_2);
        editTextDate3 = findViewById(R.id.edit_text_reject_meeting_date_3);
        btnReject = findViewById(R.id.btn_reject_meeting);

        editTextDate1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = 1;
                    datePickerOnClick();
                }
                return false;
            }
        });
        editTextDate2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = 2;
                    datePickerOnClick();
                }
                return false;
            }
        });
        editTextDate3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = 3;
                    datePickerOnClick();
                }
                return false;
            }
        });
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }

    public EditText getEditTextReason() {
        return editTextReason;
    }

    public void setEditTextReason(EditText editTextReason) {
        this.editTextReason = editTextReason;
    }

    public EditText getEditTextDate1() {
        return editTextDate1;
    }

    public void setEditTextDate1(EditText editTextDate1) {
        this.editTextDate1 = editTextDate1;
    }

    public EditText getEditTextDate2() {
        return editTextDate2;
    }

    public void setEditTextDate2(EditText editTextDate2) {
        this.editTextDate2 = editTextDate2;
    }

    public EditText getEditTextDate3() {
        return editTextDate3;
    }

    public void setEditTextDate3(EditText editTextDate3) {
        this.editTextDate3 = editTextDate3;
    }

    public Button getBtnReject() {
        return btnReject;
    }

    public void setBtnReject(Button btnReject) {
        this.btnReject = btnReject;
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        String title = "";
        String dateValue = "";
        if (dateMode == 1) {
            title = "Tanggal Pending 1";
            dateValue = editTextDate1.getText().toString();
        } else if (dateMode == 2) {
            title = "Tanggal Pending 2";
            dateValue = editTextDate2.getText().toString();
        } else if (dateMode == 3) {
            title = "Tanggal Pending 3";
            dateValue = editTextDate3.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(fragmentManager, "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;
        if (dateMode == 1) {
            editTextDate1.setText(fullDate);
        } else if (dateMode == 2) {
            editTextDate2.setText(fullDate);
        } else if (dateMode == 3) {
            editTextDate3.setText(fullDate);
        }

    }
}
