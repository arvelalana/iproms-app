package com.itant.iproms.lib.component;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.itant.iproms.R;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.mapp.mComponent.MTextView;

/**
 * Created by user on 2/17/2017.
 */

public class LoadingFull extends Dialog {


    protected Context context;


    public LoadingFull(Context _context) {
        super(_context);
        this.context = _context;
        init();
    }

    public void init() {
        setContentView(R.layout.loading_full_layout);
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }
}
