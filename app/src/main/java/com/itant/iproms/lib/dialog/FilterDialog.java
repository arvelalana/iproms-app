package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.itant.iproms.R;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.mapp.mComponent.MTextView;

/**
 * Created by user on 2/17/2017.
 */

public class FilterDialog extends Dialog {


    private Toolbar toolbar;
    private MTextView toolbarTitle;
    protected Context context;
    protected Button buttonClose;


    public FilterDialog(Context _context) {
        super(_context, R.style.AppThemeNoBack);
        this.context = _context;
        init();
    }

    public void init() {
        setContentView(R.layout.dialog_filter);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        buttonClose = findViewById(R.id.button_close_dialog);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(context.getResources().getString(R.string.filter));
        FontStyle.toolbarTitle(context, toolbarTitle);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterDialog.super.dismiss();
            }
        });
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }
}
