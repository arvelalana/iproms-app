package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.itant.iproms.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by PC on 30/11/2017.
 */

public class FilterMeetingDialog extends Dialog implements DatePickerDialog.OnDateSetListener {


    protected Context context;
    protected RelativeLayout relativeLayout;
    protected EditText editTextDate1, editTextDate2, editTextLocation;
    protected Button btnOk;
    protected FragmentManager fragmentManager;
    private Boolean isOpen = false;
    private int dateMode;

    public FilterMeetingDialog(Context _context, FragmentManager fm) {
        super(_context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.context = _context;
        fragmentManager = fm;
        init();
    }

    public void init() {
        setContentView(R.layout.dialog_filter_meeting);
        relativeLayout = findViewById(R.id.relative_dialog);
        editTextDate1 = findViewById(R.id.edit_text_start_date_filter);
        editTextDate2 = findViewById(R.id.edit_text_end_date_filter);
        editTextLocation = findViewById(R.id.edit_text_meeting_location);
        btnOk = findViewById(R.id.btn_filter_meeting);

        editTextDate1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = 1;
                    datePickerOnClick();
                }
                return false;
            }
        });
        editTextDate2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = 2;
                    datePickerOnClick();
                }
                return false;
            }
        });
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }

    public EditText getEditTextDate1() {
        return editTextDate1;
    }

    public void setEditTextDate1(EditText editTextDate1) {
        this.editTextDate1 = editTextDate1;
    }

    public EditText getEditTextDate2() {
        return editTextDate2;
    }

    public void setEditTextDate2(EditText editTextDate2) {
        this.editTextDate2 = editTextDate2;
    }

    public EditText getEditTextLocation() {
        return editTextLocation;
    }

    public void setEditTextLocation(EditText editTextLocation) {
        this.editTextLocation = editTextLocation;
    }

    public Button getBtnOk() {
        return btnOk;
    }

    public void setBtnOk(Button btnOk) {
        this.btnOk = btnOk;
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        String title = "";
        String dateValue = "";
        if (dateMode == 1) {
            title = "Tanggal Pending 1";
            dateValue = editTextDate1.getText().toString();
        } else if (dateMode == 2) {
            title = "Tanggal Pending 2";
            dateValue = editTextDate2.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(fragmentManager, "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;
        if (dateMode == 1) {
            editTextDate1.setText(fullDate);
        } else if (dateMode == 2) {
            editTextDate2.setText(fullDate);
        }

    }
}