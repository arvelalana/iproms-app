package com.itant.iproms.lib.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.itant.iproms.lib.Helper.RunningCheck;

import java.util.concurrent.ExecutionException;

import static com.itant.iproms.lib.services.BackgroundReceiver.BACKGROUND_RECEIVER_CODE;

public class BackgroundService extends Service {

    private boolean isRunning;
    private Context context;
    private Thread backgroundThread;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        this.context = this;
        this.isRunning = false;
        this.backgroundThread = new Thread(myTask);
    }

    private Runnable myTask = new Runnable() {
        public void run() {
            final Boolean isRunning;
            try {
                isRunning = new RunningCheck().execute(context).get();
                if (isRunning) {
                    System.out.println("Arvel Service Aplikasi Aktif : " + " Hellow werld");
                } else {
                    Intent intent = new Intent(context, BackgroundReceiver.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, BACKGROUND_RECEIVER_CODE, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(pendingIntent);
                    boolean alarmRunning = (pendingIntent != null);

                    System.out.println("Arvel Service Tutup Service Sekarang Status : " + alarmRunning);
                    System.out.println("Arvel Service Aplikasi Tutup: " + " Hellow werld");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            stopSelf();
        }
    };

    @Override
    public void onDestroy() {
        this.isRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!this.isRunning) {
            this.isRunning = true;
            this.backgroundThread.start();
        }
        return START_STICKY;
    }

}
