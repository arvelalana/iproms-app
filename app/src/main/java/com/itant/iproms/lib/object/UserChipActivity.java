package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by x441s on 07/09/2017.
 */

public class UserChipActivity {

    public static UserChipActivity instance = null;
    protected Context context;

    protected ArrayList<UserChip> userChipArrayList;
    protected HashMap selectedChipList;


    public UserChipActivity(Context context) {
        this.context = context;
        userChipArrayList = new ArrayList<UserChip>();
        selectedChipList = new HashMap<>();
    }

    public static UserChipActivity getInstance(Context context) {
        if (instance == null) {
            instance = new UserChipActivity(context);
        }
        return instance;
    }

    public void add(UserChip userChip) {
        this.userChipArrayList.add(userChip);
    }

//    public User get(String id) {
//        User user = null;
//        for (int i = 0; i < this.userChipArrayList.size(); i++) {
//            if (userChipArrayList.get(i).getUserId().equalsIgnoreCase(id)) {
//                user = userChipArrayList.get(i);
//            }
//        }
//        return user;
//    }

    public UserChipActivity clear() {
        this.userChipArrayList.clear();
        return this;
    }

    public ArrayList<UserChip> getUserChipArrayList() {
        return userChipArrayList;
    }

    public void setUserChipArrayList(ArrayList<UserChip> userChipArrayList) {
        this.userChipArrayList = userChipArrayList;
    }

    public int getTotalUserChip(){
        return this.userChipArrayList.size();
    }

    public void addSelectedChipList(String key, UserChip value) {
        this.selectedChipList.put(key, value);
    }

    public void removeSelectedChipList(String key) {
        this.selectedChipList.remove(key);
    }

    public UserChipActivity clearSelectedChipList(){
        this.selectedChipList.clear();
        return this;
    }

    public HashMap getSelectedChipList() {
        return selectedChipList;
    }

    public void setSelectedChipList(HashMap selectedChipList) {
        this.selectedChipList = selectedChipList;
    }
}
