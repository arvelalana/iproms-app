package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.Date;

/**
 * Created by Arvel Alana on 26/09/2017.
 */

public class MyCalendar {

    public static MyCalendar instance = null;
    protected Date date;
    protected Context context;
    protected String selectedDate;

    public MyCalendar() {

    }

    public MyCalendar(Context context) {
        this.context = context;
    }

    public static MyCalendar getInstance(Context context) {
        if (instance == null) {
            instance = new MyCalendar(context);
        }
        return instance;
    }

    public Date getDate() {
        if (date == null) {
            Date now = new Date();
            date = now;
        }
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }
}
