package com.itant.iproms.lib.api.service;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Arvel on 08/29/2017.
 */

public class ApiUpdateStatusApprovalMeeting extends MApiService {

    public static String TAG = ApiUpdateStatusApprovalMeeting.class.getSimpleName();

    public ApiUpdateStatusApprovalMeeting(MApi api) {
        super(api);
    }

    public ArrayList<StringRequest> execute(MApi.ApiListener apiListener) {
        super.execute(apiListener);
        urlServer = api.getDomain() + "data/updateStatusApprovalMeeting";
        MSharedData shareData = MSharedData.getInstance(api.getContext());
        String sessionId = shareData.get(SharedData.SESSION_ID);
        String orgId = shareData.get(SharedData.ORG_ID);
        HashMap data = api.getPostData();

        request = "";
        request += "session_id=" + sessionId;
        request += "&org_id=" + orgId;
        request += "&meeting_employee_id=" + data.get("meeting_employee_id");
        request += "&meeting_status=" + data.get("meeting_status");
        if(data.get("meeting_status").toString().equalsIgnoreCase("reject")){
            request += "&pending_date1=" + data.get("pending_date1");
            request += "&pending_date2=" + data.get("pending_date2");
            request += "&pending_date3=" + data.get("pending_date3");
            request += "&reason=" + data.get("reason");
        }
        request += "&token=" + FirebaseInstanceId.getInstance().getToken();

        System.out.println(urlServer);
        System.out.println(request);

        ArrayList stringRequestArrayList = new ArrayList<>();
        Object stringRequest = this.doRequest();
        stringRequestArrayList.add(stringRequest);
        return stringRequestArrayList;
    }
}
