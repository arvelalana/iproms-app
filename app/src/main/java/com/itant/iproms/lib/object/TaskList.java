package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class TaskList {

    public static TaskList instance = null;
    protected Context context;

    protected ArrayList<Task> taskArrayList;

    public TaskList(Context context) {
        taskArrayList = new ArrayList<Task>();
        this.context = context;
    }


    public static TaskList getInstance(Context context) {
        if (instance == null) {
            instance = new TaskList(context);
        }
        return instance;
    }

    public void add(Task task) {
        this.taskArrayList.add(task);
    }

    public void clear() {
        this.taskArrayList.clear();
    }

    public Task get(String id) {
        Task task = null;
        for (int i = 0; i < this.taskArrayList.size(); i++) {
            if (taskArrayList.get(i).getId().equalsIgnoreCase(id)) {
                task = taskArrayList.get(i);
            }
        }
        return task;
    }

    public Task getPosition(int pos) {
        Task task = this.getTaskArrayList().get(pos);
        return task;
    }

    public ArrayList<String> getAllTaskName() {
        ArrayList<String> taskNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.taskArrayList.size(); i++) {
            taskNameArrayList.add(this.taskArrayList.get(i).getTaskName().toString());
        }
        return taskNameArrayList;
    }

    public ArrayList<Task> getTaskArrayList() {
        return taskArrayList;
    }

    public void setTaskArrayList(ArrayList<Task> taskArrayList) {
        this.taskArrayList = taskArrayList;
    }

}
