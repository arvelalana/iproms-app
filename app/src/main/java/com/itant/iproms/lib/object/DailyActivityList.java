package com.itant.iproms.lib.object;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by x441s on 06/09/2017.
 */

public class DailyActivityList {

    public static DailyActivityList instance = null;
    protected Context context;

    protected ArrayList<DailyActivity> dailyActivityArrayList;

    public DailyActivityList(Context context) {
        dailyActivityArrayList = new ArrayList<DailyActivity>();
        this.context = context;
    }


    public static DailyActivityList getInstance(Context context) {
        if (instance == null) {
            instance = new DailyActivityList(context);
        }
        return instance;
    }

    public void add(DailyActivity dailyActivity) {
        this.dailyActivityArrayList.add(dailyActivity);
    }

    public void clear() {
        this.dailyActivityArrayList.clear();
    }

    public DailyActivity get(String id) {
        DailyActivity dailyActivity = null;
        for (int i = 0; i < this.dailyActivityArrayList.size(); i++) {
            if (dailyActivityArrayList.get(i).getId().equalsIgnoreCase(id)) {
                dailyActivity = dailyActivityArrayList.get(i);
            }
        }
        return dailyActivity;
    }

    public int getIndex(String id) {
        int index = 0;
        for (int i = 0; i < this.dailyActivityArrayList.size(); i++) {
            if (dailyActivityArrayList.get(i).getId().equalsIgnoreCase(id)) {
                index=i;
            }
        }
        return index;
    }

    public DailyActivity getPosition(int pos) {
        DailyActivity dailyActivity = this.getDailyActivityArrayList().get(pos);
        return dailyActivity;
    }

    public ArrayList<String> getAllDailyActivityName() {
        ArrayList<String> dailyActivityNameArrayList = new ArrayList<String>();
        for (int i = 0; i < this.dailyActivityArrayList.size(); i++) {
            dailyActivityNameArrayList.add(this.dailyActivityArrayList.get(i).getId().toString());
        }
        return dailyActivityNameArrayList;
    }

    public ArrayList<DailyActivity> getDailyActivityArrayList() {
        return dailyActivityArrayList;
    }

    public void setDailyActivityArrayList(ArrayList<DailyActivity> dailyActivityArrayList) {
        this.dailyActivityArrayList = dailyActivityArrayList;
    }

}
