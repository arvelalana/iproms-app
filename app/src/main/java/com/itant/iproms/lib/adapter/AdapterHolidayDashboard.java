package com.itant.iproms.lib.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itant.iproms.MainActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.object.Holiday;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Arvel on 2/28/2017.
 */

public class AdapterHolidayDashboard extends RecyclerView.Adapter<AdapterHolidayDashboard.HolidayViewHolder> {

    private static final String TAG = AdapterHolidayDashboard.class.getSimpleName();

    private Context mContext;
    private ArrayList<Holiday> holidayArrayList;

    public AdapterHolidayDashboard(Context context, ArrayList holidayArrayList) {
        this.mContext = context;
        this.holidayArrayList = holidayArrayList;
    }

    public void clearAll() {
        
    }

    @Override
    public HolidayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_holiday_dashboard, parent, false);

        return new HolidayViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HolidayViewHolder holder, int position) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        final Holiday holiday = holidayArrayList.get(position);
        holder.tvAdapterHolidayName.setText(holiday.getName());
}

    @Override
    public int getItemCount() {
        return holidayArrayList.size();
    }

    public class HolidayViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layoutAdapterHoliday;
        public TextView tvAdapterHolidayName;
        public ImageView imageHolidayColor;

        public HolidayViewHolder(View view) {
            super(view);
            layoutAdapterHoliday = view.findViewById(R.id.layout_adapter_holiday);
            tvAdapterHolidayName = view.findViewById(R.id.tv_adapter_holiday_name);

        }
    }

}
