package com.itant.iproms.lib.object;

import android.content.Context;

/**
 * Created by asus on 5/9/2017.
 */

public class DailyActivity {

    public static DailyActivity instance = null;
    protected Context context;
    protected String dailyActivityCode;
    protected String dailyActivityName;
    protected String description;
    protected String startDate;
    protected String endDate;
    protected String deadlineDate;
    protected String dailyActivityStatus;
    protected String created;
    protected String createdDate;
    protected String id;
    protected String usersId;
    protected String usersCreator;
    protected String userApprove;
    protected String userApproveName;
    protected String approvable;
    protected String dailyActivityReason;
    protected String updateDate;
    protected String updateBy;

    public DailyActivity() {

    }

    public DailyActivity(Context context) {
        this.context = context;
    }

    public static DailyActivity getInstance(Context context) {
        if (instance == null) {
            instance = new DailyActivity(context);
        }
        return instance;
    }

    public String getDailyActivityCode() {
        return dailyActivityCode;
    }

    public void setDailyActivityCode(String dailyActivityCode) {
        this.dailyActivityCode = dailyActivityCode;
    }

    public String getDailyActivityName() {
        return dailyActivityName;
    }

    public void setDailyActivityName(String dailyActivityName) {
        this.dailyActivityName = dailyActivityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getStartDateOnly() {
        String[] startDateOnly = startDate.split(" ");
        return startDateOnly[0];
    }

    public String getStartHour() {
        String[] startDateOnly = startDate.split(" ");
        return startDateOnly[1];
    }

    public String getEndDateOnly() {
        String[] endDateOnly = endDate.split(" ");
        return endDateOnly[0];
    }

    public String getEndHour() {
        String[] endDateOnly = endDate.split(" ");
        return endDateOnly[1];
    }

    public String getDeadlineDateOnly() {
        String[] deadlineDateOnly = deadlineDate.split(" ");
        return deadlineDateOnly[0];
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(String deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public String getDailyActivityStatus() {
        if (dailyActivityStatus == null) {
            return "";
        }
        return dailyActivityStatus;
    }

    public String getDailyActivityStatusText() {
        String textStatus = "";
        if (dailyActivityStatus == null) {
            return "";
        } else {

            if (dailyActivityStatus.equalsIgnoreCase("PENDING")) {
                textStatus = "Ditunda";
            }

            if (dailyActivityStatus.equalsIgnoreCase("IN_PROGRESS")) {
                textStatus = "Dalam Proses";
            }

            if (dailyActivityStatus.equalsIgnoreCase("DONE")) {
                textStatus = "Selesai";
            }

            if (dailyActivityStatus.equalsIgnoreCase("ACTIVE")) {
                textStatus = "Aktif";
            }

            if (dailyActivityStatus.equalsIgnoreCase("REQUEST")) {
                textStatus = "Request";
            }

            if (dailyActivityStatus.equalsIgnoreCase("ACCEPT")) {
                textStatus = "Disetujui";
            }

            if (dailyActivityStatus.equalsIgnoreCase("REJECT")) {
                textStatus = "Ditolak";
            }
        }

        return textStatus;
    }


    public void setDailyActivityStatus(String dailyActivityStatus) {
        this.dailyActivityStatus = dailyActivityStatus;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsersId() {
        return usersId;
    }

    public void setUsersId(String usersId) {
        this.usersId = usersId;
    }

    public String getUserApprove() {
        if (userApprove == null) {
            return "";
        }
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }


    public String getDailyActivityReason() {
        return dailyActivityReason;
    }

    public void setDailyActivityReason(String dailyActivityReason) {
        this.dailyActivityReason = dailyActivityReason;
    }

    public String getUserApproveName() {
        return userApproveName;
    }

    public void setUserApproveName(String userApproveName) {
        this.userApproveName = userApproveName;
    }

    public String getApprovable() {
        return approvable;
    }

    public void setApprovable(String approvable) {
        this.approvable = approvable;
    }

    public String getUsersCreator() {
        return usersCreator;
    }

    public void setUsersCreator(String usersCreator) {
        this.usersCreator = usersCreator;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
}
