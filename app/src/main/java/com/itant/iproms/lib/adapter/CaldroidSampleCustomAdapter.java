package com.itant.iproms.lib.adapter;

/**
 * Created by x441s on 30/08/2017.
 */


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itant.iproms.MainActivity;
import com.itant.iproms.R;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.lib.object.HolidayMap;
import com.itant.iproms.lib.object.MyCalendar;
import com.itant.iproms.lib.object.ProjectList;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;
import com.roomorama.caldroid.CalendarHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import hirondelle.date4j.DateTime;

public class CaldroidSampleCustomAdapter extends CaldroidGridAdapter {

    public CaldroidSampleCustomAdapter(Context context, int month, int year,
                                       Map<String, Object> caldroidData,
                                       Map<String, Object> extraData) {
        super(context, month, year, caldroidData, extraData);
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cellView = convertView;
        // For reuse
        if (convertView == null) {
            cellView = inflater.inflate(R.layout.custom_cell, null);
        }

        int topPadding = cellView.getPaddingTop();
        int leftPadding = cellView.getPaddingLeft();
        int bottomPadding = cellView.getPaddingBottom();
        int rightPadding = cellView.getPaddingRight();

        final TextView tv1 = (TextView) cellView.findViewById(R.id.tv_date_number);
        TextView tv2 = (TextView) cellView.findViewById(R.id.tv_date_notif);
        final RelativeLayout rlBackground = cellView.findViewById(R.id.date_background);
        final FrameLayout dotNotif = cellView.findViewById(R.id.date_notif_red);
        tv1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        rlBackground.setBackground(null);
        dotNotif.setVisibility(View.GONE);

        // Get dateTime of this cell
        DateTime dateTime = this.datetimeList.get(position);
        Resources resources = context.getResources();

        final Date date = CalendarHelper.convertDateTimeToDate(dateTime);
        String dayText = new SimpleDateFormat("EEE").format(date);
        if (dayText.equalsIgnoreCase("Sun")) {
            tv1.setTextColor(resources
                    .getColor(R.color.md_red_600));
            tv1.setTypeface(null, Typeface.BOLD);
        }


        final MyCalendar myCalendar = MyCalendar.getInstance(context);
        //set on click
        cellView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myCalendar.setDate(date);
                notifyDataSetChanged();
            }
        });


        Calendar cal = Calendar.getInstance();

        if (myCalendar.getDate().getTime() == date.getTime()) {
            cal.setTime(myCalendar.getDate());
            Filter filter = Filter.getInstance(context);
            String dayDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            if (dayDate.length() == 1) {
                dayDate = "0" + dayDate;
            }

            String monthString = String.valueOf(cal.get(Calendar.MONTH) + 1);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }

            String todayDateString = dayDate + "-" + monthString + "-" + cal.get(Calendar.YEAR);
            System.out.println("hue" + todayDateString);
            filter.setPlanStartDate(todayDateString);
            filter.setPlanEndDate(todayDateString);
            myCalendar.setSelectedDate(todayDateString);
            tv1.setTextColor(context.getResources().getColor(R.color.colorWhite));
            rlBackground.setBackground(context.getResources().getDrawable(R.drawable.grey_circle));

//            ((MainActivity) context).doApiTaskFilter("0");

            String selectedDayDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            if (selectedDayDate.length() == 1) {
                selectedDayDate = "0" + selectedDayDate;
            }

            String selectedMonthDate = String.valueOf(cal.get(Calendar.MONTH) + 1);
            if (selectedMonthDate.length() == 1) {
                selectedMonthDate = "0" + selectedMonthDate;
            }

            String selectedDateString = cal.get(Calendar.YEAR)
                    + "-" + selectedMonthDate + "-" + selectedDayDate;
            ((MainActivity) context).refreshEventList(selectedDateString);
        }


        cal.setTime(date);

        String dayDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        if (dayDate.length() == 1) {
            dayDate = "0" + dayDate;
        }

        String monthString = String.valueOf(cal.get(Calendar.MONTH) + 1);
        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }

        String todayDateString = cal.get(Calendar.YEAR)
                + "-" + monthString + "-" + dayDate;
        HolidayMap holidayMap = HolidayMap.getInstance(context);

        if (holidayMap.getHolidayMap().get(todayDateString) != null) {
            tv1.setTextColor(resources
                    .getColor(R.color.md_red_600));
            tv1.setTypeface(null, Typeface.BOLD);
        }


//        ProjectMap projectMap = ProjectMap.getInstance(context);
//        if (projectMap.getProjectMap().get(todayDateString) != null) {
//            dotNotif.setVisibility(View.VISIBLE);
//        }

        ProjectList projectList = ProjectList.getInstance(context);
        for (int i = 0; i <= projectList.getProjectArrayList().size() - 1; i++) {
            if (todayDateString.equalsIgnoreCase(projectList.getProjectArrayList().get(i).getEndDate()) && toMilisecond(projectList.getProjectArrayList().get(i).getEndDate()) > toMilisecond(todayDateString)) {
                dotNotif.setVisibility(View.VISIBLE);
            }
        }

        // Set color of the dates in previous / next month
        if (dateTime.getMonth() != month) {
            tv1.setTextColor(resources
                    .getColor(com.caldroid.R.color.caldroid_darker_gray));
        }

        boolean shouldResetDiabledView = false;
        boolean shouldResetSelectedView = false;

        // Customize for disabled dates and date outside min/max dates
        if ((minDateTime != null && dateTime.lt(minDateTime))
                || (maxDateTime != null && dateTime.gt(maxDateTime))
                || (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

            tv1.setTextColor(CaldroidFragment.disabledTextColor);
            if (CaldroidFragment.disabledBackgroundDrawable == -1) {
                cellView.setBackgroundResource(com.caldroid.R.drawable.disable_cell);
            } else {
                cellView.setBackgroundResource(CaldroidFragment.disabledBackgroundDrawable);
            }


        } else {
            shouldResetDiabledView = true;
        }

        // Customize for selected dates
        if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
            cellView.setBackgroundColor(resources
                    .getColor(R.color.colorWhite));

            tv1.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        } else {
            shouldResetSelectedView = true;
        }

        if (shouldResetDiabledView && shouldResetSelectedView) {
            // Customize for today

            int resID = context.getResources().getIdentifier("blue_border", "drawable", context.getPackageName());
            if (dateTime.equals(getToday())) {
                tv1.setTextColor(context.getResources().getColor(R.color.colorWhite));
                rlBackground.setBackground(context.getResources().getDrawable(R.drawable.blue_border));
//                cellView.setBackgroundResource(resID);
            } else {
//                cellView.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
//                cellView.setBackgroundResource(com.caldroid.R.drawable.cell_bg);
            }


        }

        tv1.setText("" + dateTime.getDay());
        tv2.setText("Hi");

        // Somehow after setBackgroundResource, the padding collapse.
        // This is to recover the padding
        cellView.setPadding(leftPadding, topPadding, rightPadding,
                bottomPadding);

        // Set custom color if required
        setCustomResources(dateTime, cellView, tv1);

        return cellView;
    }

    protected long toMilisecond(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            return timeInMilliseconds;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

}