package com.itant.iproms.lib.Helper;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.itant.iproms.lib.dialog.LoadingSmallDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by PC on 07/11/2017.
 */

public class DownloadTask extends AsyncTask<String, Void, String> {

    private Exception exception;
    private File file;
    private Context context;
    private LoadingSmallDialog loadingSmallDialog;

    public DownloadTask(File file, Context context) {
        this.file = file;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        loadingSmallDialog = new LoadingSmallDialog(context);
        loadingSmallDialog.show();

    }

    protected String doInBackground(String... urls) {
        try {
            URL url = new URL(urls[0]);
            FileOutputStream f = new FileOutputStream(this.file);
            System.out.println(url);
            System.out.println(this.file);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
        } catch (Exception e) {
            this.exception = e;
        }
        return null;
    }

    protected void onPostExecute(String any) {
        if (loadingSmallDialog != null && loadingSmallDialog.isShowing()) {
            loadingSmallDialog.dismiss();
        }
        Toast.makeText(this.context, "File " + this.file.toString().substring(this.file.toString().lastIndexOf("/") + 1) + " telah berhasil disimpan di folder Download", Toast.LENGTH_LONG).show();
    }


    public void viewPDF() {
        File pdfFile = this.file;
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            context.startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this.context, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }
}