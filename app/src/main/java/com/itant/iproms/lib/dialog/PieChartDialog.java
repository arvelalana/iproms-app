package com.itant.iproms.lib.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.Window;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.itant.iproms.R;
import com.itant.iproms.lib.adapter.AdapterUserStatus;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.lib.object.UserStatus;
import com.itant.iproms.lib.object.UserStatusList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by x441s on 05/09/2017.
 */

public class PieChartDialog extends Dialog {

    protected Context context;
    private ApiIProms apiIProms;
    private String mode, userId, filterStartDate, filterEndDate;
    private UserStatus userStatus;
    private PieChart pieChart;

    public PieChartDialog(Context _context, UserStatus _userStatus, String _mode) {
        super(_context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.context = _context;
        this.userStatus = _userStatus;
        this.mode = _mode;
        init();
    }

    public void init() {
        setContentView(R.layout.dialog_pie_chart);
//         PIE CHART TEST
        doApiReportUserActivity();
        pieChart = findViewById(R.id.chart_counter);
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }


    public void doApiReportUserActivity() {
        try {
            apiIProms = ApiIProms.factory(context, "ReportUserActivityDetail");
            HashMap postData = new HashMap();
            postData.put("start_date", userStatus.getStartDate());
            postData.put("end_date", userStatus.getEndDate());
            postData.put("user_id", userStatus.getUserId());
            postData.put("mode", mode);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    System.out.println(response);
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            List<PieEntry> entries = new ArrayList<PieEntry>();
                            List<Integer> colours = new ArrayList<Integer>();
                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userActivityObj = (JSONObject) resultsArr.get(i);
                                int count = Integer.parseInt(userActivityObj.getString("count"));
                                String status = userActivityObj.getString("status");
                                entries.add(new PieEntry(count, getStatusText(status)));
                                colours.add(getColor(status));
                            }

                            PieDataSet dataSet = new PieDataSet(entries, "");
                            dataSet.setSliceSpace(2);
                            dataSet.setColors(colours);
                            dataSet.setValueTextColor(context.getResources().getColor(R.color.colorTextWhite));
                            dataSet.setValueTextSize(15);

                            PieData pieData = new PieData(dataSet);
                            Description description = new Description();
                            description.setText(userStatus.getUsername());
                            description.setTextSize(15);

                            if(mode.equalsIgnoreCase("task")){
                                pieChart.setCenterText("Data laporan tugas");
                            }else if(mode.equalsIgnoreCase("project")){
                                pieChart.setCenterText("Data laporan proyek");
                            }else if(mode.equalsIgnoreCase("daily")){
                                pieChart.setCenterText("Data laporan kinerja");
                            }

                            pieChart.setCenterTextSize(22);
                            pieChart.setTransparentCircleRadius(0);
                            pieChart.setDescription(description);
                            pieChart.setData(pieData);
                            pieChart.invalidate();

//                            closeDialog(activity);
                        } else {
//                            closeDialog(activity);
//                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
//                                    .show();
                        }
                    } catch (JSONException e) {
//                        closeDialog(activity);
//                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
//                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    private int getColor(String status) {
        int color = context.getResources().getColor(R.color.colorBlack);
        if (status.equalsIgnoreCase("ACTIVE")) {
            color = context.getResources().getColor(R.color.md_teal_500);
        }

        if (status.equalsIgnoreCase("CANCEL")) {
            color = context.getResources().getColor(R.color.md_brown_500);
        }

        if (status.equalsIgnoreCase("DONE")) {
            color = context.getResources().getColor(R.color.md_blue_500);
        }

        if (status.equalsIgnoreCase("IN_PROGRESS")) {
            color = context.getResources().getColor(R.color.md_amber_500);
        }

        if (status.equalsIgnoreCase("PENDING")) {
            color = context.getResources().getColor(R.color.md_orange_500);
        }

        if (status.equalsIgnoreCase("ACCEPT")) {
            color = context.getResources().getColor(R.color.md_green_500);
        }

        if (status.equalsIgnoreCase("REJECT")) {
            color = context.getResources().getColor(R.color.md_red_500);
        }

        if (status.equalsIgnoreCase("REQUEST")) {
            color = context.getResources().getColor(R.color.md_purple_500);
        }
        if (status.equalsIgnoreCase("CLOSED")) {
            color = context.getResources().getColor(R.color.md_red_500);
        }
        return color;
    }


    private String getStatusText(String status) {
        String newStatus = "";
        if (status.equalsIgnoreCase("ACTIVE")) {
            newStatus = "Aktif";
        }

        if (status.equalsIgnoreCase("CANCEL")) {
            newStatus = "Dibatalkan";
        }

        if (status.equalsIgnoreCase("DONE")) {
            newStatus = "Selesai";
        }

        if (status.equalsIgnoreCase("IN_PROGRESS")) {
            newStatus = "Proses";
        }

        if (status.equalsIgnoreCase("PENDING")) {
            newStatus = "Pending";
        }

        if (status.equalsIgnoreCase("ACCEPT")) {
            newStatus = "Disetujui";
        }

        if (status.equalsIgnoreCase("REJECT")) {
            newStatus = "Ditolak";
        }

        if (status.equalsIgnoreCase("REQUEST")) {
            newStatus = "Request";
        }
        if (status.equalsIgnoreCase("CLOSED")) {

            newStatus = "Ditutup";
        }
        return newStatus;
    }
}

