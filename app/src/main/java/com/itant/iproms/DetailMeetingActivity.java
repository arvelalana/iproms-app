package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.RejectDialog;
import com.itant.iproms.lib.dialog.TaskStatusDialog;
import com.itant.iproms.lib.object.Meeting;
import com.itant.iproms.lib.object.MeetingList;
import com.itant.iproms.lib.object.Notes;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.itant.iproms.MyMeetingActivity.NEW_MEETING_REQUEST;

public class DetailMeetingActivity extends MAppCompat {

    private Activity activity;
    private Toolbar toolbar;
    private DrawerLayout layoutMain;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private MSharedData sharedData;
    private String meetingId, meetingName, userCreator;
    private MeetingList meetingList;
    private Button btnReconfig, btnProcess, btnCancel, btnAccept, btnReject, btnCreateNotes, btnSeeNotes;
    private TextView tvCalendar, tvDescription, tvHour, tvLocation, tvUserCreator, tvStatus, tvAttendant, tvAttendantAccept, tvAttendantReject;
    private FrameLayout frameStatus;
    //    private FlexboxLayout flexboxAttendant, flexboxAttendantAccept, flexboxAttendantReject;
    private LinearLayout layoutAttendant;
    private String myMeetingStatus, myMeetingId, reason, pendingDate, status, notesId;
    private UserList userList;
    private Boolean isSent;
    private RejectDialog rejectDialog;
    private String rejectReason, rejectDate1, rejectDate2, rejectDate3;
    private int attendantCount, attendantCountAccept, attendantCountReject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_meeting);
        activity = this;
        isSent = false;
        //Layout
        layoutMain = findViewById(R.id.drawer_main);
        btnReconfig = findViewById(R.id.btn_reconfig_meeting);
        btnProcess = findViewById(R.id.btn_process_meeting);
        btnCancel = findViewById(R.id.btn_cancel_meeting);
        btnAccept = findViewById(R.id.btn_accept_meeting);
        btnReject = findViewById(R.id.btn_reject_meeting);
        btnCreateNotes = findViewById(R.id.btn_create_notes);
        btnSeeNotes = findViewById(R.id.btn_see_notes);
        tvDescription = findViewById(R.id.tv_meeting_description);
        tvCalendar = findViewById(R.id.tv_meeting_calendar);
        tvHour = findViewById(R.id.tv_meeting_time);
        tvLocation = findViewById(R.id.tv_meeting_location);
        tvUserCreator = findViewById(R.id.tv_meeting_creator);
        tvStatus = findViewById(R.id.tv_meeting_status);
        frameStatus = findViewById(R.id.frame_meeting_status);
//        flexboxAttendant = findViewById(R.id.layout_user_list_detail_meeting);
//        flexboxAttendantAccept = findViewById(R.id.layout_user_list_detail_meeting_accept);
//        flexboxAttendantReject = findViewById(R.id.layout_user_list_detail_meeting_reject);
        tvAttendant = findViewById(R.id.tv_attendant);
        layoutAttendant = findViewById(R.id.layout_attendant);

        //Action Bar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getString(R.string.meeting_detail));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedData = MSharedData.getInstance(this);
        Bundle bundle = getIntent().getExtras();

        attendantCount = 0;
        attendantCountAccept = 0;
        attendantCountReject = 0;

        final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
        userChipActivity.getSelectedChipList().clear();

        if (bundle != null) {
            meetingId = bundle.getString("meetingId");
            meetingList = MeetingList.getInstance(activity);

            Meeting meeting = meetingList.get(meetingId);
            userList = meeting.getUserList();
            toolbarTitle.setText(meeting.getMeetingName());
            tvDescription.setText(meeting.getDescription());
            tvCalendar.setText(meeting.getStartDate());
            tvHour.setText(meeting.getStartHour() + " - " + meeting.getEndHour());
            tvLocation.setText(meeting.getLocation());

            tvUserCreator.setText(meeting.getUserCreatorName());
            frameStatus.setBackground(getResources().getDrawable(R.drawable.button_radius_green_high));

            UserChip userChip = null;
            userChipActivity.clearSelectedChipList();
            System.out.println((userList.getUserArrayList()));
            for (int i = 0; i < userList.getUserArrayList().size(); i++) {
                String username = userList.getUserArrayList().get(i).getUsername();
                String userId = userList.getUserArrayList().get(i).getUserId();
                userChip = new UserChip(userId, username);

                LinearLayout attendantTable = new LinearLayout(activity);
                attendantTable.setOrientation(LinearLayout.HORIZONTAL);

                TextView tvAttendantName = new TextView(activity);
                TextView tvAttendantStatus = new TextView(activity);

                tvAttendantName.setText(userList.getUserArrayList().get(i).getFullname());
                tvAttendantName.setGravity(Gravity.CENTER_HORIZONTAL);
                tvAttendantName.setWidth(0);
                tvAttendantName.setBackground(activity.getResources().getDrawable(R.drawable.border_primary));
                tvAttendantName.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
                tvAttendantStatus.setText(userList.getUserArrayList().get(i).getMeetingEmployeeStatusString());
                tvAttendantStatus.setGravity(Gravity.CENTER_HORIZONTAL);
                tvAttendantStatus.setWidth(0);
                tvAttendantStatus.setBackground(activity.getResources().getDrawable(R.drawable.border_primary));
                tvAttendantStatus.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                attendantTable.addView(tvAttendantName);
                attendantTable.addView(tvAttendantStatus);

                layoutAttendant.addView(attendantTable);
//                final ChipView tempChipView = chipView;
//                final int j = i;
//                chipView.setOnDeleteClicked(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        userChipActivity.getSelectedChipList().remove(j);
//                        flexboxAttendant.removeView(tempChipView);
//                    }
//                });
//                if (userList.getUserArrayList().get(i).getMeetingEmployeeStatus().equalsIgnoreCase("ACCEPT")) {
//                    attendantCountReject++;
//                    flexboxAttendantAccept.addView(chipView);
//                } else if (userList.getUserArrayList().get(i).getMeetingEmployeeStatus().equalsIgnoreCase("REJECT")) {
//                    attendantCountAccept++;
//                    flexboxAttendantReject.addView(chipView);
//                } else {
//                    attendantCount++;
//                    flexboxAttendant.addView(chipView);
//                }
//                userChipActivity.addSelectedChipList(userId, userChip);
            }


            System.out.println(userList);
            if (meeting.getMeetingStatus().equalsIgnoreCase("ACTIVE")) {
                frameStatus.setBackground(getResources().getDrawable(R.drawable.button_radius_orange_high));
                tvStatus.setText("Status : Menunggu Persetujuan");
                if (sharedData.get(SharedData.USER_ID).equalsIgnoreCase(meeting.getUserCreator())) {
                    btnReconfig.setVisibility(View.VISIBLE);
                    btnProcess.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.VISIBLE);
                    btnAccept.setVisibility(View.GONE);
                    btnReject.setVisibility(View.GONE);
                }
            } else if (meeting.getMeetingStatus().equalsIgnoreCase("ACCEPT")) {
                frameStatus.setBackground(getResources().getDrawable(R.drawable.button_radius_green_high));
                tvStatus.setText("Status : Disetujui");
                btnReconfig.setVisibility(View.GONE);
                btnProcess.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
                btnAccept.setVisibility(View.GONE);
                btnReject.setVisibility(View.GONE);
                if (meeting.getHaveNotes()) {
                    btnSeeNotes.setVisibility(View.VISIBLE);
                } else {
                    if (sharedData.get(SharedData.USER_ID).equalsIgnoreCase(meeting.getUserCreator())) {
                        btnCreateNotes.setVisibility(View.VISIBLE);
                    }
                }
            } else if (meeting.getMeetingStatus().equalsIgnoreCase("CANCEL")) {
                frameStatus.setBackground(getResources().getDrawable(R.drawable.button_radius_red_high));
                tvStatus.setText("Status : Dibatalkan");
                btnReconfig.setVisibility(View.GONE);
                btnProcess.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
                btnAccept.setVisibility(View.GONE);
                btnReject.setVisibility(View.GONE);
            }

            if (!sharedData.get(SharedData.USER_ID).equalsIgnoreCase(meeting.getUserCreator())) {
                myMeetingStatus = userList.get(sharedData.get(SharedData.USER_ID)).getMeetingEmployeeStatus();
                myMeetingId = userList.get(sharedData.get(SharedData.USER_ID)).getMeetingEmployeeId();

                if (!myMeetingStatus.equalsIgnoreCase("ACTIVE")) {
                    btnAccept.setVisibility(View.GONE);
                    btnReject.setVisibility(View.GONE);
                }
            }
        }


        btnReconfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {
                    isSent = true;
                }
                finish();
                Intent intent = new Intent(activity, NewMeetingActivity.class);
                intent.putExtra("meetingId", meetingId);
                startActivityForResult(intent, NEW_MEETING_REQUEST);
            }
        });

        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {

                    final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                    messageDialog.setMessageNo(getResources().getString(R.string.task_accept_no));
                    messageDialog.setMessageYes(getResources().getString(R.string.task_accept_yes));
                    messageDialog.setInputDisable();
                    messageDialog.setHeader("Meeting ini akan diadakan, setuju?");
                    Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                    yesbutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isSent = true;
                            status = "ACCEPT";
                            doApiStatusMeeting();
                            messageDialog.dismiss();
                        }
                    });
                    Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                    nobutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            messageDialog.dismiss();
                        }
                    });
                    messageDialog.show();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {
                    final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                    messageDialog.setMessageNo(getResources().getString(R.string.task_accept_no));
                    messageDialog.setMessageYes(getResources().getString(R.string.task_accept_yes));
                    messageDialog.setInputDisable();
                    messageDialog.setHeader("Anda yakin akan membatalkan meeting ini?");
                    Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                    yesbutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            status = "CANCEL";
                            doApiStatusMeeting();
                            isSent = true;
                            messageDialog.dismiss();
                        }
                    });
                    Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                    nobutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            messageDialog.dismiss();
                        }
                    });
                    messageDialog.show();
                }
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {

                    final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
                    messageDialog.setMessageNo(getResources().getString(R.string.task_accept_no));
                    messageDialog.setMessageYes(getResources().getString(R.string.task_accept_yes));
                    messageDialog.setInputDisable();
                    messageDialog.setHeader("Apakah anda yakin akan hadir di meeting ini? Anda tidak dapat membatalkan keputusan ini");
                    Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
                    yesbutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isSent = true;
                            status = "ACCEPT";
                            doApiApprovalMeeting();
                            messageDialog.dismiss();
                        }
                    });
                    Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
                    nobutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            messageDialog.dismiss();
                        }
                    });
                    messageDialog.show();
                }

            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (!isSent) {
//                    final TaskStatusDialog messageDialog = new TaskStatusDialog(activity, R.style.dialogNoTitle);
//                    messageDialog.setMessageNo(getResources().getString(R.string.task_accept_no));
//                    messageDialog.setMessageYes(getResources().getString(R.string.task_accept_yes));
//                    messageDialog.getEditTextTaskDate().setHint("Tanggal Pending");
//                    messageDialog.setHeader("Apakah anda yakin tidak menghadiri meeting ini?");
//                    messageDialog.setShowDate(true, getFragmentManager());
//                    Button yesbutton = messageDialog.findViewById(R.id.btn_task_yes);
//                    yesbutton.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            isSent = true;
//                            status = "REJECT";
//                            pendingDate = messageDialog.getPendingDate();
//                            reason = messageDialog.getMessage();
//                            doApiApprovalMeeting();
//                            messageDialog.dismiss();
//                        }
//                    });
//                    Button nobutton = messageDialog.findViewById(R.id.btn_task_no);
//                    nobutton.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            messageDialog.dismiss();
//
//                        }
//                    });
//                    messageDialog.show();
//
//                }

                rejectDialog = new RejectDialog(activity, getFragmentManager());
                rejectDialog.getBtnReject().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        isSent = true;
                        status = "REJECT";
                        rejectReason = rejectDialog.getEditTextReason().getText().toString();
                        rejectDate1 = rejectDialog.getEditTextDate1().getText().toString();
                        rejectDate2 = rejectDialog.getEditTextDate2().getText().toString();
                        rejectDate3 = rejectDialog.getEditTextDate3().getText().toString();
                        doApiApprovalMeeting();
                        rejectDialog.dismiss();
                    }
                });
                rejectDialog.show();
            }
        });

        btnCreateNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, NewNotesActivity.class);
                intent.putExtra("meetingId", meetingId);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            }
        });

        btnSeeNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doApiGetNotes();
            }
        });
    }

    private void doApiApprovalMeeting() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "UpdateStatusApprovalMeeting");
            HashMap postData = new HashMap();
            postData.put("meeting_employee_id", myMeetingId);
            postData.put("meeting_status", status);
            if (status.equalsIgnoreCase("reject")) {
                postData.put("pending_date1", rejectDate1);
                postData.put("pending_date2", rejectDate2);
                postData.put("pending_date3", rejectDate3);
                postData.put("reason", rejectReason);
            }
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(Activity.RESULT_OK);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Mengupdate Status", Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }

    private void doApiStatusMeeting() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "UpdateStatusMeeting");
            HashMap postData = new HashMap();
            postData.put("meeting_id", meetingId);
            postData.put("meeting_status", status);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(Activity.RESULT_OK);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Mengupdate Status", Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }

    private void doApiGetNotes() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "GetNotes");
            HashMap postData = new HashMap();
            postData.put("meeting_id", meetingId);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    System.out.println(response);
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");

                            Notes notes = Notes.getInstance(activity);
                            notes.clearAllArr();
                            notes.setId(data.getString("notes_id"));
                            notes.setMeetingId(data.getString("meeting_id"));
                            notes.setMeetingTitle(data.getString("meeting_name"));
                            notes.setNotesTitle(data.getString("notes_title"));
                            notes.setUserNotes(data.getString("user_creator"));
                            notes.setUserLeader(data.getString("meeting_leader"));
                            notes.setUserPic(data.getString("person_in_charge"));
                            notes.setStartDate(data.getString("start_date"));
                            notes.setStartHour(data.getString("start_hour"));
                            notes.setEndHour(data.getString("end_hour"));
                            notes.setNotesTopic(data.getString("topic"));

                            JSONArray usersArr = data.getJSONArray("users");

                            for (int i = 0; i < usersArr.length(); i++) {
                                JSONObject userArrObj = (JSONObject) usersArr.get(i);

                                String userStatus = userArrObj.getString("notes_employee_status");
                                String userName = userArrObj.getString("employee_name");
                                if (userStatus.equalsIgnoreCase("ATTEND")) {
                                    notes.addUserAttendance(userName);
                                } else {
                                    notes.addUserNotAttendance(userName);
                                }
                            }

                            JSONArray opinionArr = data.getJSONArray("opinion");
                            for (int i = 0; i < opinionArr.length(); i++) {
                                JSONObject opinionArrObj = (JSONObject) opinionArr.get(i);
                                String opinionCreator = opinionArrObj.getString("opinion_creator_name");
                                String opinion = opinionArrObj.getString("opinion");
                                notes.addOpinionCreator(opinionCreator);
                                notes.addOpinion(opinion);
                            }
                            closeDialog(activity);
                            Intent intent = new Intent(activity, DetailNotesActivity.class);
                            startActivity(intent);
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == NEW_MEETING_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Snackbar.make(layoutMain, "Meeting Berhasil Diatur Ulang", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
