package com.itant.iproms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NewOpinionActivity extends MAppCompat {
    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private DrawerLayout layoutMain;
    private ApiIProms apiIProms;
    private Boolean isSent, isPrivate;
    private Spinner spinnerOpinionCreator;
    private String meetingId, notesTitle, userNotes, userLeader, userPic, notesTopic;
    private ArrayList<String> userAttend, opinionCreator, opinion, userNotAttend, opinionUserList;
    private Button btnAddOpinion, btnSave;
    private EditText editTextOpinion;
    private LinearLayout layoutOpinion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_opinion);

        activity = this;
        isPrivate = false;
        isSent = false;

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.opinion));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meetingId = bundle.get("meetingId").toString();
            notesTitle = bundle.get("notesTitle").toString();
            notesTopic = bundle.get("notesTopic").toString();
            userNotes = bundle.get("userNotes").toString();
            userLeader = bundle.get("userLeader").toString();
            userPic = bundle.get("userPic").toString();
            userAttend = (ArrayList<String>) bundle.get("userAttend");
            userNotAttend = (ArrayList<String>) bundle.get("userNotAttend");
        }

        opinionCreator = new ArrayList<String>();
        opinion = new ArrayList<String>();
        opinionUserList = new ArrayList<String>();

        final UserList userArrayList = UserList.getInstance(activity);
        System.out.println(userAttend);
        for (int i = 0; i <= userAttend.size() - 1; i++) {
            opinionUserList.add(userArrayList.get(userAttend.get(i)).getFirstName() + " " + userArrayList.get(userAttend.get(i)).getLastName());
        }

        ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_item, opinionUserList);
        userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        layoutMain = findViewById(R.id.drawer_main);
        layoutOpinion = findViewById(R.id.layout_user_opinion);
        spinnerOpinionCreator = findViewById(R.id.spinner_user_opinion_creator);
        btnAddOpinion = findViewById(R.id.btn_add_opinion);
        btnSave = findViewById(R.id.btn_save_notes);
        editTextOpinion = findViewById(R.id.edit_text_opinion);
        spinnerOpinionCreator.setAdapter(userArrayAdapter);
        View view = this.getCurrentFocus();

        btnAddOpinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;
                editTextOpinion.setError(null);
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                if (editTextOpinion.getText().length() < 1) {
                    err++;
                    editTextOpinion.setError("Pendapat Kosong");
                }

                if (err == 0) {
                    UserList userList = UserList.getInstance(activity);
                    LinearLayout opinionTable = new LinearLayout(activity);
                    opinionTable.setOrientation(LinearLayout.HORIZONTAL);

                    TextView tvOpinionCreator = new TextView(activity);
                    TextView tvOpinion = new TextView(activity);

                    opinionCreator.add(userList.getPosition(spinnerOpinionCreator.getSelectedItemPosition()).getUserId());
                    opinion.add(editTextOpinion.getText().toString());

                    tvOpinionCreator.setText(userList.getPosition(spinnerOpinionCreator.getSelectedItemPosition()).getFirstName());
                    tvOpinionCreator.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvOpinionCreator.setWidth(0);
                    tvOpinionCreator.setBackground(activity.getDrawable(R.drawable.border_primary));
                    tvOpinionCreator.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                    tvOpinion.setText(editTextOpinion.getText().toString());
                    tvOpinion.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvOpinion.setWidth(0);
                    tvOpinion.setBackground(activity.getDrawable(R.drawable.border_primary));
                    tvOpinion.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 3f));

                    opinionTable.addView(tvOpinionCreator);
                    opinionTable.addView(tvOpinion);

                    layoutOpinion.addView(opinionTable);
                }
                editTextOpinion.setText("");
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doApiNewNotes();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }

    private void doApiNewNotes() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "NewNotes");
            HashMap postData = new HashMap();
            postData.put("meeting_id", meetingId);
            postData.put("notes_title", notesTitle);
            postData.put("notes_topic", notesTopic);
            postData.put("user_creator", userNotes);
            postData.put("meeting_leader", userLeader);
            postData.put("person_in_charge", userPic);
            postData.put("attendance_users", userAttend);
            postData.put("non_attendance_users", userNotAttend);
            postData.put("opinion_creator", opinionCreator);
            postData.put("opinion", opinion);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(Activity.RESULT_OK);
                            Intent intent = new Intent(activity, MyMeetingActivity.class);
//                            intent.putExtra("meetingId", meetingId);
                            startActivity(intent);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Membuat Notulensi", Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }
}
