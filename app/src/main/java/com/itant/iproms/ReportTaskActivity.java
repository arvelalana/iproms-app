package com.itant.iproms;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterDaily;
import com.itant.iproms.lib.adapter.AdapterTask;
import com.itant.iproms.lib.adapter.AdapterTaskKpi;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.DailyActivityList;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ReportTaskActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener {
    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private RecyclerView taskRecyclerView;
    private ConstraintLayout layoutMain;
    private GridLayoutManager mLayoutManager;
    private Boolean isOpen = false;
    private Spinner spinnerEmployee;
    private Button showTask;
    private String userId;
    private String dateMode;
    private EditText editTextStartDate, editTextEndDate;
    private String filterStartDate, filterEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_report);
        activity = this;

        //Set Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.task_report).toUpperCase());
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        spinnerEmployee = findViewById(R.id.spinner_task_report);
        showTask = findViewById(R.id.btn_show_task_report);
        taskRecyclerView = findViewById(R.id.task_report_recycler_view);
        mLayoutManager = new GridLayoutManager(activity, 1);
        taskRecyclerView.setLayoutManager(mLayoutManager);
        taskRecyclerView.setItemAnimator(new DefaultItemAnimator());
        editTextStartDate = findViewById(R.id.edit_text_task_start_date);
        editTextEndDate = findViewById(R.id.edit_text_task_end_date);
        doApiSubordinate();


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String stringDate = simpleDateFormat.format(new Date());
        editTextStartDate.setText(stringDate);
        editTextEndDate.setText(stringDate);

        showTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;
                editTextStartDate.setError(null);
                editTextEndDate.setError(null);
                UserList userList = UserList.getInstance(activity);

                if (spinnerEmployee.getSelectedItemPosition() >= 0) {
                    userId = userList.getPosition(spinnerEmployee.getSelectedItemPosition()).getUserId();
                }

                filterStartDate = editTextStartDate.getText().toString();
                filterEndDate = editTextEndDate.getText().toString();

                if (filterStartDate.length() <= 0) {
                    err++;
                    editTextStartDate.setError("Isi tanggal awal tugas");
                }

                if (filterEndDate.length() <= 0) {
                    err++;
                    editTextEndDate.setError("Isi tanggal akhir tugas");
                }

                Filter filter = Filter.getInstance(activity);
                filter.clear();
                if (err == 0) {
                    doApiTaskFilter("0");
                }
            }
        });

        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });

// PIE CHART TEST
//        PieChart pieChart = findViewById(R.id.chart_performance);
//        UserList userList = UserList.getInstance(this);
//        List<PieEntry> entries = new ArrayList<PieEntry>();
//        entries.add(new PieEntry(22, "Jenius"));
//        entries.add(new PieEntry(58, "Pintar"));
//        entries.add(new PieEntry(12, "Tidak Bodoh"));
//        entries.add(new PieEntry(8, "Sangat Pintar"));
//        PieDataSet dataSet = new PieDataSet(entries, "");
//        dataSet.setSliceSpace(2);
//
//        dataSet.setColors(getResources().getColor(R.color.colorBlue), getResources().getColor(R.color.colorButtonGreen), getResources().getColor(R.color.StandartOrange), getResources().getColor(R.color.md_red_500));
//        dataSet.setValueTextColor(getResources().getColor(R.color.colorPrimary));
//        dataSet.setValueTextSize(15);
//
//        PieData pieData = new PieData(dataSet);
//
////        pieData.addDataSet(dataSet2);
//
//        Description description = new Description();
//        description.setText("Data IQ orang di Surabaya");
//        pieChart.setCenterText("Data IQ 2017");
//        pieChart.setCenterTextSize(22);
//        pieChart.setTransparentCircleRadius(0);
//        pieChart.setDescription(description);
//        pieChart.setData(pieData);
//        pieChart.invalidate();


    }

    private void doApiSubordinate() {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "Subordinate");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);

                        UserList userArrayList = UserList.getInstance(activity);
                        userArrayList.clear();
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                user = new User(activity);
                                user.setUserId(userList.getString("user_id"));
                                user.setUsername(userList.getString("username"));
                                user.setFirstName(userList.getString("first_name"));
                                user.setLastName(userList.getString("last_name"));
                                user.setEmail(userList.getString("email"));
                                userArrayList.add(user);
                            }

                            ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, userArrayList.getAllUsername());
                            userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerEmployee.setAdapter(userArrayAdapter);
                            closeDialog(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
            closeDialog(activity);
        }
    }


    private void doApiTaskFilter(final String page) {
        openDialog(activity);
        Filter filter = Filter.getInstance(activity);
        filter.setTaskStatus("DONE");
        filter.setPlanStartDate(filterStartDate);
        filter.setPlanEndDate(filterEndDate);
        try {
            apiIProms = ApiIProms.factory(this, "ReportTask");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("user_id", userId);
            postData.put("sort_by", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Task task = Task.getInstance(activity);
                        TaskList taskListArr = TaskList.getInstance(activity);
                        ArrayList<Task> tempArraylist = new ArrayList<Task>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject taskList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = taskList.getJSONArray("users");
                                task = new Task();

                                if (taskList.getString("to_do_list_id").length() > 0) {
                                    pageCount += 1;
                                }

                                task.setId(taskList.getString("to_do_list_id"));
                                task.setCreated(taskList.getString("creator_name"));
                                task.setCreatedDate(taskList.getString("created"));
                                task.setDescription(taskList.getString("description"));
                                task.setTaskCode(taskList.getString("to_do_list_code"));
                                task.setTaskName(taskList.getString("to_do_list_name"));
                                task.setTaskStatus(taskList.getString("to_do_list_status"));
                                task.setProjectName(taskList.getString("project_name"));
                                task.setProjectDate(taskList.getString("project_created_date"));
                                task.setProjectCreator(taskList.getString("project_created_by"));
                                task.setStartDate(taskList.getString("plan_start_date"));
                                task.setEndDate(taskList.getString("plan_end_date"));
                                task.setActualStartDate(taskList.getString("actual_start_date"));
                                task.setActualEndDate(taskList.getString("actual_end_date"));
                                task.setUserOrder(taskList.getString("user_order_name"));
                                task.setUserApprove(taskList.getString("user_approve"));
                                task.setTaskReason(taskList.getString("reason"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    userList.add(user);
                                }

                                task.setUserList(userList);
                                tempArraylist.add(task);
                            }

                            task = Task.getInstance(activity);
                            taskListArr.setTaskArrayList(tempArraylist);
                            AdapterTask adapterTask = new AdapterTask(activity, taskListArr.getTaskArrayList());
                            taskRecyclerView.setAdapter(adapterTask);
                            closeDialog(activity);

                            if (taskListArr.getTaskArrayList().size() < 1) {

                            }
                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        closeDialog(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            closeDialog(activity);
            e1.printStackTrace();
        }
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;

        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Awal Kinerja Harian";
            dateValue = editTextStartDate.getText().toString();
        } else {
            title = "Tanggal Akhir Kinerja Harian";
            dateValue = editTextEndDate.getText().toString();
        }
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                ReportTaskActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );
        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    ReportTaskActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
        } else {
        }
        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
        } else {
            editTextEndDate.setText(fullDate);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
        Filter filter = Filter.getInstance(activity);
        filter.clear();
    }
}
