package com.itant.iproms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Meeting;
import com.itant.iproms.lib.object.MeetingList;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.MShareDataString;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextInputEditText;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.itant.iproms.NewTaskActivity.NEW_TASK_REQUEST_USER;

public class NewMeetingActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    public static final int NEW_MEETING_REQUEST_USER = 76;
    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private DrawerLayout layoutMain;
    private ApiIProms apiIProms;
    private Boolean isOpen = false;
    private Button buttonSave;
    private FlexboxLayout layoutUserList;
    private ArrayList<String> users;
    private ArrayList<String> userArrayList;
    private String dateMode, meetingName, hourMode, description, startDate, startHour, location, endHour;
    private MTextInputEditText editTextTitle, editTextDescription, editTextDate, editTextStartHour, editTextEndHour, editTextLocation;
    private ImageView addUser;
    private Boolean isSent, isPrivate;
    private String title, dateValue;
    private String mode = "add";
    private String meetingId;
    private MeetingList meetingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_meeting);
        activity = this;
        isPrivate = false;
        isSent = false;


        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.create_meeting));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        layoutMain = findViewById(R.id.drawer_main);
        editTextTitle = findViewById(R.id.edit_text_meeting_title);
        editTextDescription = findViewById(R.id.edit_text_meeting_description);
        editTextDate = findViewById(R.id.edit_text_meeting_date);
        editTextStartHour = findViewById(R.id.edit_text_meeting_start_hour);
        editTextEndHour = findViewById(R.id.edit_text_meeting_end_hour);
        editTextLocation = findViewById(R.id.edit_text_meeting_location);
        layoutUserList = findViewById(R.id.layout_user_list);
        addUser = findViewById(R.id.btn_add_user);
        buttonSave = findViewById(R.id.btn_add_meeting);

        userArrayList = new ArrayList<String>();
        users = new ArrayList<String>();
        isSent = false;
        final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
        userChipActivity.getSelectedChipList().clear();

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            mode = "edit";

            toolbarTitle.setText(getResources().getString(R.string.reschedule_meeting));
            FontStyle.toolbarTitle(activity, toolbarTitle);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            meetingId = bundle.getString("meetingId");
            meetingList = MeetingList.getInstance(activity);
            Meeting meeting = meetingList.get(meetingId);
            UserList userList = meeting.getUserList();
            editTextTitle.setText(meeting.getMeetingName().toString());
            editTextDescription.setText(meeting.getDescription().toString());
            editTextDate.setText(meeting.getStartDate().toString());
            editTextStartHour.setText(meeting.getStartHour().toString());
            editTextEndHour.setText(meeting.getEndHour().toString());
            editTextLocation.setText(meeting.getLocation().toString());


            ChipView chipView;
            UserChip userChip = null;
            userChipActivity.clearSelectedChipList();

            for (int i = 0; i < userList.getUserArrayList().size(); i++) {
                String username = userList.getUserArrayList().get(i).getUsername();
                String userId = userList.getUserArrayList().get(i).getUserId();
                userChip = new UserChip(userId, username);
                chipView = new ChipView(activity);
                chipView.setPadding(15, 10, 15, 10);
                chipView.setLabel(userChip.getLabel());
                chipView.setHasAvatarIcon(true);
                chipView.setLabelColor(activity.getColor(R.color.colorTextWhite));
                chipView.setAvatarIcon(activity.getDrawable(R.drawable.ic_orange_profile));
                chipView.setChipBackgroundColor(activity.getColor(R.color.md_orange_700));
                chipView.setDeleteIconColor(activity.getColor(R.color.colorWhite));
                chipView.setDeletable(true);

                final ChipView tempChipView = chipView;
                final int j = i;
                chipView.setOnDeleteClicked(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userChipActivity.getSelectedChipList().remove(j);
                        layoutUserList.removeView(tempChipView);
                    }
                });
                layoutUserList.addView(chipView);
                userChipActivity.addSelectedChipList(userId, userChip);
            }
        }

        editTextStartHour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    hourMode = "start";
                    timePickerOnClick();
                }
                return false;
            }
        });

        editTextEndHour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    hourMode = "end";
                    timePickerOnClick();
                }
                return false;
            }
        });

        editTextDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(activity);
                doApiUserList();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {
                    isSent = true;
                    int err = 0;
                    UserList userList = UserList.getInstance(activity);
                    meetingName = editTextTitle.getText().toString();
                    description = editTextDescription.getText().toString();
                    startDate = editTextDate.getText().toString();
                    startHour = editTextStartHour.getText().toString();
                    endHour = editTextEndHour.getText().toString();
                    location = editTextLocation.getText().toString();
                    if (meetingName.length() < 1) {
                        err++;
                        editTextTitle.setError(getResources().getString(R.string.task_name_blank));
                    }

                    if (description.length() < 1) {
                        err++;
                        editTextDescription.setError(getResources().getString(R.string.short_description_blank));
                    }


                    if (startDate.length() < 1) {
                        err++;
                        editTextDate.setError(getResources().getString(R.string.start_date_blank));
                    }

                    if (startHour.length() < 1) {
                        err++;
                        editTextStartHour.setError(getResources().getString(R.string.start_hour_blank));
                    }

                    if (startHour.length() < 1) {
                        err++;
                        editTextStartHour.setError(getResources().getString(R.string.start_hour_blank));
                    }

                    if (endHour.length() < 1) {
                        err++;
                        editTextStartHour.setError(getResources().getString(R.string.end_hour_blank));
                    }

                    if (location.length() < 1) {
                        err++;
                        editTextLocation.setError(getResources().getString(R.string.end_date_blank));
                    }

                    UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                    HashMap selectedUserList = userChipActivity.getSelectedChipList();
                    Iterator it = selectedUserList.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        users.add(((UserChip) pair.getValue()).getId().toString());
                    }

                    if (users.size() < 1) {
                        err++;
                        Snackbar.make(layoutMain, getResources().getString(R.string.user_blank), Snackbar.LENGTH_LONG)
                                .show();
                    }

                    if (err == 0) {
                        doApiNewMeeting();
                    } else {
                        isSent = false;
                    }
                }
            }
        });


    }


    private void doApiNewMeeting() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "NewMeeting");
            HashMap postData = new HashMap();
            if(mode.equalsIgnoreCase("edit")){
                postData.put("meeting_id", meetingId);
                postData.put("mode", "edit");
            }else{
                postData.put("mode", "add");
            }
            postData.put("meeting_name", meetingName);
            postData.put("start_date", startDate);
            postData.put("start_hour", startHour);
            postData.put("end_hour", endHour);
            postData.put("description", description);
            postData.put("location", location);
            postData.put("users", users);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(Activity.RESULT_OK);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Membuat Meeting", Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }


    private void doApiUserList() {
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            postData.put("project_id", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();
                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                String username = userList.getString("username");
                                userChipActivity.add(new UserChip(userList.getString("user_id"), username));
                            }

                            Intent intent = new Intent(activity, AddUserActivity.class);
                            startActivityForResult(intent, NEW_MEETING_REQUEST_USER);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    public void timePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        String title = "Jam Mulai";
        if (hourMode.equalsIgnoreCase("start")) {
            title = "jam Mulai";
        } else {
            title = "jam Berakhir";
        }
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                NewMeetingActivity.this,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                true
        );
        tpd.setTitle(title);
        tpd.show(getFragmentManager(), "Timepickerdialog");
        tpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";

        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Mulai";
            dateValue = editTextDate.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                NewMeetingActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    NewMeetingActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }

        if (dateMode.equalsIgnoreCase("start")) {

            Calendar calendarNow = Calendar.getInstance();
            dpd.setMinDate(calendarNow);
        } else {
            Calendar calendarNow = Calendar.getInstance();
            if (dateValue.length() > 0) {
                String dateValueStart = editTextDate.getText().toString();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                Date date = calendarNow.getTime();
                try {
                    date = (Date) df.parse(dateValueStart);
                    calendarNow.setTime(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            dpd.setMinDate(calendarNow);
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
            editTextDate.setText(fullDate);
        } else {
            editTextDate.setText(fullDate);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String fullTime;
        String hourString = String.valueOf(hourOfDay);
        String minuteString = String.valueOf(minute);
        if (hourString.length() == 1) {
            hourString = "0" + hourString;
        }
        if (minuteString.length() == 1) {
            minuteString = "0" + minuteString;
        }
        fullTime = hourString + ":" + minuteString;
        if (hourMode.equalsIgnoreCase("start")) {
            editTextStartHour.setText(fullTime);
        } else {
            editTextEndHour.setText(fullTime);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("On Result");
        if (requestCode == NEW_MEETING_REQUEST_USER) {
            if (resultCode == Activity.RESULT_OK) {
                closeDialog(activity);

                final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                layoutUserList.removeAllViews();
                ChipView chipView;
                UserChip userChip;
                Iterator it = userChipActivity.getSelectedChipList().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();

                    userChip = (UserChip) pair.getValue();

                    chipView = new ChipView(activity);
                    chipView.setPadding(15, 10, 15, 10);
                    chipView.setLabel(userChip.getLabel());
                    chipView.setHasAvatarIcon(true);
                    chipView.setLabelColor(activity.getColor(R.color.colorTextWhite));
                    chipView.setAvatarIcon(activity.getDrawable(R.drawable.ic_orange_profile));
                    chipView.setChipBackgroundColor(activity.getColor(R.color.md_orange_700));
                    chipView.setDeleteIconColor(activity.getColor(R.color.colorWhite));
                    chipView.setDeletable(true);

                    final ChipView tempChipView = chipView;
                    final String userId = pair.getKey().toString();
                    chipView.setOnDeleteClicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userChipActivity.removeSelectedChipList(userId);
                            layoutUserList.removeView(tempChipView);
                        }
                    });
                    layoutUserList.addView(chipView);
                }


                ImageView imgAddUser = new ImageView(this);
                imgAddUser.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_circle));
                imgAddUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog(activity);
                        doApiUserList();
                    }
                });
                layoutUserList.addView(imgAddUser);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                closeDialog(activity);
            }
        }
    }


}
