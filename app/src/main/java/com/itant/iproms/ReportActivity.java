package com.itant.iproms;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterReport;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.mapp.mComponent.MTextView;

public class ReportActivity extends MAppCompat {

    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private RelativeLayout layoutMain;
    private GridView gridvViewReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        activity = this;
        layoutMain = findViewById(R.id.layout_my_daily_activity);

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("LAPORAN");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        AdapterReport adapterReport = new AdapterReport(activity);
        gridvViewReport = findViewById(R.id.gridview_report);
        gridvViewReport.setAdapter(adapterReport);
    }
}
