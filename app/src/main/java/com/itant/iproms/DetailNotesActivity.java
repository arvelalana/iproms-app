package com.itant.iproms;

import android.*;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.DownloadTask;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Notes;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class DetailNotesActivity extends MAppCompat {

    private Activity activity;
    private Toolbar toolbar;
    private DrawerLayout layoutMain;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private MSharedData sharedData;
    private Boolean isSent;
    private TextView notesTitle, userNotes, userLeader, userPic, notesDate, notesTime, userAttendance, userNotAttendance;
    private FlexboxLayout layoutAttendance, layoutNotAttendance;
    private LinearLayout layoutOpinion;
    private String meetingId;
    private Button btnDownloadPdf, btnSendPdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_notes);
        activity = this;
        isSent = false;
        //Layout
        layoutMain = findViewById(R.id.drawer_main);


        //Action Bar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getString(R.string.notes_detail));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        notesTitle = findViewById(R.id.tv_notes_title);
        userNotes = findViewById(R.id.tv_user_notes);
        userLeader = findViewById(R.id.tv_user_leader);
        userPic = findViewById(R.id.tv_user_pic);
        notesDate = findViewById(R.id.tv_notes_date);
        notesTime = findViewById(R.id.tv_notes_time);
        btnDownloadPdf = findViewById(R.id.btn_download_pdf);
        btnSendPdf = findViewById(R.id.btn_send_pdf);

        layoutOpinion = findViewById(R.id.layout_opinion);
        layoutAttendance = findViewById(R.id.layout_user_attendance);
        layoutNotAttendance = findViewById(R.id.layout_user_not_attendance);
        sharedData = MSharedData.getInstance(this);

        final Notes notes = Notes.getInstance(activity);
        meetingId = notes.getMeetingId();
        notesTitle.setText(notes.getNotesTitle());
        userNotes.setText(notes.getUserNotes());
        userLeader.setText(notes.getUserLeader());
        userPic.setText(notes.getUserPic());
        notesDate.setText(notes.getStartDate());
        notesTime.setText(notes.getStartHour() + " - " + notes.getEndHour());

        btnDownloadPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int MyVersion = Build.VERSION.SDK_INT;
                if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    if (!checkIfAlreadyhavePermission()) {
                        requestForSpecificPermission();
                    } else {
                        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                        File folder = new File(extStorageDirectory, "Download");
                        folder.mkdir();
                        File file = new File(folder, "Notulensi-" + notes.getNotesTitle() + ".pdf");
                        try {
                            file.createNewFile();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        MSharedData shareData = MSharedData.getInstance(activity);
                        String sessionId = shareData.get(SharedData.SESSION_ID);
                        String url = "http://iproms.kip-capital.com/api/data/getNotesPdf?";
                        url += "session_id=" + sessionId;
                        url += "&meeting_id=" + meetingId;
                        System.out.println(url);
//                            Downloader.DownloadFile(activity, url, file);
                        new DownloadTask(file, activity).execute(url);
                    }
                } else {
                    String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                    File folder = new File(extStorageDirectory, "Download");
                    folder.mkdir();
                    File file = new File(folder, "Notulensi-" + notes.getNotesTitle() + ".pdf");
                    try {
                        file.createNewFile();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    MSharedData shareData = MSharedData.getInstance(activity);
                    String sessionId = shareData.get(SharedData.SESSION_ID);
                    String url = "http://iproms.kip-capital.com/api/data/getDailyPdf?";
                    url += "session_id=" + sessionId;
                    url += "&meeting_id=" + meetingId;
                    new DownloadTask(file, activity).execute(url);
                }
            }
        });

        btnSendPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doApiSendNotes();
            }
        });

        ChipView chipView;
        UserChip userChip = null;
        for (int i = 0; i < notes.getUserAttendance().size(); i++) {
            String username = notes.getUserAttendance().get(i);
            userChip = new UserChip(username, username);
            chipView = new ChipView(activity);
//            chipView.setPadding(15, 10, 15, 10);
            chipView.setLabel(userChip.getLabel());
            chipView.setHasAvatarIcon(true);
            chipView.setDeletable(false);
            layoutAttendance.addView(chipView);
        }


        for (int i = 0; i < notes.getUserNotAttendance().size(); i++) {
            String username = notes.getUserNotAttendance().get(i);
            userChip = new UserChip(username, username);
            chipView = new ChipView(activity);
//            chipView.setPadding(15, 10, 15, 10);
            chipView.setLabel(userChip.getLabel());
            chipView.setHasAvatarIcon(true);
            chipView.setDeletable(false);
            layoutNotAttendance.addView(chipView);
        }

        for (int i = 0; i < notes.getOpinionCreator().size(); i++) {
            String opinionCreator = notes.getOpinionCreator().get(i);
            String opinion = notes.getOpinion().get(i);
            LinearLayout opinionTable = new LinearLayout(activity);
            opinionTable.setOrientation(LinearLayout.HORIZONTAL);

            TextView tvOpinionCreator = new TextView(activity);
            TextView tvOpinion = new TextView(activity);

            tvOpinionCreator.setText(opinionCreator);
            tvOpinionCreator.setGravity(Gravity.CENTER_HORIZONTAL);
            tvOpinionCreator.setWidth(0);
            tvOpinionCreator.setBackground(activity.getDrawable(R.drawable.border_primary));
            tvOpinionCreator.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

            tvOpinion.setText(opinion);
            tvOpinion.setGravity(Gravity.CENTER_HORIZONTAL);
            tvOpinion.setWidth(0);
            tvOpinion.setBackground(activity.getDrawable(R.drawable.border_primary));
            tvOpinion.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 3f));

            opinionTable.addView(tvOpinionCreator);
            opinionTable.addView(tvOpinion);

            layoutOpinion.addView(opinionTable);
        }


    }

    private void doApiSendNotes() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "SendNotes");
            HashMap postData = new HashMap();
            postData.put("meeting_id", meetingId);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {

                            Snackbar.make(layoutMain, "Sukses Mengirim PDF", Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
}
