package com.itant.iproms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.MShareDataString;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextInputEditText;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;
import com.pchmn.materialchips.ChipsInput;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class NewTaskActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener {


    public static final int NEW_TASK_ADDED = 12;
    public static final int NEW_TASK_REQUEST_USER = 2;
    private Activity activity;
    private Toolbar toolbar;
    private ApiIProms apiIProms;
    private MTextView toolbarTitle;
    private ArrayList<String> userArrayList;
    private ConstraintLayout layoutMain;
    private ChipsInput chipsInput;
    private Spinner spinnerProject;
    private Project project;
    private MTextInputEditText editTextTaskName, editTextShortDescription, editTextStartDate, editTextEndDate;
    private Boolean isOpen = false;
    private String dateMode;
    private String projectId, taskName, description, startDate, endDate, userOrder, userApprove;
    private Button buttonSave;
    private ImageView addUser;
    private FlexboxLayout layoutUserList;
    private ArrayList<String> users;
    private String projectIdBundle;
    private Spinner spinnerUserApprove, spinnerUserOrder;
    private RadioButton radioFrom, radioTo;
    private LinearLayout layoutUserTo;
    private CheckBox checkboxPrivateTask;
    private RadioGroup radioGroupTask;
    private Boolean myTask, isSent, isPrivate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        activity = this;
        isPrivate = false;
        isSent = false;

        //Layout
        layoutMain = findViewById(R.id.layout_new_task);
        chipsInput = findViewById(R.id.chips_input);
        spinnerProject = findViewById(R.id.spinner_project);
        editTextTaskName = findViewById(R.id.edit_text_task_name);
        editTextShortDescription = findViewById(R.id.edit_text_task_short_description);
        editTextStartDate = findViewById(R.id.edit_text_task_start_date);
        editTextEndDate = findViewById(R.id.edit_text_task_end_date);
        spinnerUserOrder = findViewById(R.id.spinner_user_order_my_task);
        spinnerUserApprove = findViewById(R.id.spinner_user_approve_my_task);
        buttonSave = findViewById(R.id.button_save_task);
        layoutUserList = findViewById(R.id.layout_user_list);
        addUser = findViewById(R.id.btn_add_user);
        radioFrom = findViewById(R.id.radio_task_from);
        radioTo = findViewById(R.id.radio_task_to);
        layoutUserTo = findViewById(R.id.layout_user_to);
        radioGroupTask = findViewById(R.id.radio_group_task);
        checkboxPrivateTask = findViewById(R.id.checkbox_private_task);
        layoutUserTo.setVisibility(View.GONE);

        //Var
        projectIdBundle = "";
        myTask = false;
        userArrayList = new ArrayList<String>();
        users = new ArrayList<String>();

        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
        userChipActivity.getSelectedChipList().clear();

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.add_new_task).toUpperCase());
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinnerProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                doApiUserListSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        checkboxPrivateTask.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    radioGroupTask.setVisibility(View.GONE);
                    layoutUserTo.setVisibility(View.GONE);
                    layoutUserList.setVisibility(View.GONE);
                    isPrivate = true;
                } else {
                    isPrivate = false;
                    radioGroupTask.setVisibility(View.VISIBLE);
                    if (myTask) {
                        layoutUserTo.setVisibility(View.VISIBLE);
                    } else {
                        layoutUserList.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });

        radioFrom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    myTask = false;
                    layoutUserList.setVisibility(View.VISIBLE);
                    layoutUserTo.setVisibility(View.GONE);
                }
            }
        });

        radioTo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    myTask = true;
                    layoutUserList.setVisibility(View.GONE);
                    layoutUserTo.setVisibility(View.VISIBLE);
                }
            }
        });
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {
                    if (isPrivate) {
                        myTask = false;
                        MSharedData sharedData = MSharedData.getInstance(activity);
                        users.add(sharedData.get(SharedData.USER_ID));
                    }
                    isSent = true;
                    int err = 0;
                    ProjectList projectList = ProjectList.getInstance(activity);
                    if (spinnerProject.getSelectedItemPosition() >= 0) {
                        projectId = projectList.getPosition(spinnerProject.getSelectedItemPosition()).getId();
                    }
                    taskName = editTextTaskName.getText().toString();
                    description = editTextShortDescription.getText().toString();
                    startDate = editTextStartDate.getText().toString();
                    endDate = editTextEndDate.getText().toString();

                    if (myTask) {
                        UserList userList = UserList.getInstance(activity);
                        userOrder = userList.getPosition(spinnerUserOrder.getSelectedItemPosition()).getUserId();
                        userApprove = userList.getPosition(spinnerUserApprove.getSelectedItemPosition()).getUserId();
                        if (userOrder.length() < 1) {
                            err++;
                            Snackbar.make(layoutMain, getResources().getString(R.string.user_blank), Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        if (userApprove.length() < 1) {
                            err++;
                            Snackbar.make(layoutMain, getResources().getString(R.string.user_blank), Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } else {
                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
//                    List<UserChip> selectedUserList = userChipActivity.getSelectedChipList();
                        HashMap selectedUserList = userChipActivity.getSelectedChipList();
                        Iterator it = selectedUserList.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry) it.next();
                            users.add(((UserChip) pair.getValue()).getId().toString());
                        }
                        if (users.size() < 1) {
                            err++;
                            Snackbar.make(layoutMain, getResources().getString(R.string.user_blank), Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }


                    if (projectList.getProjectArrayList().size() <= 0) {
                        err++;
                        Snackbar.make(layoutMain, getResources().getString(R.string.project_blank), Snackbar.LENGTH_LONG)
                                .show();
                    }
                    if (taskName.length() < 1) {
                        err++;
                        editTextTaskName.setError(getResources().getString(R.string.task_name_blank));
                    }

                    if (description.length() < 1) {
                        err++;
                        editTextShortDescription.setError(getResources().getString(R.string.short_description_blank));
                    }

                    if (startDate.length() < 1) {
                        err++;
                        editTextStartDate.setError(getResources().getString(R.string.start_date_blank));
                    }

                    if (endDate.length() < 1) {
                        err++;
                        editTextEndDate.setError(getResources().getString(R.string.end_date_blank));
                    }

                    if (err == 0) {
                        openDialog(activity);
                        if (myTask) {
                            doApiNewMyTask();
                        } else {
                            doApiNewTask();
                        }
                    } else {
                        isSent = false;
                    }
                }
            }
        });

        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(activity);
//                if (UserChipActivity.getInstance(activity).getTotalUserChip() > 0) {
//                    Intent intent = new Intent(activity, AddUserActivity.class);
//                    startActivityForResult(intent, NEW_TASK_REQUEST_USER);
//                } else {

                if (spinnerProject.getSelectedItemPosition() >= 0) {
                    doApiUserList();
                } else {
                    closeDialog(activity);
                    Snackbar.make(layoutMain, "Tidak ada project", Snackbar.LENGTH_LONG)
                            .show();
                }
//                }
            }
        });

        openDialog(activity);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            projectIdBundle = bundle.getString("projectId");
            doApiProject();
        } else {
            doApiProject();
        }
    }

    private void doApiNewTask() {
        try {
            apiIProms = ApiIProms.factory(this, "NewTask");
            HashMap postData = new HashMap();
            postData.put("project_id", projectId);
            postData.put("start_date", startDate);
            postData.put("end_date", endDate);
            postData.put("task_name", taskName);
            postData.put("description", description);
            postData.put("users", users);
            System.out.println(postData);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            closeDialog(activity);
                            Intent intent = new Intent(activity, MyTaskActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        closeDialog(activity);
                        isSent = false;
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            closeDialog(activity);
            e1.printStackTrace();
        }

    }

    private void doApiNewMyTask() {
        try {
            apiIProms = ApiIProms.factory(this, "NewMyTask");
            HashMap postData = new HashMap();
            postData.put("project_id", projectId);
            postData.put("start_date", startDate);
            postData.put("end_date", endDate);
            postData.put("task_name", taskName);
            postData.put("description", description);
            postData.put("user_order", userOrder);
            postData.put("user_approve", userApprove);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            closeDialog(activity);
                            Intent intent = new Intent(activity, MyTaskActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        closeDialog(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            closeDialog(activity);
            e1.printStackTrace();
        }
    }

    private void doApiUserList() {
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            ProjectList projectList = ProjectList.getInstance(activity);
            projectId = projectList.getPosition(spinnerProject.getSelectedItemPosition()).getId();
            postData.put("project_id", projectId);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                String username = userList.getString("username");
//                                userChipActivity.add(new UserChip(userList.getString("user_id"), username));
                                MSharedData sharedData = MSharedData.getInstance(activity);
                                if (!sharedData.get(MShareDataString.USER_ID).equalsIgnoreCase(userList.getString("user_id"))) {
                                    userChipActivity.add(new UserChip(userList.getString("user_id"), username));
                                }
                            }
                            Intent intent = new Intent(activity, AddUserActivity.class);
                            startActivityForResult(intent, NEW_TASK_REQUEST_USER);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }


    private void doApiProject() {
        try {
            apiIProms = ApiIProms.factory(this, "ProjectList");
            HashMap postData = new HashMap();
            postData.put("show", "all");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        ProjectList projectArrayList = ProjectList.getInstance(activity);
                        projectArrayList.clear();

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            if (projectIdBundle.length() > 0) {
                                for (int i = 0; i < resultsArr.length(); i++) {
                                    JSONObject projectList = (JSONObject) resultsArr.get(i);
                                    project = new Project(activity);
                                    project.setId(projectList.getString("project_id"));
                                    project.setCreated(projectList.getString("created"));
                                    project.setDescription(projectList.getString("description"));
                                    project.setProjectCode(projectList.getString("project_code"));
                                    project.setProjectName(projectList.getString("project_name"));
                                    project.setProjectStatus(projectList.getString("status_project"));
                                    if (projectIdBundle.equalsIgnoreCase(projectList.getString("project_id"))) {
                                        projectArrayList.add(project);
                                    }
                                }
                            } else {
                                for (int i = 0; i < resultsArr.length(); i++) {
                                    JSONObject projectList = (JSONObject) resultsArr.get(i);
                                    project = new Project(activity);
                                    project.setId(projectList.getString("project_id"));
                                    project.setCreated(projectList.getString("created"));
                                    project.setDescription(projectList.getString("description"));
                                    project.setProjectCode(projectList.getString("project_code"));
                                    project.setProjectName(projectList.getString("project_name"));
                                    project.setProjectStatus(projectList.getString("status_project"));
                                    projectArrayList.add(project);
                                }
                            }


                            ArrayAdapter<String> projectArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, projectArrayList.getAllProjectName());
                            projectArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerProject.setAdapter(projectArrayAdapter);

                            doApiUserListSpinner();
                            closeDialog(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    private void doApiUserListSpinner() {
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            ProjectList projectList = ProjectList.getInstance(activity);
            if (projectList.getProjectArrayList().size() > 0) {
                projectId = projectList.getPosition(spinnerProject.getSelectedItemPosition()).getId();
            }
            postData.put("project_id", projectId);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);

                        UserList userArrayList = UserList.getInstance(activity);
                        userArrayList.clear();
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                user = new User(activity);
                                user.setUserId(userList.getString("user_id"));
                                user.setUsername(userList.getString("username"));
                                user.setFirstName(userList.getString("first_name"));
                                user.setLastName(userList.getString("last_name"));
                                user.setEmail(userList.getString("email"));
                                userArrayList.add(user);
                            }

                            ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, userArrayList.getAllUsername());
                            userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerUserOrder.setAdapter(userArrayAdapter);
                            spinnerUserApprove.setAdapter(userArrayAdapter);
                            closeDialog(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;
        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Mulai";
            dateValue = editTextStartDate.getText().toString();
        } else {
            title = "Tanggal Berakhir";
            dateValue = editTextEndDate.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                NewTaskActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    NewTaskActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");

        Calendar calendarNow = Calendar.getInstance();
        dpd.setMinDate(calendarNow);
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });

    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
        } else {
            editTextEndDate.setText(fullDate);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("On Result");
        if (requestCode == NEW_TASK_REQUEST_USER) {
            if (resultCode == Activity.RESULT_OK) {
                closeDialog(activity);

                final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                layoutUserList.removeAllViews();
                ChipView chipView;
                UserChip userChip;
                Iterator it = userChipActivity.getSelectedChipList().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();

                    userChip = (UserChip) pair.getValue();

                    chipView = new ChipView(activity);
                    chipView.setPadding(15, 10, 15, 10);
                    chipView.setLabel(userChip.getLabel());
                    chipView.setHasAvatarIcon(true);
                    chipView.setDeletable(true);

                    final ChipView tempChipView = chipView;
                    final String userId = pair.getKey().toString();
                    chipView.setOnDeleteClicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userChipActivity.removeSelectedChipList(userId);
                            layoutUserList.removeView(tempChipView);
                        }
                    });
                    layoutUserList.addView(chipView);
                }


                ImageView imgAddUser = new ImageView(this);
                imgAddUser.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_circle));
                imgAddUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog(activity);
                        if (UserChipActivity.getInstance(activity).getTotalUserChip() > 0) {
                            Intent intent = new Intent(activity, AddUserActivity.class);
                            startActivityForResult(intent, NEW_TASK_REQUEST_USER);
                        } else {
                            if (spinnerProject.getSelectedItemPosition() >= 0) {
                                doApiUserList();
                            } else {
                                Snackbar.make(layoutMain, "Tidak ada project", Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        }
                    }
                });
                layoutUserList.addView(imgAddUser);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // do nothing
                closeDialog(activity);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }
}
