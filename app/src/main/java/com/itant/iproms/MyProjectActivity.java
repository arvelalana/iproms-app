package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.adapter.AdapterProject;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Nav;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.helper.MAndroidUtil;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.itant.iproms.mapp.mObject.UserPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyProjectActivity extends MAppCompat {

    private Activity activity;
    private Toolbar toolbar;
    private ConstraintLayout layoutMain;
    private MTextView toolbarTitle;
    private FloatingActionButton fabAddProject;
    private ApiIProms apiIProms;
    private AdapterProject adapterProject;
    private RecyclerView projectRecyclerView;
    private GridLayoutManager mLayoutManager;
    private ImageView imageMoreProject,imageNoProject;
    private String show;
    private SwipeRefreshLayout swipeProject;
    public static final int NEW_PROJECT_REQUEST = 3;
    public static final int RESULT_EDIT_PROJECT_SUCCESS = 300;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_project);
        activity = this;

        //Layout
        layoutMain = findViewById(R.id.layout_my_project);
        imageMoreProject = findViewById(R.id.img_more_project);
        imageNoProject= findViewById(R.id.image_view_no_project);
        //Action Bar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("PROYEK SAYA");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fabAddProject = findViewById(R.id.fab_add_project);
        fabAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                userChipActivity.getSelectedChipList().clear();
                Intent intent = new Intent(activity, NewProjectActivity.class);
                startActivityForResult(intent, NEW_PROJECT_REQUEST);
            }
        });
        UserPermission userPermission = UserPermission.getInstance();
        if (!userPermission.isExists(Nav.NEW_PROJECT)) {
            fabAddProject.setVisibility(View.GONE);
        }
        imageMoreProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(activity, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.project_histori, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.history_project:
                                show = "past";
                                doApiProject("0");
                                break;
                            case R.id.my_project:
                                show = "all";
                                doApiProject("0");
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

        projectRecyclerView = findViewById(R.id.project_recycler_view);
        if(MAndroidUtil.isTablet(activity)){
            mLayoutManager = new GridLayoutManager(activity, 4);
        }else{
            mLayoutManager = new GridLayoutManager(activity, 2);
        }
        projectRecyclerView.setLayoutManager(mLayoutManager);
        projectRecyclerView.setItemAnimator(new DefaultItemAnimator());

        swipeProject = findViewById(R.id.swipe_project);

        swipeProject.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                show = "all";
                doApiProject("0");
            }
        });


        show = "all";
        doApiProject("0");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        doApiProject("0");
    }

    public void doApiProject(final String page) {
        imageMoreProject.setVisibility(View.GONE);
        imageNoProject.setVisibility(View.GONE);
        if (!swipeProject.isRefreshing()) {
            openDialogWhite(activity);
        }
        try {
            apiIProms = ApiIProms.factory(this, "ProjectList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("show", show);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Project project = Project.getInstance(activity);
                        ProjectList projectListArr = ProjectList.getInstance(activity);
                        ArrayList<Project> tempArraylist = new ArrayList<Project>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {

                                UserList userList = new UserList(activity);
                                JSONObject projectList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = projectList.getJSONArray("users");

                                project = new Project();
                                if (projectList.getString("project_id").length() > 0) {
                                    pageCount += 1;
                                }
                                project.setId(projectList.getString("project_id"));
                                project.setCreated(projectList.getString("created"));
                                project.setCreatedBy(projectList.getString("createdby"));
                                project.setDescription(projectList.getString("description"));
                                project.setProjectCode(projectList.getString("project_code"));
                                project.setProjectName(projectList.getString("project_name"));
                                project.setProjectStatus(projectList.getString("status_project"));
                                project.setStartDate(projectList.getString("plan_start_date"));
                                project.setEndDate(projectList.getString("plan_end_date"));
                                project.setActualStartDate(projectList.getString("actual_start_date"));
                                project.setActualEndDate(projectList.getString("actual_end_date"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    user.setFirstName(userArrList.getString("first_name"));
                                    user.setLastName(userArrList.getString("last_name"));
                                    user.setEmail(userArrList.getString("email"));
                                    userList.add(user);
                                }

                                project.setUserList(userList);
                                tempArraylist.add(project);
                            }

                            project = Project.getInstance(activity);

                            projectListArr.setProjectArrayList(tempArraylist);
                            adapterProject = new AdapterProject(activity, projectListArr.getProjectArrayList());
                            projectRecyclerView.setAdapter(adapterProject);
                            if (swipeProject.isRefreshing()) {
                                swipeProject.setRefreshing(false);
                            }
                            if (projectListArr.getProjectArrayList().size() < 1) {
                                imageNoProject.setVisibility(View.VISIBLE);
                            }
                            closeDialogWhite(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        if (requestCode == NEW_PROJECT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Snackbar.make(layoutMain, "Proyek Berhasil Ditambahkan", Snackbar.LENGTH_SHORT).show();
            }
            if (resultCode == RESULT_EDIT_PROJECT_SUCCESS) {
                Snackbar.make(layoutMain, "Proyek Berhasil Diubah", Snackbar.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }
}
