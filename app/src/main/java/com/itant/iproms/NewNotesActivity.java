package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Meeting;
import com.itant.iproms.lib.object.MeetingList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.MShareDataString;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.itant.iproms.NewTaskActivity.NEW_TASK_REQUEST_USER;

public class NewNotesActivity extends MAppCompat {
    public static final int NEW_MEETING_REQUEST_USER = 76;
    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private DrawerLayout layoutMain;
    private ApiIProms apiIProms;
    private Boolean isOpen = false;
    private Button buttonSave;
    private FlexboxLayout layoutUserListAttend;
    private ArrayList<String> users;
    private ArrayList<String> notAttendanceUsers;
    private ImageView addUserAttend;
    private Boolean isSent, isPrivate;
    private String notesTitle, userNotes, userLeader, userPic, notesTopic, userAttend;
    private String meetingId;
    private EditText editTextTitle, editTextTopic;
    private Spinner spinnerUserNotes, spinnerUserPic, spinnerUserLeader;
    private UserList userMeetingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notes);
        activity = this;
        isPrivate = false;
        isSent = false;

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.create_notes));
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meetingId = bundle.getString("meetingId");
            MeetingList meetingList = MeetingList.getInstance(activity);
            Meeting meeting = meetingList.get(meetingId);
            userMeetingList = meeting.getUserList();
        }

        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
        userChipActivity.getSelectedChipList().clear();
        users = new ArrayList<String>();
        notAttendanceUsers = new ArrayList<String>();
        layoutMain = findViewById(R.id.drawer_main);
        editTextTitle = findViewById(R.id.edit_text_notes_title);
        editTextTopic = findViewById(R.id.edit_text_notes_topic);
        layoutUserListAttend = findViewById(R.id.layout_user_list_attend);
        spinnerUserNotes = findViewById(R.id.spinner_user_notes);
        spinnerUserLeader = findViewById(R.id.spinner_user_leader);
        spinnerUserPic = findViewById(R.id.spinner_user_pic);
        addUserAttend = findViewById(R.id.btn_add_user_attend);
        buttonSave = findViewById(R.id.btn_next_notes);
        addUserAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(activity);
                doApiUserList();
            }
        });
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;
                UserList userList = UserList.getInstance(activity);
                editTextTitle.setError(null);
                editTextTopic.setError(null);
                notesTitle = editTextTitle.getText().toString();
                notesTopic = editTextTopic.getText().toString();
                userNotes = userList.getPosition(spinnerUserNotes.getSelectedItemPosition()).getUserId();
                userLeader = userList.getPosition(spinnerUserLeader.getSelectedItemPosition()).getUserId();
                userPic = userList.getPosition(spinnerUserPic.getSelectedItemPosition()).getUserId();
                UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                HashMap selectedUserList = userChipActivity.getSelectedChipList();
                Iterator it = selectedUserList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    users.add(((UserChip) pair.getValue()).getId().toString());
                }
                if (users.size() < 1) {
                    err++;
                    Snackbar.make(layoutMain, getResources().getString(R.string.attendance_blank), Snackbar.LENGTH_LONG)
                            .show();
                }
                if (notesTitle.length() < 1) {
                    err++;
                    editTextTitle.setError(getResources().getString(R.string.notes_title_blank));
                }
                if (notesTopic.length() < 1) {
                    err++;
                    editTextTopic.setError(getResources().getString(R.string.notes_topic_blank));
                }

                if (err == 0) {
                    Intent intent = new Intent(activity, NewOpinionActivity.class);
                    intent.putExtra("meetingId", meetingId);
                    intent.putExtra("notesTitle", notesTitle);
                    intent.putExtra("notesTopic", notesTopic);
                    intent.putExtra("userNotes", userNotes);
                    intent.putExtra("userLeader", userLeader);
                    intent.putExtra("userPic", userPic);
                    intent.putExtra("userAttend", users);
                    intent.putExtra("userNotAttend", notAttendanceUsers);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
                }
            }
        });
        doApiUserListSpinner();
    }


    private void doApiUserListSpinner() {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            postData.put("project_id", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);
                        UserList userArrayList = UserList.getInstance(activity);
                        userArrayList.clear();
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                user = new User(activity);
                                user.setUserId(userList.getString("user_id"));
                                user.setUsername(userList.getString("username"));
                                user.setFirstName(userList.getString("first_name"));
                                user.setLastName(userList.getString("last_name"));
                                user.setEmail(userList.getString("email"));
                                MSharedData sharedData = MSharedData.getInstance(activity);
                                userArrayList.add(user);
                            }

                            ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, userArrayList.getAllUsername());
                            userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerUserNotes.setAdapter(userArrayAdapter);
                            spinnerUserLeader.setAdapter(userArrayAdapter);
                            spinnerUserPic.setAdapter(userArrayAdapter);
                            closeDialog(activity);
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
            closeDialog(activity);
        }
    }

    private void doApiUserList() {
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            postData.put("project_id", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                String username = userList.getString("username");
//                                userChipActivity.add(new UserChip(userList.getString("user_id"), username));
                                MSharedData sharedData = MSharedData.getInstance(activity);
                                for (int j = 0; j < userMeetingList.getUserArrayList().size(); j++) {
                                    if (userMeetingList.getUserArrayList().get(j).getUserId().equalsIgnoreCase(userList.getString("user_id"))) {
                                        userChipActivity.add(new UserChip(userList.getString("user_id"), username));
                                        notAttendanceUsers.add(userList.getString("user_id"));
                                    }
                                }
                            }
                            System.out.println("waw" + notAttendanceUsers);
                            Intent intent = new Intent(activity, AddUserActivity.class);
                            startActivityForResult(intent, NEW_TASK_REQUEST_USER);
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("On Result");
        if (requestCode == NEW_TASK_REQUEST_USER) {
            if (resultCode == Activity.RESULT_OK) {
                closeDialog(activity);

                final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                layoutUserListAttend.removeAllViews();
                ChipView chipView;
                UserChip userChip;
                Iterator it = userChipActivity.getSelectedChipList().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();

                    userChip = (UserChip) pair.getValue();

                    chipView = new ChipView(activity);
                    chipView.setPadding(15, 10, 15, 10);
                    chipView.setLabel(userChip.getLabel());
                    chipView.setHasAvatarIcon(true);
                    chipView.setDeletable(true);

                    final ChipView tempChipView = chipView;
                    final String userId = pair.getKey().toString();
                    notAttendanceUsers.remove(userId);
                    chipView.setOnDeleteClicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userChipActivity.removeSelectedChipList(userId);
                            layoutUserListAttend.removeView(tempChipView);

                        }
                    });
                    layoutUserListAttend.addView(chipView);

                }


                ImageView imgAddUser = new ImageView(this);
                imgAddUser.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_circle));
                imgAddUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog(activity);

                        if (UserChipActivity.getInstance(activity).getTotalUserChip() > 0) {
                            Intent intent = new Intent(activity, AddUserActivity.class);
                            startActivityForResult(intent, NEW_TASK_REQUEST_USER);
                        } else {
                            doApiUserList();
                        }
                    }
                });
                layoutUserListAttend.addView(imgAddUser);


            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // do nothing
                closeDialog(activity);
            }
        }
    }
}
