package com.itant.iproms;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.adapter.AdapterHolidayDashboard;
import com.itant.iproms.lib.adapter.AdapterProject;
import com.itant.iproms.lib.adapter.AdapterProjectDashboard;
import com.itant.iproms.lib.adapter.AdapterTaskDashboard;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.NotifDialog;
import com.itant.iproms.lib.fragment.CaldroidSampleCustomFragment;
import com.itant.iproms.lib.object.Filter;
import com.itant.iproms.lib.object.Holiday;
import com.itant.iproms.lib.object.HolidayList;
import com.itant.iproms.lib.object.HolidayMap;
import com.itant.iproms.lib.object.MyCalendar;
import com.itant.iproms.lib.object.Nav;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.Task;
import com.itant.iproms.lib.object.TaskList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.lib.services.BackgroundReceiver;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.helper.MAndroidUtil;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MDialog;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.itant.iproms.mapp.mObject.UserPermission;
import com.roomorama.caldroid.CaldroidFragment;
import com.tooltip.Tooltip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static com.itant.iproms.lib.services.BackgroundReceiver.BACKGROUND_RECEIVER_CODE;

public class MainActivity extends MAppCompat {

    private Activity activity;
    private ApiIProms apiIProms;
    private MSharedData sharedData;
    private CaldroidFragment caldroidFragment;
    private FrameLayout frameLayout;
    private ActionBarDrawerToggle drawerToggle;
    private FloatingActionButton fabAddProject;
    private DrawerLayout drawerLayout;
    private AdapterTaskDashboard adapterTaskDashboard;
    private RecyclerView taskRecyclerView, holidayRecyclerView;
    private GridLayoutManager mLayoutManager;
    private Toolbar toolbar;
    private NotifDialog notifDialog;
    private MTextView toolbarTitle, tvUsernameLogin, tvUsernameWelcome;
    private View headerLayout;
    private ImageView imageNotifBell;
    private String usernameLogin;
    private View calShadow;
    private RelativeLayout layoutSad;
    private RelativeLayout viewMyProject;
    private View viewMyTask, viewNewTask;
    public static final int NEW_PROJECT_REQUEST = 3;
    private Boolean tooltipShow;
    private Tooltip tooltipMyProject, tooltipMyTask, tooltipNewTask;
    private MTextView tvMyProjectCount, tvMyTaskCount, tvNewTaskCount, tvVersion;
    private ScrollView layoutMain;
    private RelativeLayout layoutLoadingScreen;
    public static boolean active = false;
    private boolean firstOpen = false;
    private Integer developermode;
    private Menu mainNavMenu;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText("KALENDER");
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);

//        Intent alarm = new Intent(this, BackgroundReceiver.class);
//        boolean alarmRunning = (PendingIntent.getBroadcast(this, BACKGROUND_RECEIVER_CODE, alarm, PendingIntent.FLAG_NO_CREATE) != null);
//        if(alarmRunning == false) {
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, BACKGROUND_RECEIVER_CODE, alarm, 0);
//            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 60000, pendingIntent);
//        }

        //SharedData
        User user = User.getInstance(activity);
        sharedData = MSharedData.getInstance(this);
        usernameLogin = sharedData.get(SharedData.USER_COMPLETE_NAME);

        // Find our drawer view
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_main);
        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        headerLayout = navigationView.getHeaderView(0);

        layoutMain = findViewById(R.id.layout_main);

        UserPermission userPermission = UserPermission.getInstance();

        tvUsernameLogin = headerLayout.findViewById(R.id.tv_username_login);
        tvUsernameLogin.setText(usernameLogin);
        tvUsernameWelcome = headerLayout.findViewById(R.id.tv_username_welcome);
        tvVersion = findViewById(R.id.tv_version);
        Typeface typefaceUsername = Typeface.createFromAsset(getAssets(), "fonts/NuevaStd-Bold.otf");
        tvUsernameLogin.setTypeface(typefaceUsername);
        Typeface typefaceWelcome = Typeface.createFromAsset(getAssets(), "fonts/NirmalaUIBold.ttf");
        tvUsernameWelcome.setTypeface(typefaceWelcome);

        //layout
        layoutSad = findViewById(R.id.layout_sad);
        tvMyProjectCount = findViewById(R.id.tv_my_project_count);
        tvMyTaskCount = findViewById(R.id.tv_my_task_count);
        tvNewTaskCount = findViewById(R.id.tv_new_task_count);
        layoutSad = findViewById(R.id.layout_sad);
        layoutLoadingScreen = findViewById(R.id.layout_loading_screen);
        layoutLoadingScreen.setVisibility(View.GONE);

        layoutLoadingScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        tvMyProjectCount.setText(user.getMyProject());
        tvMyTaskCount.setText(user.getMyTask());
        tvNewTaskCount.setText(user.getWeekTask());

        tvMyProjectCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MyProjectActivity.class);
                startActivity(intent);
            }
        });

        tvMyTaskCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MyTaskActivity.class);
                startActivity(intent);
            }
        });

        tvNewTaskCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MyTaskActivity.class);
                startActivity(intent);
            }
        });
        fabAddProject = findViewById(R.id.fab_add_project);
        fabAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                userChipActivity.getSelectedChipList().clear();
                Intent intent = new Intent(activity, NewProjectActivity.class);
                startActivityForResult(intent, NEW_PROJECT_REQUEST);
                overridePendingTransition(R.anim.fab_anim, 0);
            }
        });

        System.out.println("waa" + !userPermission.isExists(Nav.NEW_PROJECT));

        if (!userPermission.isExists(Nav.NEW_PROJECT)) {
            fabAddProject.setVisibility(View.GONE);
        }

        viewMyProject = findViewById(R.id.view_my_project);
        viewMyTask = findViewById(R.id.view_my_task);
        viewNewTask = findViewById(R.id.view_new_task);

        tooltipMyProject = new Tooltip.Builder(viewMyProject)
                .setText(getResources().getString(R.string.tooltip_my_project)).setBackgroundColor(getResources().getColor(R.color.md_grey_300)).setCancelable(true)
                .setTextColor(getResources().getColor(R.color.colorTextBlack)).setDismissOnClick(true).setCornerRadius(20f).build();

        tooltipMyTask = new Tooltip.Builder(viewMyTask)
                .setText(getResources().getString(R.string.tooltip_my_task)).setBackgroundColor(getResources().getColor(R.color.md_grey_300)).setCancelable(true)
                .setTextColor(getResources().getColor(R.color.colorTextBlack)).setDismissOnClick(true).setCornerRadius(20f).build();


        tooltipNewTask = new Tooltip.Builder(viewNewTask)
                .setText(getResources().getString(R.string.tooltip_new_task)).setBackgroundColor(getResources().getColor(R.color.md_grey_300)).setCancelable(true)
                .setTextColor(getResources().getColor(R.color.colorTextBlack)).setDismissOnClick(true).setCornerRadius(20f).build();


        viewMyProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tooltipMyTask.isShowing()) {
                    tooltipMyTask.dismiss();
                }
                if (tooltipNewTask.isShowing()) {
                    tooltipNewTask.dismiss();
                }
                if (tooltipMyProject.isShowing()) {
                    tooltipMyProject.dismiss();
                } else {
                    tooltipMyProject.show();
                }

            }
        });

        viewMyTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tooltipNewTask.isShowing()) {
                    tooltipNewTask.dismiss();
                }
                if (tooltipMyProject.isShowing()) {
                    tooltipMyProject.dismiss();
                }
                if (tooltipMyTask.isShowing()) {
                    tooltipMyTask.dismiss();
                } else {
                    tooltipMyTask.show();
                }
            }
        });

        viewNewTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tooltipMyProject.isShowing()) {
                    tooltipMyProject.dismiss();
                }
                if (tooltipMyTask.isShowing()) {
                    tooltipMyTask.dismiss();
                }
                if (tooltipNewTask.isShowing()) {
                    tooltipNewTask.dismiss();
                } else {
                    tooltipNewTask.show();
                }
            }
        });

        calShadow = findViewById(R.id.cal_container_shadow);


        //dialog notif
        notifDialog = new NotifDialog(activity);
        imageNotifBell = findViewById(R.id.image_notif_bell);
        imageNotifBell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifDialog.show();
            }
        });


        //Mini Project isTablet

        if (MAndroidUtil.isTablet(activity)) {

            MTextView tvMyProject = findViewById(R.id.tv_tab_my_project);
            FontStyle.nirmalaUIBoldExtraLarge(activity, tvMyProject);

            RecyclerView projectRecyclerView = findViewById(R.id.project_recycler_view);
            LinearLayoutManager linearLayoutManager
                    = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

            projectRecyclerView.setLayoutManager(linearLayoutManager);
            projectRecyclerView.setItemAnimator(new DefaultItemAnimator());
            doApiProject("0", projectRecyclerView);
        }
        //Recycleview bot
        taskRecyclerView = findViewById(R.id.task_recycler_view);
        if (MAndroidUtil.isTablet(activity)) {
            mLayoutManager = new GridLayoutManager(activity, 2);
        } else {
            mLayoutManager = new GridLayoutManager(activity, 1);
        }
        taskRecyclerView.setLayoutManager(mLayoutManager);
        taskRecyclerView.setItemAnimator(new DefaultItemAnimator());


        holidayRecyclerView = findViewById(R.id.holiday_recycler_view);
        mLayoutManager = new GridLayoutManager(activity, 1);
        holidayRecyclerView.setLayoutManager(mLayoutManager);
        holidayRecyclerView.setItemAnimator(new DefaultItemAnimator());


        //        doApiTaskFilter("0");
//        doApiDashboard();
        // Attach to the activity


        //get ANDROID VERSION
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionText = "";
            String versionCode = String.valueOf(pInfo.versionCode);
            for (int i = 0; i < versionCode.length(); i++) {
                versionCode.substring(i, i + 1);
                if (i + 1 == versionCode.length()) {
                    versionText = versionText + versionCode.substring(i, i + 1);
                } else {
                    versionText = versionText + versionCode.substring(i, i + 1) + ".";

                }
            }
            tvVersion.setText("App Version " + versionText);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Filter filter = Filter.getInstance(activity);
        MyCalendar myCalendar = MyCalendar.getInstance(activity);
        filter.setPlanStartDate(myCalendar.getSelectedDate());
        filter.setPlanEndDate(myCalendar.getSelectedDate());
//        doApiTaskFilter("0");
    }

    @Override
    protected void onResume() {
        super.onResume();
        doApiDashboard();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_main);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                final MDialog messageDialog = new MDialog(this, R.style.dialogNoTitle);
                messageDialog.setButtonType("yesno");
                String strLblPopup = activity.getResources().getString(R.string.quit_app_ask);
                messageDialog.setMessage(strLblPopup);
                Button yesbutton = (Button) messageDialog.findViewById(R.id.btn_yes);
                yesbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                Button nobutton = (Button) messageDialog.findViewById(R.id.btn_no);
                nobutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        messageDialog.dismiss();

                    }
                });
                messageDialog.show();
            }
        } else {
            final MDialog messageDialog = new MDialog(this, R.style.dialogNoTitle);
            messageDialog.setButtonType("yesno");
            String strLblPopup = activity.getResources().getString(R.string.quit_app_ask);
            messageDialog.setMessage(strLblPopup);
            Button yesbutton = (Button) messageDialog.findViewById(R.id.btn_yes);
            yesbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            Button nobutton = (Button) messageDialog.findViewById(R.id.btn_no);
            nobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messageDialog.dismiss();

                }
            });
            messageDialog.show();
        }

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        if (requestCode == NEW_PROJECT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Snackbar.make(drawerLayout, "Proyek Berhasil Ditambahkan", Snackbar.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    public void doApiTaskFilter(final String page) {
        taskRecyclerView.setVisibility(View.GONE);
        layoutLoadingScreen.setVisibility(View.VISIBLE);
        try {
            Filter filter = Filter.getInstance(activity);
            apiIProms = ApiIProms.factory(this, "TaskFilter");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("user_id", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Task task = Task.getInstance(activity);
                        TaskList taskListArr = TaskList.getInstance(activity);
                        ArrayList<Task> tempArraylist = new ArrayList<Task>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject taskList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = taskList.getJSONArray("users");
                                task = new Task();

                                if (taskList.getString("to_do_list_id").length() > 0) {
                                    pageCount += 1;
                                }

                                task.setId(taskList.getString("to_do_list_id"));
                                task.setCreated(taskList.getString("creator_name"));
                                task.setCreatedDate(taskList.getString("created"));
                                task.setDescription(taskList.getString("description"));
                                task.setTaskCode(taskList.getString("to_do_list_code"));
                                task.setTaskName(taskList.getString("to_do_list_name"));
                                task.setTaskStatus(taskList.getString("to_do_list_status"));
                                task.setProjectName(taskList.getString("project_name"));
                                task.setProjectDate(taskList.getString("project_created_date"));
                                task.setProjectCreator(taskList.getString("project_created_by"));
                                task.setStartDate(taskList.getString("plan_start_date"));
                                task.setEndDate(taskList.getString("plan_end_date"));
                                task.setActualStartDate(taskList.getString("actual_start_date"));
                                task.setActualEndDate(taskList.getString("actual_end_date"));
                                task.setUserOrder(taskList.getString("user_order_name"));
                                task.setUserApprove(taskList.getString("user_approve"));
                                task.setTaskReason(taskList.getString("reason"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    user.setFullname(userArrList.getString("user_employee_name"));
                                    userList.add(user);
                                }

                                task.setUserList(userList);
                                tempArraylist.add(task);
                            }

                            task = Task.getInstance(activity);

                            taskListArr.setTaskArrayList(tempArraylist);
                            adapterTaskDashboard = new AdapterTaskDashboard(activity, taskListArr.getTaskArrayList());
                            taskRecyclerView.setAdapter(adapterTaskDashboard);

                            if (taskListArr.getTaskArrayList().size() <= 0) {
                                calShadow.setVisibility(View.GONE);
                                layoutSad.setVisibility(View.VISIBLE);
                            } else {
                                calShadow.setVisibility(View.GONE);
                                layoutSad.setVisibility(View.GONE);
                            }

                            layoutLoadingScreen.setVisibility(View.GONE);
                            taskRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            layoutLoadingScreen.setVisibility(View.GONE);
                            taskRecyclerView.setVisibility(View.VISIBLE);
                            Snackbar.make(drawerLayout, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        layoutLoadingScreen.setVisibility(View.GONE);
                        taskRecyclerView.setVisibility(View.VISIBLE);
                        Snackbar.make(drawerLayout, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    layoutLoadingScreen.setVisibility(View.GONE);

                }
            });
        } catch (MApiException e1) {
            layoutLoadingScreen.setVisibility(View.GONE);
            e1.printStackTrace();
        }
    }

    public void refreshEventList(String todayDate) {
        holidayRecyclerView.setVisibility(View.GONE);
        taskRecyclerView.setVisibility(View.GONE);
        layoutLoadingScreen.setVisibility(View.VISIBLE);

        //project recyclerview
        ProjectList projectList = ProjectList.getInstance(activity);
        System.out.println("WAA1" + todayDate);
        ArrayList<Project> tempProjectArrayList = new ArrayList<Project>();
        for (int i = 0; i <= projectList.getProjectArrayList().size() - 1; i++) {
            System.out.println("WAA2" + projectList.getProjectArrayList().get(i).getEndDate());
            if (todayDate.equalsIgnoreCase(projectList.getProjectArrayList().get(i).getEndDate())) {
                tempProjectArrayList.add(projectList.getProjectArrayList().get(i));
            }
        }
        System.out.println(tempProjectArrayList);
        AdapterProjectDashboard adapterProjectDashboard = new AdapterProjectDashboard(activity, tempProjectArrayList);

        //holiday recyclerview
        HolidayList holidayList = HolidayList.getInstance(activity);
        ArrayList<Holiday> tempHolidayList = new ArrayList<Holiday>();
        for (int i = 0; i <= holidayList.getHolidayArrayList().size() - 1; i++) {
            if (todayDate.equalsIgnoreCase(holidayList.getHolidayArrayList().get(i).getDate())) {
                tempHolidayList.add(holidayList.getHolidayArrayList().get(i));
            }
        }
        System.out.println(tempProjectArrayList);
        AdapterHolidayDashboard adapterHolidayDashboard = new AdapterHolidayDashboard(activity, tempHolidayList);

        holidayRecyclerView.setAdapter(adapterHolidayDashboard);
        taskRecyclerView.setAdapter(adapterProjectDashboard);

        if (tempProjectArrayList.size() <= 0 && tempHolidayList.size() <= 0) {
            calShadow.setVisibility(View.GONE);
            layoutSad.setVisibility(View.VISIBLE);
        } else {
            calShadow.setVisibility(View.GONE);
            layoutSad.setVisibility(View.GONE);
        }

        layoutLoadingScreen.setVisibility(View.GONE);
        taskRecyclerView.setVisibility(View.VISIBLE);
        holidayRecyclerView.setVisibility(View.VISIBLE);
    }

    public void doApiDashboard() {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "Dashboard");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        User user = User.getInstance(activity);

                        ProjectList projectList = ProjectList.getInstance(activity);
                        ArrayList<Project> tempProjectArrayList = new ArrayList<Project>();
                        Project project;

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            user.setMyTask(data.getString("my_task_pending"));
                            user.setWeekTask(data.getString("deadline_this_week"));
                            user.setMyProject(data.getString("my_project"));
                            tvMyProjectCount.setText(user.getMyProject());
                            tvMyTaskCount.setText(user.getMyTask());
                            tvNewTaskCount.setText(user.getWeekTask());

                            JSONArray holidayArray = data.getJSONArray("holiday");
                            HolidayMap holidayMap = HolidayMap.getInstance(activity);
                            ArrayList<Holiday> tempHolidayArrayList = new ArrayList<Holiday>();
                            Holiday holiday;
                            for (int i = 0; i <= holidayArray.length() - 1; i++) {
                                JSONObject holidayObj = holidayArray.getJSONObject(i);
                                holiday = new Holiday(activity);
                                holiday.setId(holidayObj.getString("holiday_id"));
                                holiday.setName(holidayObj.getString("holiday_name"));
                                holiday.setDate(holidayObj.getString("holiday_date"));
                                tempHolidayArrayList.add(holiday);
                                holidayMap.add(holiday);
                            }
                            HolidayList holidayList = HolidayList.getInstance(activity);
                            holidayList.setHolidayArrayList(tempHolidayArrayList);

                            JSONArray projectArray = data.getJSONArray("all_project");

                            for (int i = 0; i <= projectArray.length() - 1; i++) {

                                UserList userList = new UserList(activity);
                                JSONObject projectObj = projectArray.getJSONObject(i);
                                JSONArray userArr = projectObj.getJSONArray("users");
                                project = new Project(activity);
                                project.setId(projectObj.getString("project_id"));
                                project.setCreated(projectObj.getString("created"));
                                project.setCreatedBy(projectObj.getString("createdby"));
                                project.setDescription(projectObj.getString("description"));
                                project.setProjectCode(projectObj.getString("project_code"));
                                project.setProjectName(projectObj.getString("project_name"));
                                project.setProjectStatus(projectObj.getString("status_project"));
                                project.setStartDate(projectObj.getString("plan_start_date"));
                                project.setEndDate(projectObj.getString("plan_end_date"));
                                project.setActualStartDate(projectObj.getString("actual_start_date"));
                                project.setActualEndDate(projectObj.getString("actual_end_date"));

                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    user.setFirstName(userArrList.getString("first_name"));
                                    user.setLastName(userArrList.getString("last_name"));
                                    user.setEmail(userArrList.getString("email"));
                                    userList.add(user);
                                }

                                project.setUserList(userList);
                                tempProjectArrayList.add(project);

                            }

                            project = Project.getInstance(activity);
                            projectList.setProjectArrayList(tempProjectArrayList);

                            //Set caldroid args settings
                            caldroidFragment = new CaldroidSampleCustomFragment();
                            Bundle args = new Bundle();
                            Calendar cal = Calendar.getInstance();
                            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
                            args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
                            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
                            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
                            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
                            frameLayout = findViewById(R.id.cal_container);
                            caldroidFragment.setArguments(args);

                            Filter filter = Filter.getInstance(activity);
                            String dayDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
                            if (dayDate.length() == 1) {
                                dayDate = "0" + dayDate;
                            }

                            String monthString = String.valueOf(cal.get(Calendar.MONTH) + 1);
                            if (monthString.length() == 1) {
                                monthString = "0" + monthString;
                            }
                            String todayDateString = dayDate + "-" + monthString + "-" + cal.get(Calendar.YEAR);
                            filter.setPlanStartDate(todayDateString);
                            filter.setPlanEndDate(todayDateString);

                            MyCalendar myCalendar = MyCalendar.getInstance(activity);
                            myCalendar.setSelectedDate(todayDateString);

                            String selectedDayDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
                            if (selectedDayDate.length() == 1) {
                                selectedDayDate = "0" + selectedDayDate;
                            }

                            String selectedDateString = cal.get(Calendar.YEAR)
                                    + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + selectedDayDate;
                            refreshEventList(selectedDateString);
                            FragmentTransaction t = getSupportFragmentManager().beginTransaction();
                            t.replace(R.id.cal_container, caldroidFragment);
                            t.commitAllowingStateLoss();

                            closeDialog(activity);
                        } else {
                            Snackbar.make(drawerLayout, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        layoutLoadingScreen.setVisibility(View.GONE);
                        Snackbar.make(drawerLayout, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    layoutLoadingScreen.setVisibility(View.GONE);
                    closeDialog(activity);

                }
            });
        } catch (MApiException e1) {
            layoutLoadingScreen.setVisibility(View.GONE);
            closeDialog(activity);
            e1.printStackTrace();
        }
    }

    public void doApiProject(final String page, final RecyclerView recyclerView) {
        try {
            apiIProms = ApiIProms.factory(this, "ProjectList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("show", "all");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        Project project = Project.getInstance(activity);
                        ProjectList projectListArr = ProjectList.getInstance(activity);
                        ArrayList<Project> tempArraylist = new ArrayList<Project>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                UserList userList = new UserList(activity);
                                JSONObject projectList = (JSONObject) resultsArr.get(i);
                                JSONArray userArr = projectList.getJSONArray("users");

                                project = new Project();
                                if (projectList.getString("project_id").length() > 0) {
                                    pageCount += 1;
                                }
                                project.setId(projectList.getString("project_id"));
                                project.setCreated(projectList.getString("created"));
                                project.setCreatedBy(projectList.getString("createdby"));
                                project.setDescription(projectList.getString("description"));
                                project.setProjectCode(projectList.getString("project_code"));
                                project.setProjectName(projectList.getString("project_name"));
                                project.setProjectStatus(projectList.getString("status_project"));
                                project.setStartDate(projectList.getString("plan_start_date"));
                                project.setEndDate(projectList.getString("plan_end_date"));
                                project.setActualStartDate(projectList.getString("actual_start_date"));
                                project.setActualEndDate(projectList.getString("actual_end_date"));


                                for (int j = 0; j < userArr.length(); j++) {
                                    JSONObject userArrList = (JSONObject) userArr.get(j);
                                    User user = new User(activity);
                                    user.setUserId(userArrList.getString("user_id_employee"));
                                    user.setUsername(userArrList.getString("username_employee"));
                                    user.setFirstName(userArrList.getString("first_name"));
                                    user.setLastName(userArrList.getString("last_name"));
                                    user.setEmail(userArrList.getString("email"));
                                    userList.add(user);
                                }

                                project.setUserList(userList);
                                tempArraylist.add(project);
                            }

                            project = Project.getInstance(activity);

                            projectListArr.setProjectArrayList(tempArraylist);
                            AdapterProject adapterProject = new AdapterProject(activity, projectListArr.getProjectArrayList());
                            recyclerView.setAdapter(adapterProject);
                            closeDialogWhite(activity);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

}
