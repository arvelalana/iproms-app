package com.itant.iproms;

import android.app.LauncherActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.itant.iproms.lib.Helper.RunningCheck;
import com.itant.iproms.mapp.helper.MAndroidUtil;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public class MAppMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MAppMessagingService";
    private String myTitle = "";

    public MAppMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        System.out.println("NOTIFIKASI BARU MASUK");
        System.out.println("New NOTIF" + remoteMessage);
        // Check if message contains a notification payload.
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> map = remoteMessage.getData();
            showNotification(map);
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            Map<String, String> map = remoteMessage.getData();
            Log.d(TAG, "Message NotificationData Title: " + remoteMessage.getNotification().getTitle());
            Log.d(TAG, "Message NotificationData Body: " + map);
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();
        }
    }

    private void showNotification(Map messageData) {
        String image = messageData.get("image").toString();
        String module = messageData.get("module").toString();
        String body = messageData.get("body").toString();
        String title = messageData.get("title").toString();

        Class activityClass = SplashActivity.class;
        Intent intent = new Intent(this, activityClass);
        String moduleFirstWord = module.substring(0, 3);


        final Boolean isRunning;
        try {
            isRunning = new RunningCheck().execute(this).get();
            if (isRunning) {
                System.out.println("APPKU SEDANG BERJALAN");

                if (moduleFirstWord.equalsIgnoreCase("PJT")) {
                    activityClass = MyProjectActivity.class;
                } else if (moduleFirstWord.equalsIgnoreCase("TDL")) {
                    activityClass = MyTaskActivity.class;
                } else if (moduleFirstWord.equalsIgnoreCase("DAC")) {
                    activityClass = MyDailyActivity.class;
                } else if (moduleFirstWord.equalsIgnoreCase("MET")) {
                    activityClass = MyMeetingActivity.class;
                }


                intent = new Intent(this, activityClass);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);
                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), (this.getResources().getIdentifier("icon_main", "drawable", this.getPackageName())));
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setLargeIcon(icon)
                        .setSmallIcon(R.drawable.ic_o_logo)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                notificationManager.notify(0, notificationBuilder.build());
            } else {
                System.out.println("APPKU SEDANG TIDAK BERJALAN");

                intent = new Intent(this, activityClass);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);
                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), (this.getResources().getIdentifier("icon_main", "drawable", this.getPackageName())));
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setLargeIcon(icon)
                        .setSmallIcon(R.drawable.ic_o_logo)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                notificationManager.notify(0, notificationBuilder.build());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }
}

