package com.itant.iproms;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Nav;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mObject.UserPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends MAppCompat {

    private ApiIProms apiIProms;
    private MSharedData sharedData;
    private Activity activity;
    private ImageView imgOLogo;
    private ConstraintLayout constraintLayout;

    private Timer splashTimer;
    private Integer splashCountdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        activity = this;
        sharedData = MSharedData.getInstance(this);
        //HARAPAN ABADI f7659b7d956c8fd9c762c8af5bdd9e72
        //PT KIP 7ebac7f271ae78983609f6298900885f
        sharedData.set(SharedData.AUTH_ID, "f7659b7d956c8fd9c762c8af5bdd9e72");




        constraintLayout = findViewById(R.id.layout_splash);
        imgOLogo = findViewById(R.id.image_o_logo);
        splashCountdown = 0;
        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(3000);
        imgOLogo.startAnimation(anim);

        splashTimer = new Timer();
        splashTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                splashCountdown++;
                System.out.println(splashCountdown);
                if (splashCountdown == 3) {
                    doApiSession(constraintLayout);
                    splashTimer.cancel();
                }
            }

        }, 0, 1000);

    }

    public void doApiSession(final View view) {
        ApiIProms apiIProms = null;
        final MSharedData sharedData = MSharedData.getInstance(this);
        try {
            apiIProms = ApiIProms.factory(this, "Session");
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            String sessionId = data.getString("session_id");
                            String orgId = data.getString("org_id");

                            sharedData.set(SharedData.ORG_ID, orgId);
                            sharedData.set(SharedData.SESSION_ID, sessionId);

                            String userToken = sharedData.get(SharedData.USER_TOKEN);
                            if (userToken != null && userToken.length() > 0) {
                                doApiLogin();
                            } else {
                                Intent intent = new Intent(activity, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Snackbar.make(view, errMessage, Snackbar.LENGTH_LONG)
                                    .show();

                        }
                    } catch (JSONException e) {
                        Snackbar.make(view, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                    Snackbar.make(view, getResources().getString(R.string.connection_error), Snackbar.LENGTH_LONG)
                            .show();
                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    private void doApiLogin() {
        try {
            apiIProms = ApiIProms.factory(this, "KeepMeLoggedIn");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            String userCompleteName = data.getString("first_name") + " " + data.getString("last_name");
                            String username = data.getString("username");
                            String userId = data.getString("user_id");
                            String userToken = data.getString("user_token");
                            String userEmail = data.getString("email");
                            String permissions = data.getString("permissions");
                            String userParentId = data.getString("users_parent_id");

                            User user = User.getInstance(activity);
                            user.setUserId(userId);
                            user.setUsername(username);
                            user.setEmail(userEmail);
                            user.setUserParentId(userParentId);
                            user.setFirstName(data.getString("first_name"));
                            user.setLastName(data.getString("last_name"));
                            user.setMyProject(data.getString("my_project"));
                            user.setMyTask(data.getString("my_task_pending"));
                            user.setWeekTask(data.getString("deadline_this_week"));
                            sharedData.set(SharedData.USERNAME, username);
                            sharedData.set(SharedData.USER_ID, userId);
                            sharedData.set(SharedData.USER_TOKEN, userToken);
                            sharedData.set(SharedData.USER_COMPLETE_NAME, userCompleteName);
                            sharedData.set(SharedData.USER_PARENT_ID, userParentId);

                            UserPermission userPermission = UserPermission.getInstance();
                            userPermission.setRawData(permissions);
                            userPermission.process();

                            System.out.println("waa" + userPermission.toString());

                            if (!userPermission.isExists(Nav.NEW_PROJECT)) {

                            }

                            Intent intent = new Intent(activity, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(activity, LoginActivity.class);
                            startActivity(intent);
                            finish();
                            Snackbar.make(constraintLayout, errMessage, Snackbar.LENGTH_LONG)
                                    .show();

                        }
                    } catch (JSONException e) {
                        Snackbar.make(constraintLayout, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }
}
