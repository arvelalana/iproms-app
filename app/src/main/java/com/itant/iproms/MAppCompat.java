package com.itant.iproms;

/**
 * Created by x441s on 30/08/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.dialog.LoadingSmallDialog;
import com.itant.iproms.lib.dialog.LoadingSmallReverseDialog;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MDialog;
import com.itant.iproms.mapp.mComponent.MProgressDialog;

/**
 * Created by Raymond on 4/27/2017.
 */

public class MAppCompat extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public Activity activity;
    private MProgressDialog progressDialog;
    private Boolean Activated = false;
    private MSharedData sharedData;
    private LoadingSmallDialog loadingSmallDialog;
    private LoadingSmallReverseDialog loadingSmallReverseDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_main);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.going_out, R.anim.going_in);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent intent = null;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_main);
        drawer.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_my_project) {
            try {
                intent = new Intent(this, MyProjectActivity.class);
                this.startActivity(intent);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id == R.id.nav_setting) {
            try {
                intent = new Intent(this, NewMyTaskActivity.class);
                this.startActivity(intent);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id == R.id.nav_new_task) {
            try {
                intent = new Intent(this, NewTaskActivity.class);
                this.startActivity(intent);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id == R.id.nav_my_task) {
            try {
                intent = new Intent(this, MyTaskActivity.class);
                this.startActivity(intent);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id == R.id.nav_report) {
            try {
                intent = new Intent(this, ReportActivity.class);
                this.startActivity(intent);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id == R.id.nav_daily_activity) {
            try {
                intent = new Intent(this, MyDailyActivity.class);
                this.startActivity(intent);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id == R.id.nav_meeting) {
            try {
                intent = new Intent(this, MyMeetingActivity.class);
                this.startActivity(intent);
                overridePendingTransition(R.anim.coming_in, R.anim.coming_out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id == R.id.nav_logout) {
            final MDialog messageDialog = new MDialog(activity, R.style.dialogNoTitle);
            messageDialog.setButtonType("yesno");
            String strLblPopup = activity.getResources().getString(R.string.logout_ask);
            messageDialog.setMessage(strLblPopup);
            Button yesbutton = (Button) messageDialog.findViewById(R.id.btn_yes);
            yesbutton.setText(activity.getResources().getString(R.string.btn_msg_yes_in));
            yesbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendLogout();
                    messageDialog.dismiss();
                }
            });
            Button nobutton = (Button) messageDialog.findViewById(R.id.btn_no);
            nobutton.setText(activity.getResources().getString(R.string.btn_msg_no_in));
            nobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messageDialog.dismiss();

                }
            });
            messageDialog.show();
        }
        return true;
    }

    public void ApiSession(final View view) {
        final Boolean isSuccess;
        ApiIProms apiIProms = null;
        final MSharedData sharedData = MSharedData.getInstance(this);
        try {
            apiIProms = ApiIProms.factory(this, "Session");
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            String sessionId = data.getString("session_id");
                            String orgId = data.getString("org_id");
                            sharedData.set(SharedData.ORG_ID, orgId);
                            sharedData.set(SharedData.SESSION_ID, sessionId);
                            System.out.println("SUCCESS " + sessionId);
                        } else {
                            Snackbar.make(view, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(view, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    public ActionBarDrawerToggle setupDrawerToggle(Toolbar toolbar) {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_main);
        return new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    protected void openDialog(Context dContext) {
//        progressDialog = new MProgressDialog(dContext);
//        progressDialog.show();
        loadingSmallDialog = new LoadingSmallDialog(dContext);
        loadingSmallDialog.show();
    }

    protected void closeDialog(Context dContext) {
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
        if (loadingSmallDialog != null && loadingSmallDialog.isShowing()) {
            loadingSmallDialog.dismiss();
        }
    }

    protected void openDialogWhite(Context dContext) {
//        progressDialog = new MProgressDialog(dContext);
//        progressDialog.show();
        loadingSmallReverseDialog = new LoadingSmallReverseDialog(dContext);
        loadingSmallReverseDialog.show();
    }

    protected void closeDialogWhite(Context dContext) {
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
        if (loadingSmallReverseDialog != null && loadingSmallReverseDialog.isShowing()) {
            loadingSmallReverseDialog.dismiss();
        }
    }

    protected void closeKeyboard(View layoutMain) {

        if (layoutMain != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(layoutMain.getWindowToken(), 0);
        }
    }


    public void sendLogout() {
        openDialog(activity);
        try {
            ApiIProms apiIProms = ApiIProms.factory(activity, "Logout");
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        JSONObject data = responseObj.getJSONObject("data");
                        if (errCode.equalsIgnoreCase("0")) {
                            System.out.println("Logout SUCCESS");
                            final MSharedData sharedData = MSharedData.getInstance(activity);
                            sharedData.setBoolean(SharedData.IS_LOGIN, false);
                            final MDialog messageDialog = new MDialog(activity, R.style.dialogNoTitle);
                            messageDialog.setMessage("Logout Success");
                            messageDialog.setButtonType(MDialog.BTN_OK);
                            messageDialog.getBtnOk().setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    try {
                                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                                        userChipActivity.getSelectedChipList().clear();
                                        userChipActivity.getUserChipArrayList().clear();
                                        sharedData.set(SharedData.USER_TOKEN, "");
                                        Intent intent = new Intent(activity, LoginActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.going_out, R.anim.going_in);
                                        finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            messageDialog.show();

                        } else {
                            Snackbar.make(getWindow().getDecorView().getRootView(), errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        System.out.println(e);
                        Snackbar.make(getWindow().getDecorView().getRootView(), getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    Snackbar.make(getWindow().getDecorView().getRootView(), getResources().getString(R.string.connection_error), Snackbar.LENGTH_LONG)
                            .show();
                    closeDialog(activity);

                }
            });

        } catch (MApiException e) {
            Snackbar.make(getWindow().getDecorView().getRootView(), getResources().getString(R.string.unexpected_error), Snackbar.LENGTH_LONG)
                    .show();
            closeDialog(activity);
        }

    }


}
