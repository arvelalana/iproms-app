package com.itant.iproms;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.flexbox.FlexboxLayout;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.Project;
import com.itant.iproms.lib.object.ProjectList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChip;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextInputEditText;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.pchmn.materialchips.ChipView;
import com.pchmn.materialchips.ChipsInput;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.itant.iproms.MyProjectActivity.RESULT_EDIT_PROJECT_SUCCESS;

public class NewProjectActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener {

    public static final int NEW_PROJECT_ADDED = 11;
    public static final int NEW_PROJECT_REQUEST_USER = 1;
    private Activity activity;
    private ApiIProms apiIProms;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ConstraintLayout layoutMain;
    private ArrayList<String> userArrayList;
    private ChipsInput chipsInput;
    private MTextInputEditText edtProjectName, edtShortDescription, editTextStartDate, editTextEndDate;
    private LinearLayout layoutBottomNewProject;
    private FlexboxLayout layoutUserList;
    private String projectName, shortDescription;
    private String dateMode, startDate, endDate;
    private Boolean isOpen = false;
    private Button saveProjectButton, addUser;
    private ArrayList<String> users;
    private String mode, projectId;
    private Project project;
    private Boolean isSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);
        activity = this;

        //getComponent
        edtProjectName = findViewById(R.id.edit_text_project_name);
        edtShortDescription = findViewById(R.id.edit_text_short_description);
        layoutMain = findViewById(R.id.layout_add_project);
        layoutBottomNewProject = findViewById(R.id.ll_bottom_new_project);
        layoutUserList = findViewById(R.id.layout_user_list);
        saveProjectButton = findViewById(R.id.button_save_project);
        addUser = findViewById(R.id.btn_add_user);
        editTextStartDate = findViewById(R.id.edit_text_project_start_date);
        editTextEndDate = findViewById(R.id.edit_text_project_end_date);
//        chipsInput = findViewById(R.id.chips_input);

        //var
        userArrayList = new ArrayList<String>();
        users = new ArrayList<String>();
        isSent = false;
        mode = "add";
        final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
        userChipActivity.getSelectedChipList().clear();

        //Set Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.add_new_project).toUpperCase());
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mode = bundle.getString("mode");
            projectId = bundle.getString("projectId");

            if (mode.equalsIgnoreCase("edit")) {

                ProjectList projectList = ProjectList.getInstance(activity);
                project = projectList.get(projectId);
                toolbarTitle.setText(getResources().getString(R.string.edit_project).toUpperCase());


                edtProjectName.setText(project.getProjectName());
                edtShortDescription.setText(project.getDescription());

                editTextStartDate.setText(project.getStartDate());
                editTextEndDate.setText(project.getEndDate());
                UserList userList = project.getUserList();
                ChipView chipView;
                UserChip userChip = null;
                userChipActivity.clearSelectedChipList();
                layoutUserList.removeAllViews();

                for (int i = 0; i < userList.getUserArrayList().size(); i++) {
                    String username = userList.getUserArrayList().get(i).getUsername();
                    final String userId = userList.getUserArrayList().get(i).getUserId();
                    userChip = new UserChip(userId, username);
                    chipView = new ChipView(activity);
                    chipView.setPadding(15, 10, 15, 10);
                    chipView.setLabel(userChip.getLabel());
                    chipView.setHasAvatarIcon(true);
                    chipView.setDeletable(true);
                    final ChipView tempChipView = chipView;
                    chipView.setOnDeleteClicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userChipActivity.removeSelectedChipList(userId);
                            layoutUserList.removeView(tempChipView);
                        }
                    });
                    layoutUserList.addView(chipView);
                    userChipActivity.addSelectedChipList(userId, userChip);
                }


                System.out.println("AUW" + userChipActivity.getSelectedChipList());

            }
        }
        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(activity);
                doApiUserList();
//                if (UserChipActivity.getInstance(activity).getTotalUserChip() > 0) {
//                    Intent intent = new Intent(activity, AddUserActivity.class);
//                    startActivityForResult(intent, NEW_PROJECT_REQUEST_USER);
//                } else {
//                    doApiUserList();
//                }
            }
        });


        saveProjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSent) {
                    isSent = true;
                    int err = 0;
                    UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                    HashMap selectedUserList = userChipActivity.getSelectedChipList();
                    System.out.println("AUW" + selectedUserList);
                    Iterator it = selectedUserList.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();

                        users.add(((UserChip) pair.getValue()).getId().toString());
                    }
                    projectName = edtProjectName.getText().toString();
                    shortDescription = edtShortDescription.getText().toString();
                    startDate = editTextStartDate.getText().toString();
                    endDate = editTextEndDate.getText().toString();
                    if (projectName.length() < 1) {
                        err++;
                        edtProjectName.setError(getResources().getString(R.string.project_name_blank));
                    }
                    if (shortDescription.length() < 1) {
                        err++;
                        edtShortDescription.setError(getResources().getString(R.string.short_description_blank));
                    }

                    if (users.size() <= 0) {
                        err++;
                        Snackbar.make(layoutMain, getResources().getString(R.string.user_blank), Snackbar.LENGTH_LONG)
                                .show();
                    }

                    if (startDate.length() < 1) {
                        err++;
                        editTextStartDate.setError(getResources().getString(R.string.start_date_blank));
                    }

                    if (endDate.length() < 1) {
                        err++;
                        editTextEndDate.setError(getResources().getString(R.string.end_date_blank));
                    }

                    if (err == 0) {
                        if (mode.equalsIgnoreCase("edit")) {
                            doApiEditProject();
                        } else {
                            doApiNewProject();
                        }
                    } else {
                        isSent = false;
                    }
                }
            }
        });
    }


    private void doApiUserList() {
        try {
            apiIProms = ApiIProms.factory(this, "UserList");
            HashMap postData = new HashMap();
            postData.put("project_id", "");
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                String username = userList.getString("username");
                                userChipActivity.add(new UserChip(userList.getString("user_id"), username));
                            }

                            Intent intent = new Intent(activity, AddUserActivity.class);
                            startActivityForResult(intent, NEW_PROJECT_REQUEST_USER);

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
        }
    }

    private void doApiNewProject() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "NewProject");
            HashMap postData = new HashMap();
            System.out.println(users);
            postData.put("project_name", projectName);
            postData.put("start_date", startDate);
            postData.put("end_date", endDate);
            postData.put("description", shortDescription);
            postData.put("users", users);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(Activity.RESULT_OK);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Menambahkan Proyek", Snackbar.LENGTH_LONG)
                                    .show();

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }

    private void doApiEditProject() {
        try {
            openDialog(activity);
            apiIProms = ApiIProms.factory(this, "EditProject");
            HashMap postData = new HashMap();
            System.out.println(users);
            postData.put("project_id", projectId);
            postData.put("project_name", projectName);
            postData.put("description", shortDescription);
            postData.put("start_date", startDate);
            postData.put("end_date", endDate);
            postData.put("users", users);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);

                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");

                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            closeDialog(activity);
                            setResult(RESULT_EDIT_PROJECT_SUCCESS);
                            finish();
                            Snackbar.make(layoutMain, "Sukses Mengubah Proyek", Snackbar.LENGTH_LONG)
                                    .show();

                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                        isSent = false;
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        isSent = false;
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    isSent = false;
                }
            });
        } catch (MApiException e1) {
            isSent = false;
            e1.printStackTrace();
        }
    }

    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;
        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Mulai";
            dateValue = editTextStartDate.getText().toString();
        } else {
            title = "Tanggal Berakhir";
            dateValue = editTextEndDate.getText().toString();
        }

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                NewProjectActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );

        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    NewProjectActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");

        Calendar calendarNow = Calendar.getInstance();
        dpd.setMinDate(calendarNow);
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
        } else {
            editTextEndDate.setText(fullDate);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("On Result");
        if (requestCode == NEW_PROJECT_REQUEST_USER) {
            if (resultCode == Activity.RESULT_OK) {
                closeDialog(activity);
                final UserChipActivity userChipActivity = UserChipActivity.getInstance(activity);
                layoutUserList.removeAllViews();
                ChipView chipView;
                UserChip userChip;
                Iterator it = userChipActivity.getSelectedChipList().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();

                    userChip = (UserChip) pair.getValue();

                    chipView = new ChipView(activity);
                    chipView.setPadding(15, 10, 15, 10);
                    chipView.setLabel(userChip.getLabel());
                    chipView.setHasAvatarIcon(true);
                    chipView.setDeletable(true);

                    final ChipView tempChipView = chipView;
                    final String userId = pair.getKey().toString();
                    chipView.setOnDeleteClicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userChipActivity.removeSelectedChipList(userId);
                            layoutUserList.removeView(tempChipView);
                        }
                    });
                    layoutUserList.addView(chipView);
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // do nothing
                closeDialog(activity);
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }
}
