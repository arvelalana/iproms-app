package com.itant.iproms;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.itant.iproms.lib.Helper.DownloadTask;
import com.itant.iproms.lib.Helper.FontStyle;
import com.itant.iproms.lib.SharedData;
import com.itant.iproms.lib.adapter.AdapterDaily;
import com.itant.iproms.lib.api.ApiIProms;
import com.itant.iproms.lib.object.DailyActivity;
import com.itant.iproms.lib.object.DailyActivityList;
import com.itant.iproms.lib.object.User;
import com.itant.iproms.lib.object.UserChipActivity;
import com.itant.iproms.lib.object.UserList;
import com.itant.iproms.mapp.MSharedData;
import com.itant.iproms.mapp.mApi.MApi;
import com.itant.iproms.mapp.mApi.MApiException;
import com.itant.iproms.mapp.mComponent.MTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ReportPerformanceActivity extends MAppCompat implements DatePickerDialog.OnDateSetListener {
    private Activity activity;
    private Toolbar toolbar;
    private MTextView toolbarTitle;
    private ApiIProms apiIProms;
    private RecyclerView dailyRecyclerView;
    private DrawerLayout layoutMain;
    private GridLayoutManager mLayoutManager;
    private Spinner spinnerEmployee;
    private Button showTask, downloadPdf;
    private Boolean isOpen = false;
    private String dateMode;
    private String userId;
    private EditText editTextStartDate, editTextEndDate;
    private String filterStartDate, filterEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_report);
        activity = this;
        layoutMain = findViewById(R.id.drawer_main);

        //Set Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.performance_report).toUpperCase());
        FontStyle.toolbarTitle(activity, toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        editTextStartDate = findViewById(R.id.edit_text_performance_start_date);
        editTextEndDate = findViewById(R.id.edit_text_performance_end_date);
        spinnerEmployee = findViewById(R.id.spinner_performance_report);
        showTask = findViewById(R.id.btn_show_performance_report);
        dailyRecyclerView = findViewById(R.id.performance_report_recycler_view);
        downloadPdf = findViewById(R.id.btn_download_pdf);


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String stringDate = simpleDateFormat.format(new Date());
        editTextStartDate.setText(stringDate);
        editTextEndDate.setText(stringDate);

        mLayoutManager = new GridLayoutManager(activity, 1);
        dailyRecyclerView.setLayoutManager(mLayoutManager);
        dailyRecyclerView.setItemAnimator(new DefaultItemAnimator());
        doApiSubordinate();

        showTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;
                editTextStartDate.setError(null);
                editTextEndDate.setError(null);
                UserList userList = UserList.getInstance(activity);
                if (spinnerEmployee.getSelectedItemPosition() >= 0) {
                    userId = userList.getPosition(spinnerEmployee.getSelectedItemPosition()).getUserId();
                }

                filterStartDate = editTextStartDate.getText().toString();
                filterEndDate = editTextEndDate.getText().toString();

                if (filterStartDate.length() <= 0) {
                    err++;
                    editTextStartDate.setError("Isi tanggal awal kinerja harian");
                }

                if (filterEndDate.length() <= 0) {
                    err++;
                    editTextEndDate.setError("Isi tanggal akhir kinerja harian");
                }

                if (err == 0) {
                    doApiDailyACtivity("0");
                }
            }
        });

        editTextStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "start";
                    datePickerOnClick();
                }
                return false;
            }
        });

        editTextEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isOpen == false) {
                    isOpen = true;
                    dateMode = "end";
                    datePickerOnClick();
                }
                return false;
            }
        });

        downloadPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int err = 0;
                editTextStartDate.setError(null);
                UserList userList = UserList.getInstance(activity);

                if (spinnerEmployee.getSelectedItemPosition() >= 0) {
                    userId = userList.getPosition(spinnerEmployee.getSelectedItemPosition()).getUserId();
                }

                filterStartDate = editTextStartDate.getText().toString();
                filterEndDate = editTextEndDate.getText().toString();
                if (filterStartDate.length() <= 0) {
                    err++;
                }

                if (err == 0) {
                    int MyVersion = Build.VERSION.SDK_INT;
                    if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        if (!checkIfAlreadyhavePermission()) {
                            requestForSpecificPermission();
                        } else {
                            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                            File folder = new File(extStorageDirectory, "Download");
                            folder.mkdir();
                            File file = new File(folder, "Kinerja_Harian_" + filterStartDate + " - " + filterEndDate + ".pdf");
                            try {
                                file.createNewFile();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            MSharedData shareData = MSharedData.getInstance(activity);
                            String sessionId = shareData.get(SharedData.SESSION_ID);
                            String url = "http://iproms.kip-capital.com/api/data/getDailyPdf?";
                            url += "session_id=" + sessionId;
                            url += "&status=" + "ACCEPT";
                            url += "&start_date=" + filterStartDate;
                            url += "&end_date=" + filterEndDate;
                            url += "&user=" + userId;
                            System.out.println(url);
//                            Downloader.DownloadFile(activity, url, file);
                            new DownloadTask(file, activity).execute(url);
                        }
                    } else {
                        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                        File folder = new File(extStorageDirectory, "Download");
                        folder.mkdir();
                        File file = new File(folder, "Kinerja_Harian" + filterStartDate + ".pdf");
                        try {
                            file.createNewFile();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        MSharedData shareData = MSharedData.getInstance(activity);
                        String sessionId = shareData.get(SharedData.SESSION_ID);
                        String url = "http://iproms.kip-capital.com/api/data/getDailyPdf?";
                        url += "session_id=" + sessionId;
                        url += "&status=" + "ACCEPT";
                        url += "&date=" + filterStartDate;
                        url += "&user=" + userId;
                        new DownloadTask(file, activity).execute(url);
                    }
                } else {
                    editTextStartDate.setError("Isi tanggal kinerja harian");
                }


            }
        });

// PIE CHART TEST
//        PieChart pieChart = findViewById(R.id.chart_performance);
//        UserList userList = UserList.getInstance(this);
//        List<PieEntry> entries = new ArrayList<PieEntry>();
//        entries.add(new PieEntry(22, "Jenius"));
//        entries.add(new PieEntry(58, "Pintar"));
//        entries.add(new PieEntry(12, "Tidak Bodoh"));
//        entries.add(new PieEntry(8, "Sangat Pintar"));
//        PieDataSet dataSet = new PieDataSet(entries, "");
//        dataSet.setSliceSpace(2);
//
//        dataSet.setColors(getResources().getColor(R.color.colorBlue), getResources().getColor(R.color.colorButtonGreen), getResources().getColor(R.color.StandartOrange), getResources().getColor(R.color.md_red_500));
//        dataSet.setValueTextColor(getResources().getColor(R.color.colorPrimary));
//        dataSet.setValueTextSize(15);
//
//        PieData pieData = new PieData(dataSet);
//
////        pieData.addDataSet(dataSet2);
//
//        Description description = new Description();
//        description.setText("Data IQ orang di Surabaya");
//        pieChart.setCenterText("Data IQ 2017");
//        pieChart.setCenterTextSize(22);
//        pieChart.setTransparentCircleRadius(0);
//        pieChart.setDescription(description);
//        pieChart.setData(pieData);
//        pieChart.invalidate();


    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void doApiSubordinate() {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "Subordinate");
            HashMap postData = new HashMap();
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {

                        UserChipActivity userChipActivity = UserChipActivity.getInstance(activity).clear();

                        responseObj = new JSONObject(response);
                        User user = User.getInstance(activity);

                        UserList userArrayList = UserList.getInstance(activity);
                        userArrayList.clear();
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject userList = (JSONObject) resultsArr.get(i);
                                user = new User(activity);
                                user.setUserId(userList.getString("user_id"));
                                user.setUsername(userList.getString("username"));
                                user.setFirstName(userList.getString("first_name"));
                                user.setLastName(userList.getString("last_name"));
                                user.setEmail(userList.getString("email"));
                                userArrayList.add(user);
                            }

                            ArrayAdapter<String> userArrayAdapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_spinner_item, userArrayList.getAllUsername());
                            userArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerEmployee.setAdapter(userArrayAdapter);
                            closeDialog(activity);
                        } else {
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                            closeDialog(activity);
                        }
                    } catch (JSONException e) {
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                        closeDialog(activity);
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {

                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            e1.printStackTrace();
            closeDialog(activity);
        }
    }


    public void doApiDailyACtivity(final String page) {
        openDialog(activity);
        try {
            apiIProms = ApiIProms.factory(this, "DailyList");
            HashMap postData = new HashMap();
            postData.put("page", page);
            postData.put("status", "");
            postData.put("start_date", filterStartDate);
            postData.put("end_date", filterEndDate);
            postData.put("user", userId);
            apiIProms.setPostData(postData);
            apiIProms.execute(new MApi.ApiListener() {
                @Override
                public void onResponse(String response) {
                    JSONObject responseObj = null;
                    try {
                        responseObj = new JSONObject(response);
                        String errCode = responseObj.getString("err_code");
                        String errMessage = responseObj.getString("err_message");
                        System.out.println(response);
                        DailyActivity dailyActivity = DailyActivity.getInstance(activity);
                        DailyActivityList dailyActivityList = DailyActivityList.getInstance(activity);
                        ArrayList<DailyActivity> tempArraylist = new ArrayList<DailyActivity>();
                        if (errCode.equalsIgnoreCase("0")) {
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray resultsArr = data.getJSONArray("list");
                            System.out.println(resultsArr);
                            int pageCount = 0;

                            for (int i = 0; i < resultsArr.length(); i++) {
                                JSONObject dailyActivityObj = (JSONObject) resultsArr.get(i);
                                dailyActivity = new DailyActivity();

                                if (dailyActivityObj.getString("daily_activity_id").length() > 0) {
                                    pageCount += 1;
                                }

                                dailyActivity.setId(dailyActivityObj.getString("daily_activity_id"));
                                dailyActivity.setUsersId(dailyActivityObj.getString("users_id"));
                                dailyActivity.setCreated(dailyActivityObj.getString("createdby"));
                                dailyActivity.setCreatedDate(dailyActivityObj.getString("created"));
                                dailyActivity.setDescription(dailyActivityObj.getString("description"));
                                dailyActivity.setDailyActivityCode(dailyActivityObj.getString("daily_activity_code"));
                                dailyActivity.setDailyActivityName(dailyActivityObj.getString("daily_activity_name"));
                                dailyActivity.setDailyActivityStatus(dailyActivityObj.getString("daily_activity_status"));
                                dailyActivity.setStartDate(dailyActivityObj.getString("start_date"));
                                dailyActivity.setEndDate(dailyActivityObj.getString("end_date"));
                                dailyActivity.setDeadlineDate(dailyActivityObj.getString("deadline_date"));
                                dailyActivity.setUserApprove(dailyActivityObj.getString("users_id_approve"));
                                dailyActivity.setUserApproveName(dailyActivityObj.getString("user_approve_name"));
                                dailyActivity.setUsersCreator(dailyActivityObj.getString("user_creator"));
                                dailyActivity.setApprovable(dailyActivityObj.getString("approvable"));
                                dailyActivity.setDailyActivityReason(dailyActivityObj.getString("reason"));
                                dailyActivity.setUpdateDate(dailyActivityObj.getString("updated"));
                                dailyActivity.setUpdateBy(dailyActivityObj.getString("updatedby"));

                                if (dailyActivityObj.getString("approvable").equalsIgnoreCase("0")) {
                                    tempArraylist.add(dailyActivity);
                                }

                            }

                            dailyActivity = DailyActivity.getInstance(activity);
                            dailyActivityList.setDailyActivityArrayList(tempArraylist);

                            AdapterDaily adapterDaily = new AdapterDaily(activity, tempArraylist, false);
                            dailyRecyclerView.setAdapter(adapterDaily);

                            closeDialog(activity);
                        } else {
                            closeDialog(activity);
                            Snackbar.make(layoutMain, errMessage, Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } catch (JSONException e) {
                        closeDialog(activity);
                        Snackbar.make(layoutMain, getResources().getString(R.string.parse_error), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onErrorResponse(Exception error) {
                    closeDialog(activity);
                }
            });
        } catch (MApiException e1) {
            closeDialog(activity);
            e1.printStackTrace();
        }
    }


    public void datePickerOnClick() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = "";
        final String title, dateValue;

        if (dateMode.equalsIgnoreCase("start")) {
            title = "Tanggal Awal Kinerja Harian";
            dateValue = editTextStartDate.getText().toString();
        } else {
            title = "Tanggal Akhir Kinerja Harian";
            dateValue = editTextEndDate.getText().toString();
        }
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                ReportPerformanceActivity.this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );
        if (dateValue.trim().length() > 1) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String newdate = "";
            Date date = cal.getTime();
            try {
                date = (Date) df.parse(dateValue);
                cal.setTime(date);
                System.out.println(date);
                System.out.println("hei1" + cal.get(Calendar.DAY_OF_MONTH));
                System.out.println("hei2" + cal.get(Calendar.MONTH));
                System.out.println("hei3" + cal.get(Calendar.YEAR));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dpd = DatePickerDialog.newInstance(
                    ReportPerformanceActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            );
        }
        dpd.setTitle(title);
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isOpen = false;
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String fullDate;
        monthOfYear = monthOfYear + 1;
        String month = "00";
        if (monthOfYear >= 10) {
            month = String.valueOf(monthOfYear);
        } else {
            month = "0" + String.valueOf(monthOfYear);
        }
        fullDate = dayOfMonth + "-" + month + "-" + year;

        if (dateMode.equalsIgnoreCase("start")) {
        } else {
        }
        if (dateMode.equalsIgnoreCase("start")) {
            editTextStartDate.setText(fullDate);
        } else {
            editTextEndDate.setText(fullDate);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.going_out, R.anim.going_in);
    }
}
